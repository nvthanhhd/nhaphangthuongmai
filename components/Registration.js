/*
Màn hình đăng ky thanh vien
Author: Kee
*/
import React, { Component } from 'react';
import {View, Text, Item, Input, Icon, Container, Content, Button, Picker} from 'native-base';
import {StyleSheet, Image, TextInput, KeyboardAvoidingView, Keyboard, Platform, Alert, Dimensions} from 'react-native';
import { validateEmail, validatePhoneNumber } from '../Validate';
import { LoginScreen } from '../ScreenNames';
import { appConfig } from "../AppConfig";
import { postDataRegistration } from "../requests/User";
import Loader from './Loader';


export default class Registration extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title:navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>)
		};
	};

	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			txtUsername: '',
			txtFullname: '',
			txtEmail: '',
			txtPassword: '',
			txtRePassword: '',
			txtPhoneNumber: '',
			errorRegistration: ''
		};
	}

	

  	onSubmitFormRegistration(){
		if(this.state.txtUsername == ""){
			this.setState({ errorRegistration: 'Vui lòng nhập vào Username' });
			return false;
		}

		if(this.state.txtUsername.length < 6){
			this.setState({ errorRegistration: 'Username phải có ít nhất 6 ký tự' });
			return false;
		}

		//Validate PhoneNumber
		if(this.state.txtPhoneNumber == ""){
			this.setState({ errorRegistration: 'Vui lòng nhập vào số điện thoại' });
			return false;
		}

		if(!validatePhoneNumber(this.state.txtPhoneNumber)) {
			this.setState({ errorRegistration: 'Số điện thoại không đúng' });
			return false;
		}else{
			this.setState({ errorRegistration: '' });
		}

		//Validate Email
		if(this.state.txtEmail == ""){
			this.setState({ errorRegistration: 'Vui lòng nhập vào email' });
			return false;
		}

		if(!validateEmail(this.state.txtEmail)) {
			this.setState({ errorRegistration: 'Email không đúng định dạng' });
			return false;
		}else{
			this.setState({ errorRegistration: '' });
		}

		if(this.state.txtFullname == ""){
			this.setState({ errorRegistration: 'Vui lòng nhập vào họ và tên' });
			return false;
		}

		//Validate Password
		if(this.state.txtPassword == ""){
			this.setState({ errorRegistration: 'Vui lòng nhập vào mật khẩu' });
			return false;
		}

		if(this.state.txtPassword.length < 6){
			this.setState({ errorRegistration: 'Mật khẩu phải có ít nhất 6 ký tự' });
			return false;
		}

		//Valide RePassword
		if(this.state.txtRePassword == ""){
			this.setState({ errorRegistration: 'Vui lòng xác nhận mật khẩu' });
			return false;
		}

		if(this.state.txtPassword !== this.state.txtRePassword){
			this.setState({ errorRegistration: 'Xác nhận mật khẩu chưa đúng' });
			return false;
		}

		//Post data to server
		const postData = {
			username: this.state.txtUsername,
			mobile_phone: this.state.txtPhoneNumber,
			email: this.state.txtEmail,
			name: this.state.txtFullname,
			password: this.state.txtPassword,
			password_confirmation: this.state.txtRePassword
		};

		this.setState({ loading: true });
		postDataRegistration(postData).then((result) => {
			if(result.success == false){
				this.setState({ loading: false });
				var arrMesage = result.message.split("<br />");
				arrMesage.map((message) => {
					this.setState({ 'errorRegistration': this.state.errorRegistration + message + '. ' });
				});
			}else{
				Alert.alert('Thông báo', 'Đăng ký thành công!', [{text: 'OK', onPress: () => this.setState({ loading: false })}], {cancelable: false});
				//Reset form
				this.setState({
					txtUsername: '',
					txtFullname: '',
					txtEmail: '',
					txtPassword: '',
					txtRePassword: '',
					txtPhoneNumber: '',
					errorRegistration: '',
				});
				//Back to login view
				this.props.navigation.navigate(LoginScreen);
			}
		}).catch((error) => {
			Alert.alert('Thông báo', 'Có lỗi xảy ra trong quá trình đăng ký', [{text: 'OK', onPress: () => this.setState({ loading: false })}], {cancelable: false});
		});
	}

	render(){
		return(
			<KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
				<View style={styles.infoContainer}>
					<Loader loading={this.state.loading} />
					<Item style={styles.item}>
						<TextInput style={{ height: 40, width: 350}}
							placeholder='Username'
							returnKeyType='next'
							autoCorrect={false}
							onChangeText={(text) => this.setState({txtUsername: text})}
							value={this.state.txtUsername}
							onSubmitEditing={()=> this.refs.txtPhoneNumber.focus()}/>
					</Item>
					<Item style={styles.item}>
						<TextInput style={{ height: 40, width: 350}}
							placeholder='Số điện thoại'
							returnKeyType='next'
							keyboardType="number-pad"
							onChangeText={(text) => this.setState({txtPhoneNumber: text})}
							value={this.state.txtPhoneNumber}
							ref={'txtPhoneNumber'}
							onSubmitEditing={()=> this.refs.txtEmail.focus()} />
					</Item>
					<Item style={styles.item}>
						<TextInput style={{ height: 40, width: 350}}
							placeholder='Email'
							keyboardType="email-address"
							returnKeyType='next'
							autoCorrect={false}
							onChangeText={(text) => this.setState({txtEmail: text})}
							value={this.state.txtEmail}
							ref={'txtEmail'}
							onSubmitEditing={()=> this.refs.txtFullname.focus()}/>
					</Item>
					<Item style={styles.item}>
						<TextInput style={{ height: 40, width: 350}}
							placeholder='Họ và tên'
							returnKeyType='next'
							autoCorrect={false}
							onChangeText={(text) => this.setState({txtFullname: text})}
							value={this.state.txtFullname}
							ref={'txtFullname'}
							onSubmitEditing={()=> this.refs.txtPassword.focus()}/>
					</Item>
					<Item style={styles.item}>
						<TextInput style={{ height: 40, width: 350}}
							placeholder='Mật khẩu'
							returnKeyType='next'
							secureTextEntry={true}
							autoCorrect={false}
							onChangeText={(text) => this.setState({txtPassword: text})}
							value={this.state.txtPassword}
							onSubmitEditing={()=> this.refs.txtRePassword.focus()}
							ref={'txtPassword'} />
					</Item>
					<Item style={styles.item}>
						<TextInput style={{ height: 40, width: 350}}
							placeholder='Nhập lại mật khẩu'
							returnKeyType='next'
							secureTextEntry={true}
							autoCorrect={false}
							onChangeText={(text) => this.setState({txtRePassword: text})}
							value={this.state.txtRePassword}
							ref={'txtRePassword'} 
							onSubmitEditing={() => this.onSubmitFormRegistration()}/>
					</Item>
					<Text style={{ marginTop: 5, color: 'red'}}>{this.state.errorRegistration}</Text>
					<Button block style={{ marginTop: 10, backgroundColor: appConfig.baseColor}} onPress={this.onSubmitFormRegistration.bind(this)}>
						<Text>Đăng ký</Text>
					</Button>
				</View>
			</KeyboardAvoidingView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white'
	},
	infoContainer: {
		flex: 1,
		paddingLeft: 10,
		paddingRight: 10,
	},
	item: {
		paddingTop: 10,
	}
});
