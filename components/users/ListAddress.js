import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modalbox';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListAddress, deleteAddress, setAddressDefault, addAddress } from "../../requests/User";
import Loader from '../Loader';
import { MainScreen, AddressDepositScreen } from '../../ScreenNames';

var screen = Dimensions.get('window');

class ModalAdd extends Component{
	constructor(props){
		super(props);
		this.state = {
			reciver_name: '',
			reciver_phone: '',
			province_id: '',
			district_id: '',
			list_district: [],
			detail: '',
			note: '',
			user_address_id: 0,
			errorAddNew: ''
		};
	}

	addNew(){
		if(this.state.reciver_name == ""){
			this.setState({ errorAddNew: 'Vui lòng nhập vào tên người nhận' });
			return false;
		}

		if(this.state.reciver_phone == ""){
			this.setState({ errorAddNew: 'Vui lòng nhập vào số điện thoại người nhận' });
			return false;
		}

		if(this.state.province_id == 0 || this.state.province_id == ""){
			this.setState({ errorAddNew: 'Vui lòng nhập chọn tỉnh / thành phố' });
			return false;
		}

		if(this.state.district_id == 0 || this.state.district_id == ""){
			this.setState({ errorAddNew: 'Vui lòng nhập chọn quận / huyện' });
			return false;
		}

		if(this.state.detail == ""){
			this.setState({ errorAddNew: 'Vui lòng nhập vào địa chỉ' });
			return false;
		}

		_retrieveData('login_info').then((user_info) => {
			var postData = {
				reciver_name: this.state.reciver_name,
				reciver_phone: this.state.reciver_phone,
				province_id: this.state.province_id,
				district_id: this.state.district_id,
				detail: this.state.detail,
				note: this.state.note,
				user_address_id: this.state.user_address_id
			}

			addAddress(postData, user_info.userInfo.access_token).then((result) => {
				if(result.success == false){
					var arrMesage = result.message.split("<br />");
					arrMesage.map((message) => {
						this.setState({ 'errorAddNew': this.state.errorAddNew + message + '. ' });
					});
				}else{
					Alert.alert('Thông báo', 'Thêm mới thành công!', [{text: 'OK', onPress: () => this.setState({ loading: false })}], {cancelable: false});
					this.closeModalAdd();
					this.props.parentView.reloadListAddress();
				}
				
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra trong quá trình cập nhật', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	showModalAdd(){
		this.refs.modalAdd.open();
	}

	closeModalAdd(){
		this.refs.modalAdd.close();
	}

	changeProvince(value: string) {
		this.setState({ 
			province_id: value,
			list_district: this.props.all_districts[value]
		});
	}

	render(){
		return(
			<Modal
				ref={'modalAdd'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:400,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5}}>
				<TextInput style={{
					borderBottomColor: '#ececec',
					borderBottomWidth: 1
				}}
				returnKeyType='next'
				autoCorrect={false}
				placeholder='Tên người nhận'
				value={this.state.reciver_name}
				onChangeText={(text) => this.setState({reciver_name: text})}
				/>
				<TextInput style={{
					borderBottomColor: '#ececec',
					borderBottomWidth: 1
				}}
				returnKeyType='next'
				autoCorrect={false}
				placeholder='Số điện thoại'
				keyboardType="number-pad"
				value={this.state.reciver_phone}
				onChangeText={(text) => this.setState({reciver_phone: text})}
				/>
				<Item >
					<Text>Tỉnh / Thành phố: </Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						selectedValue={this.state.province_id}
						onValueChange={(value) => this.changeProvince(value)}
					>
						<Picker.Item label="Chọn tỉnh/TP" value="0" />
						{Object.values(this.props.all_provinces).map((value, i) => {
							return <Picker.Item key={i} value={value.id} label={value.label} />
						})}
					</Picker>
				</Item>
				<Item >
					<Text>Quận / Huyện: </Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						selectedValue={this.state.district_id}
						onValueChange={(value) => this.setState({ district_id: value })}
					>
						<Picker.Item label="Chọn quận/huyện" value="0" />
						{Object.values(this.state.list_district).map((value, i) => {
							return <Picker.Item key={i} value={value.id} label={value.label} />
						})}
					</Picker>
				</Item>
				<TextInput style={{
					borderBottomColor: '#ececec',
					borderBottomWidth: 1
				}}
				returnKeyType='next'
				autoCorrect={false}
				placeholder='Địa chỉ'
				value={this.state.detail}
				onChangeText={(text) => this.setState({detail: text})}
				/>
				<TextInput style={{
					borderBottomColor: '#ececec',
					borderBottomWidth: 1
				}}
				returnKeyType='go'
				autoCorrect={false}
				placeholder='Ghi chú'
				value={this.state.note}
				onChangeText={(text) => this.setState({note: text})}
				/>
				<Text style={{ marginTop: 5, color: 'red'}}>{this.state.errorAddNew}</Text>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this.addNew() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: 'white'}}>Thêm</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class FlatListItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			notify_status: this.props.item.notify_status,
			show: true
		}
	}

	_deleteAddress(){
		Alert.alert('Thông báo', 'Bạn có muốn xoá địa chỉ này?', [
			{text: 'No', onPress: () => console.log('Canceled'), style: 'cancel'},
			{text: 'Yes', onPress: () => {
				_retrieveData('login_info').then((user_info) => {
					const postData = { id: this.props.item.id };

					deleteAddress(postData, user_info.userInfo.access_token).then((result) => {
						if(result.success == true){
							this.setState({ show: false });
						}
					}).catch((error) => {
						console.error(`Error is: ${error}`);
					});
				});
			}}
		]);
	}

	setAddressDefault(){
		_retrieveData('login_info').then((user_info) => {
			const postData = { id: this.props.item.id };

			setAddressDefault(postData, user_info.userInfo.access_token).then((result) => {
				if(result.success == true){
					this.props.parentFlatList.reloadListAddress();
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra trong quá trình cập nhật', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.setState({ is_read: this.props.item.is_read });
	}

	render(){
		if(!this.state.show){
			return (<View></View>);
		}

		return(
			<View style={{flex: 1, flexDirection: 'row', padding: 10, margin: 5, backgroundColor: 'white', borderRadius: 10}}>
				<View style={{flex: 3, borderRightWidth: 1, borderRightColor: '#ececec'}}>
					<View style={{flex: 1}}>
						<Text style={{fontSize: 13}}>{this.props.item.reciver_name} - {this.props.item.reciver_phone}</Text>
					</View>
					<View style={{flex: 1}}>
						<Text style={{fontSize: 13}}>{this.props.item.detail}</Text>
					</View>
					<View style={{flex: 1}}>
						<Text style={{fontSize: 13, color: 'gray'}}>{this.props.item.district_label}, {this.props.item.province_label}</Text>
					</View>
				</View>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center',}}>
					{this.props.item.is_default == '1' ?
					(<View><Icon name='checkmark' style={{fontSize: 40, color: 'green'}} /></View>) :
						<TouchableOpacity onPress={() => { this.setAddressDefault() }}>
							<Text style={{ fontSize: 13, padding: 5, color: appConfig.baseColor, borderRadius: 5, borderColor: appConfig.baseColor, borderWidth: 1}}>Mặc định</Text>
						</TouchableOpacity>
					}

					{this.props.item.is_default == '1' ?
					(<View></View>) :
					<Button iconLeft transparent onPress={() => { this._deleteAddress() }}>
						<Icon  name='trash' style={{ color: 'red'}} />
						<Text style={{ fontSize: 12, left: -10, color: 'red' }}>Xoá</Text>
					</Button>
					}
				</View>
			</View>
		)
	}
}

export default class ListAddress extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			total: 0,
			data: [],
			all_provinces: [],
			all_districts: []
		}
	}

	freshDataAddress(){
		_retrieveData('login_info').then((user_info) => {
			if(this.state.lastpage) return;
			this.setState({ 
				isLoading: true,
				all_provinces: user_info.userInfo.all_provinces,
				all_districts: user_info.userInfo.all_districts,
			});
			getListAddress(user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					this.setState({
						data: result.data,
						total: result.total,
					});
				}else{
					this.setState({ data: [] });
				}


			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}


	addNewAddress(){
		this.refs.modalAdd.showModalAdd();
	}

	componentDidMount(){
		this.freshDataAddress();
	}

	reloadListAddress(){
		this.freshDataAddress();
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
					<Button disabled style={{  flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#ececec'}} iconLeft full light>
						<View style={{flex: 1}}>
							<Text style={{ fontSize: 13, color: appConfig.baseColor }}>{this.state.total} (Địa chỉ)</Text>
						</View>
						<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
							<Button onPress={() => this.addNewAddress() } small style={{margin: 5, backgroundColor: appConfig.baseColor}}><Text style={{fontSize: 13}}>Thêm địa chỉ</Text></Button>
						</View>
					</Button>

				<FlatList
					data={this.state.data}
					renderItem={({item, index}) => {
						return(
							<FlatListItem navigation={this.props.navigation} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => index.toString()}
				>

				</FlatList>
				<ModalAdd all_provinces={this.state.all_provinces} all_districts={this.state.all_districts} ref={'modalAdd'} parentView={this}></ModalAdd>
			</View>
		)
	}
}
