import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Picker, Icon, Button, Thumbnail } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, Image, Text } from 'react-native';
import { createComplain } from "../../requests/Order";
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import Loader from '../Loader';
import { MainScreen } from '../../ScreenNames';
import ImagePicker from 'react-native-image-picker';


export default class CreateComplain extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);

		this.state = {
			isLoading: false,
			transaction_type: '',
			process_type: '',
			object_id: this.props.navigation.getParam('order_code'),
			item_id: this.props.navigation.getParam('item_id') != undefined ? this.props.navigation.getParam('item_id').toString() : '',
			object_type: 'ORDER',
			customer_request_amount: '',
			customer_description: '',
			list_complain_type: [],
			list_complain_reason: [],
			images: [],
			images_base64: []
		};
	}

	selectImage = async () => {
		ImagePicker.showImagePicker({data: true, mediaType: 'photo', title: 'Chọn ảnh khiếu nại', takePhotoButtonTitle: 'Chụp ảnh', chooseFromLibraryButtonTitle: 'Chọn từ thư viện'}, (response) => {
		  if (response.didCancel) {
		    console.log('User cancelled image picker');
		  } else if (response.error) {
		    console.log('ImagePicker Error: ', response.error);
		  } else if (response.customButton) {
		    console.log('User tapped custom button: ', response.customButton);
		  } else {
		    const source = { uri: response.uri };
		    const source64 = 'data:image/jpeg;base64,' + response.data;
		    var images = this.state.images;
		    var images_base64 = this.state.images_base64;

		    images.push(source);
		    images_base64.push(source64);
		    this.setState({
		      images: images,
		      images_base64: images_base64
		    });
		  }
		});
	}


	changeTransactionType(value: string){
		this.setState({
			transaction_type: value
		});
	}

	changeProcessType(value: string) {
		this.setState({
			process_type: value
		});
	}

	createComplain(){
		if(this.state.object_id == ""){
			Alert.alert('Thông báo', 'Vui lòng nhập vào mã đơn', [{text: 'OK' }], {cancelable: false});
			return false;
		}

		if(this.state.transaction_type == ''){
			Alert.alert('Thông báo', 'Vui lòng chọn lý do khiếu nại', [{text: 'OK' }], {cancelable: false});
			return false;
		}

		if(this.state.process_type == ''){
			Alert.alert('Thông báo', 'Vui lòng chọn phương án xử lý', [{text: 'OK' }], {cancelable: false});
			return false;
		}

		_retrieveData('login_info').then((user_info) => {
			var postData = {
				object_id: this.state.object_id,
				item_id: this.state.item_id,
				object_type: this.state.object_type,
				transaction_type: this.state.transaction_type,
				process_type: this.state.process_type,
				customer_request_amount: this.state.customer_request_amount,
				customer_description: this.state.customer_description,
				files: this.state.images_base64
			}
			this.setState({ isLoading: true });
			// console.log(postData);
			// return;
			createComplain(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				console.log(result);
				if(result.success == true){
					Alert.alert('Thông báo', 'Gửi khiếu nại thành công', [{text: 'OK' }], {cancelable: false});
					//back to detail order
					this.props.navigation.goBack();
				}else{
					Alert.alert('Thông báo', 'Không thể gửi khiếu nại', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		_retrieveData('login_info').then((user_info) => {
			this.setState({
				list_complain_type: user_info.userInfo.complaint_type,
				list_complain_reason: user_info.userInfo.complaint_reason
			});
		});
	}

  render() {
    return (
		<Container>
			<Loader loading={this.state.isLoading} />
			<Content>
				<Form>
					<Item>
						<Input 
							placeholder="Mã đơn"
							returnKeyType='next'
							autoCorrect={false}
							onChangeText={(text) => this.setState({object_id: text})}
							value={this.state.object_id}
						/>
					</Item>
					<Item>
						<Input 
							placeholder="Mã sản phẩm"
							returnKeyType='next'
							autoCorrect={false}
							onChangeText={(text) => this.setState({item_id: text})}
							value={this.state.item_id} />
					</Item>
					<Item>
						<Picker
							mode="dropdown"
							iosIcon={<Icon name="arrow-down" />}
							style={{ width: undefined }}
							selectedValue={this.state.transaction_type}
							onValueChange={this.changeTransactionType.bind(this)}
							>
							<Picker.Item label="Lý do khiếu nại" value="" />
							{Object.keys(this.state.list_complain_reason).map((key, i) => {
								return <Picker.Item key={i} value={key} label={this.state.list_complain_reason[key]} />
							})}
						</Picker>
					</Item>
					<Item>
						<Picker
							mode="dropdown"
							iosIcon={<Icon name="arrow-down" />}
							style={{ width: undefined }}
							selectedValue={this.state.process_type}
							onValueChange={this.changeProcessType.bind(this)}
							>
							<Picker.Item label="Phương án xử lý" value="" />
							{Object.keys(this.state.list_complain_type).map((key, i) => {
								return <Picker.Item key={i} value={key} label={this.state.list_complain_type[key]} />
							})}
						</Picker>
					</Item>
					<Item>
						<Input 
							placeholder="Số tiền đề xuất" 
							returnKeyType='next'
							keyboardType="number-pad"
							autoCorrect={false}
							onChangeText={(text) => this.setState({customer_request_amount: text})}
							value={this.state.customer_request_amount}
						/>
					</Item>
					<Item>
						<Input 
							placeholder="Mô tả" 
							returnKeyType='next'
							autoCorrect={false}
							multiline={true}
	    					numberOfLines={4}
							onChangeText={(text) => this.setState({customer_description: text})}
							value={this.state.customer_description}
						/>
					</Item>
					<Item style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
						{ this.state.images.length < 5 ? (
							<Button onPress={() => this.selectImage() } small iconLeft light style={{ margin: 5, padding: 10}}>
								<Icon name='add' style={{ fontSize: 26}} />
								<Text style={{ marginHorizontal: 5, fontSize: 16}}> Upload ảnh</Text>
							</Button>) : (<View></View>)}
						<View style={{flex: 1, flexDirection: 'row'}}>
					        {Object.keys(this.state.images).map((key, i) => {
								return <View key={key} style={{flex: 1, marginRight: 5}}><Thumbnail style={{ borderWidth: 1, borderColor: '#ececec'}} square large source={this.state.images[key] } /></View>
							})}
						</View>
					</Item>
					<View style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20}}>
							<Button onPress={() => this.createComplain() } style={{
								backgroundColor: appConfig.baseColor,
								padding: 10,
							}}>
							<Text style={{ color: 'white'}}>Tạo khiếu nại</Text>
						</Button>
					</View>
				</Form>
			</Content>
		</Container>
    );
  }
}