import React, { Component } from 'react';
import { View, Text, TextInput, Alert } from 'react-native';
import { Container, Header, Content, Form, List, ListItem, Item, Input, Label, Button, Picker, Icon, Left, Body, Right, Thumbnail } from 'native-base';
import { DetailWebOrderScreen, MainScreen } from '../../ScreenNames';
import Loader from '../Loader';
import _ from 'lodash';
import { translateSearchKeyword } from "../../requests/GoogleTranslate";
import { _retrieveData, isValidURL } from '../../Helpers';
import { appConfig } from "../../AppConfig";

export default class SearchProduct extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			loading: false,
			site_selected: "taobao.com",
			search_value: '',
			translate_value: '',
			btnShowClearInput: false
		};
	}

	onValueChange(value: string) {
		this.setState({
			site_selected: value
		});
	}

	_gotoSearchKeyword(translate_value){
		var url = '';
		if(this.state.site_selected == 'taobao.com'){
			url = 'https://m.intl.taobao.com/search/search.html?q='+translate_value;
		}else if(this.state.site_selected == 'tmall.com'){
			url = 'https://list.tmall.com/search_product.htm?q='+translate_value;
		}else{
			url = 'https://m.1688.com/offer_search/-search.html?keywords='+translate_value;
		}

		this.props.navigation.navigate(DetailWebOrderScreen, {title: this.state.site_selected, url: url });
	}

	_getHostName(url) {
		var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
		if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
			return match[2];
		}else {
			return null;
		}
	}

	_searchProduct(){
		if(this.state.search_value != ''){
			if(isValidURL(this.state.search_value)){
				var hostName = this._getHostName(this.state.search_value);
				var domain = hostName;

				if (hostName != null) {
					var parts = hostName.split('.').reverse();
					if (parts != null && parts.length > 1) {
						domain = parts[1] + '.' + parts[0];
						if (hostName.toLowerCase().indexOf('.co.uk') != -1 && parts.length > 2) {
							domain = parts[2] + '.' + domain;
						}
					}
				}

				if(domain != null && (domain == '1688.com' || domain == 'taobao.com' || domain == 'tmall.com')){
					this.props.navigation.navigate(DetailWebOrderScreen, {title: domain, url: this.state.search_value})
				}
			}else{
				this.setState({ loading: true });
				translateSearchKeyword(this.state.search_value).then((result) => {
					var translate_value = '';
					this.setState({ loading: false });
					if(result.data.translations != undefined && result.data.translations[0].translatedText != undefined && result.data.translations[0].translatedText != ''){
						translate_value = result.data.translations[0].translatedText;
					}else{
						translate_value = this.state.search_value;
					}

					this._gotoSearchKeyword(translate_value);
				}).catch((error) => {
					console.error(`Error is: ${error}`);
				});
			}
		}else{
			Alert.alert('Thông báo', 'Vui lòng nhập vào link sản phẩm hoặc từ khoá tìm kiếm!');
		}
	}

	_clearInputSearch(){
		this.refs.inputSearch.clear();
		this.setState({btnShowClearInput: false});
	}

	_changeTextInputSearch(text){
		this.setState({search_value: text});
		if(text.length > 0){
			this.setState({btnShowClearInput: true});
		}else{
			this.setState({btnShowClearInput: false});
		}
	}

	render() {
		return (
			<Container>
				<Content>
					<Loader loading={this.state.loading} />
					<List>
						<ListItem style={{justifyContent: 'center', alignItems: 'center', borderColor: 'white'}}>
							<TextInput style={{borderRadius: 10, borderWidth: 1, borderColor: '#ececec', width: this.state.btnShowClearInput ? '92%' : '100%', padding: 12}}
								placeholder='Link sản phẩm hoặc từ khoá tìm kiếm'
								returnKeyType='next'
								autoCorrect={false}
								onChangeText={(text) => this._changeTextInputSearch(text) }
								value={this.state.search_value}
								ref={'inputSearch'}
								/>
								{ this.state.btnShowClearInput && (
								<Button onPress={() => this._clearInputSearch()} iconLeft danger transparent>
									<Icon name='close-circle' />
								</Button>)
								}
						</ListItem>
						<ListItem style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginRight: 10}}>
							<View style={{flex: 3}}>
								<Picker
								  mode="dropdown"
								  iosHeader="Website"
								  iosIcon={<Icon name="arrow-down" />}
								  style={{ width: undefined }}
								  selectedValue={this.state.site_selected}
								  onValueChange={this.onValueChange.bind(this)}
								>
									<Picker.Item label="taobao.com" value="taobao.com" />
									<Picker.Item label="tmall.com" value="tmall.com" />
									<Picker.Item label="1688.com" value="1688.com" />
								</Picker>
							</View>
							<Button onPress={() => this._searchProduct()} block iconLeft style={{ flex: 2, paddingRight: 10, backgroundColor: appConfig.baseColor}}>
								<Icon name='search' />
								<Label style={{ color: 'white', marginLeft: 5}}>Tìm kiếm</Label>
							</Button>
						</ListItem>
						<ListItem onPress={() => this.props.navigation.navigate(DetailWebOrderScreen, {title: 'taobao.com', url: 'https://m.intl.taobao.com/'})}  style={{justifyContent: 'flex-start', alignItems: 'flex-start', paddingVertical: 10}} thumbnail >
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Left style={{flex: 1}}>
									<Thumbnail large square source={require('../../images/taobao_icon.png')} />
								</Left>
								<View style={{paddingRight: 10, flex: 3}}>
									<Text style={{fontWeight: 'bold', fontSize: 13}}>TAOBAO.COM</Text>
									<Text style={{fontSize: 13}} note numberOfLines={2}>Là trang tham khảo nguồn hàng đa dạng</Text>
								</View>
							</View>
						</ListItem>
						<ListItem onPress={() => this.props.navigation.navigate(DetailWebOrderScreen, {title: '1688.com', url: 'https://m.1688.com/'})} style={{justifyContent: 'flex-start', alignItems: 'flex-start', paddingVertical: 10}} thumbnail >
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Left style={{flex: 1}}>
									<Thumbnail large square source={require('../../images/1688_icon.png')} />
								</Left>
								<View style={{paddingRight: 10, flex: 3}}>
									<Text style={{fontWeight: 'bold', fontSize: 13}}>1688.COM</Text>
									<Text style={{fontSize: 13}} note numberOfLines={2}>Là trang bán buôn giá rẻ hàng đầu Trung Quốc</Text>
								</View>
							</View>
						</ListItem>
						<ListItem onPress={() => this.props.navigation.navigate(DetailWebOrderScreen, {title: 'tmall.com', url: 'https://www.tmall.com/'})} style={{justifyContent: 'flex-start', alignItems: 'flex-start', paddingVertical: 10}} thumbnail >
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Left style={{flex: 1}}>
									<Thumbnail large square source={require('../../images/tmall_icon.png')} />
								</Left>
								<View style={{paddingRight: 10, flex: 3}}>
									<Text style={{fontWeight: 'bold', fontSize: 13}}>TMALL.COM</Text>
									<Text style={{fontSize: 13}} note numberOfLines={2}>Bán hàng hoá chính hãng chất lượng cao</Text>
								</View>
							</View>
						</ListItem>
					</List>
				</Content>
			</Container>
			);
  }
}
