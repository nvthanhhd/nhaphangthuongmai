import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ActivityIndicator, Alert, Dimensions, TouchableOpacity } from 'react-native';
import { Button, Icon, Thumbnail } from 'native-base';
import { WebView } from 'react-native-webview';
import { ListCartScreen, MainScreen } from '../../ScreenNames';
import Loader from '../Loader';
import { addToCart } from "../../requests/Cart";
import { translateArrayKeyword } from "../../requests/GoogleTranslate";
import { _retrieveData } from '../../Helpers';
import { appConfig } from "../../AppConfig";

export default class DetailWebOrder extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={navigation.getParam('_previousWebOrBackScreen')}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () => (<View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'cart'})} style={{ justifyContent: 'center', flex: 1}}>
					<Icon type="FontAwesome" name='shopping-cart' style={{ fontSize: 24, color: '#FFF', marginRight: 5}} />
				</Button>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center', flex: 1}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
				</View>
				)

		};
	};

	componentDidMount() {
		this.props.navigation.setParams({ _previousWebOrBackScreen: this._previousWebOrBackScreen});
	}

	_previousWebOrBackScreen = () => {
		if(this.state.canGoBack == true){
			this.webView.goBack();
		}else{
			this.props.navigation.goBack();
		}
 	}

	constructor(props){
		super(props);
		this.state = {
			loading: false,
			url: this.props.navigation.getParam('url'),
			title: this.props.navigation.getParam('title'),
			btnDisabled: true,
			btnTranslateDisabled: true,
			transToVn: true,
			transCn: undefined,
			transVn: undefined,
			itemIdTranslate: 0,
		};

		this.onMessage = this.onMessage.bind(this);
	}

	_getJsCode(){
		let taobaoJs = `
			try {
					//taobao
					var bar = document.querySelector('.bar');
					if(bar != null){

						window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnDisabled", "valuePost" : false }));
						bar.style["display"] = "none";

						setTimeout(function(){
							var modalSku = document.querySelector('.sku.card .modal');
							if(modalSku != null){
								if(modalSku.classList.contains('modal-show')){
									window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnTranslateDisabled", "valuePost" : false }));
								}else{
									window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnTranslateDisabled", "valuePost" : true }));
								}
							}

							var btnTranslateCn = document.getElementById('btnTranslateCn');
							//Them nut dich vao giao dien
							if(btnTranslateCn == null){
								var modalTitleSku = document.querySelector('.modal-title.modal-title-sku');
								var newElement = document.createElement('div');
								newElement.style.padding = '10px';
								newElement.innerHTML = '<button id="btnTranslateCn" style="color: white; background-color: #00c4f4; padding-left: 10px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; font-size: 14px;  border: 1px solid white ; border-radius: 5px;" type="button" data-spm-anchor-id="a21wu.9600033.0.i2">Dịch</button>';
								var elementParent = modalTitleSku.parentNode;
								elementParent.insertBefore(newElement, modalTitleSku.nextSibling);
								var btnTranslateCn = document.getElementById('btnTranslateCn');
								btnTranslateCn.addEventListener("click", clickToTranslate);
							}
						}, 500);
					}else{
						//tmall
						var actionBar = document.getElementById('s-actionBar-container');
						if(actionBar != null){
							window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnDisabled", "valuePost" : false }));
							actionBar.style["display"] = "none";
						}else{
							window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnDisabled", "valuePost" : true }));
						}
					}



					var inputLoaded = document.getElementById('jsLoaded__');
					if(inputLoaded == null){
						var input = document.createElement("input");
						input.type = "hidden";
						input.id = "jsLoaded__";
						document.querySelector('div').appendChild(input);

						window.addEventListener('message', function(e) {
							eventListenerFromRN(e);
						});

						document.addEventListener('message', function(e) {
							eventListenerFromRN(e);
						});

						function eventListenerFromRN(e){
							var data = JSON.parse(e.data);
							if(data.action_name == 'translate'){
								//taobao
								var skuContent = document.querySelectorAll('.modal-sku-content');
								var arrTranslate = [];
								var itemIdTranslate = getItemId();
								if(skuContent.length > 0){
									for(var i = 0; i < skuContent.length; i++){
										var pid = skuContent[i].getAttribute('data-pid');
										var skuContentItem = skuContent[i].querySelectorAll('.modal-sku-content-item');
										var skuContentTitle = skuContent[i].querySelector('.modal-sku-content-title').textContent;
										arrTranslate.push(skuContentTitle + ' ::::'+pid);

										if(skuContentItem.length > 0){
											for(var j = 0; j < skuContentItem.length; j++){
												var vid = skuContentItem[j].getAttribute('data-vid');
												var itemTitle = skuContentItem[j].textContent;
												arrTranslate.push(itemTitle + ' :::::'+vid);
											}
										}
									}
								}

								//tmall
								if(arrTranslate.length == 0){
									var skuList = document.querySelectorAll('.sku-list-wrap li');
									if(skuList.length > 0){
										for(var i = 0; i < skuList.length; i++){
											var title = skuList[i].querySelector('h2').textContent;
											var skuItems = skuList[i].querySelectorAll('a');
											if(skuItems.length > 0){
												for(var j = 0; j < skuItems.length; j++){
													var id = skuItems[j].getAttribute('data-value');
													var itemText = skuItems[j].textContent;
													if(j == 0){
														arrTranslate.push(title + ' ::::t'+id);
													}
													arrTranslate.push(itemText + ' :::::'+id);
												}
											}
										}
									}
								}

								window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "transToVn", "valuePost" : true, "data": arrTranslate, "itemIdTranslate": itemIdTranslate }));
							}else if(data.action_name == 'order'){
								//taobao
								var modalSku = document.querySelector('.sku.card .modal');
								if(modalSku != null){
									var skuQuantityTitle = document.querySelector('.modal-sku-content-quantity-title');

									if(skuQuantityTitle != null){
										skuQuantityTitle.innerHTML = 'Số lượng';
									}

									if(modalSku.classList.contains('modal-show')){
										linkClick();
									}else{
										var btnShowModal = document.querySelector('.sku.card');
										btnShowModal.click();
									}
								}

								setTimeout(function(){
									var modalSku = document.querySelector('.sku.card .modal');
									if(modalSku != null){
										if(modalSku.classList.contains('modal-show')){
											window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnTranslateDisabled", "valuePost" : false }));
										}else{
											window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnTranslateDisabled", "valuePost" : true }));
										}
									}
								}, 500);

								//tmall
								var skuWarp = document.querySelector('.sku-wrap');
								if(skuWarp != null){
									var modalSkuTmall = skuWarp.parentNode.parentNode;
									var skuQuantityTitle = modalSkuTmall.querySelector('.number-line label');

									if(skuQuantityTitle != null){
										skuQuantityTitle.innerHTML = 'Số lượng';
									}

									if(modalSkuTmall.classList.contains('show')){
										linkClick();
									}else{
										var btnShowModal = document.querySelector('.module-sku');
										btnShowModal.click();
									}
								}else{
									var btnShowModal = document.querySelector('.module-sku');
									btnShowModal.click();
								}



								setTimeout(function(){
									var skuWarp = document.querySelector('.sku-wrap');
									if(skuWarp != null){
										var modalSkuTmall = skuWarp.parentNode.parentNode;

										if(modalSkuTmall.classList.contains('show')){
											window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnTranslateDisabled", "valuePost" : false }));
										}else{
											window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnTranslateDisabled", "valuePost" : true }));
										}
									}
								}, 500);
							}else if(data.action_name == 'translate_to_vn'){
								var trans = data.extend_data;
								if(trans.length > 0){
									for(var i = 0; i < trans.length; i++){
										if(trans[i].translatedText != undefined && trans[i].translatedText != ''){
											if(trans[i].translatedText.substring(0, trans[i].translatedText.indexOf(':::::')) != ""){ //Item
												var vnStr = trans[i].translatedText.substring(0, trans[i].translatedText.indexOf(':::::'));
												var id = trans[i].translatedText.substring(trans[i].translatedText.indexOf(':::::') + 6, trans[i].translatedText.length).replace(/[^0-9]/g, '');
												var skuItem = document.querySelector('.modal-sku-content-item[data-vid="'+id+'"]');
												if(skuItem != null){
													skuItem.innerHTML = vnStr;
												}else{
													//tmall
													var id = trans[i].translatedText.substring(trans[i].translatedText.indexOf(':::::') + 6, trans[i].translatedText.length).replace(/[^0-9\:]/g, '');
													var skuItem = document.querySelector('.sku-list-wrap .items a[data-value="'+id+'"]');
													if(skuItem != null){
														skuItem.innerHTML = vnStr;
													}
												}
											}else if(trans[i].translatedText.substring(0, trans[i].translatedText.indexOf('::::')) != ""){ //Title
												var vnStr = trans[i].translatedText.substring(0, trans[i].translatedText.indexOf('::::'));
												var id = trans[i].translatedText.substring(trans[i].translatedText.indexOf('::::') + 5, trans[i].translatedText.length).replace(/[^0-9]/g, '');

												var skuContent = document.querySelector('.modal-sku-content[data-pid="'+id+'"]');
												if(skuContent != null){
													skuContent.querySelector('.modal-sku-content-title').innerHTML = vnStr;
												}else{
													//tmall
													var id = trans[i].translatedText.substring(trans[i].translatedText.indexOf('::::') + 5, trans[i].translatedText.length).replace(/[^0-9\:]/g, '');
													var skuContent = document.querySelector('.sku-list-wrap .items a[data-value="'+id+'"]').parentNode.parentNode;
													if(skuContent != null){
														skuContent.querySelector('h2').innerHTML = vnStr;
													}
												}
											}
										}
									}
								}
							}else if(data.action_name == 'translate_to_cn'){
								var trans = data.extend_data;
								if(trans.length > 0){
									for(var i = 0; i < trans.length; i++){
										if(trans[i] != undefined && trans[i] != ''){
											if(trans[i].substring(0, trans[i].indexOf(':::::')) != ""){ //Item
												var cnStr = trans[i].substring(0, trans[i].indexOf(':::::'));
												var id = trans[i].substring(trans[i].indexOf(':::::') + 5, trans[i].length).replace(/[^0-9]/g, '');
												var skuItem = document.querySelector('.modal-sku-content-item[data-vid="'+id+'"]');
												if(skuItem != null){
													skuItem.innerHTML = cnStr;
												}else{
													//tmall
													var id = trans[i].substring(trans[i].indexOf(':::::') + 5, trans[i].length).replace(/[^0-9\:]/g, '');
													var skuItem = document.querySelector('.sku-list-wrap .items a[data-value="'+id+'"]');
													if(skuItem != null){
														skuItem.innerHTML = cnStr;
													}
												}
											}else if(trans[i].substring(0, trans[i].indexOf('::::')) != ""){ //Title
												var cnStr = trans[i].substring(0, trans[i].indexOf('::::'));
												var id = trans[i].substring(trans[i].indexOf('::::') + 4, trans[i].length).replace(/[^0-9]/g, '');

												var skuContent = document.querySelector('.modal-sku-content[data-pid="'+id+'"]');
												if(skuContent != null){
													skuContent.querySelector('.modal-sku-content-title').innerHTML = cnStr;
												}else{
													//tmall
													var id = trans[i].substring(trans[i].indexOf('::::') + 4, trans[i].length).replace(/[^0-9\:]/g, '');
													var skuContent = document.querySelector('.sku-list-wrap .items a[data-value="'+id+'"]').parentNode.parentNode;
													if(skuContent != null){
														skuContent.querySelector('h2').innerHTML = cnStr;
													}
												}
											}
										}
									}
								}
							}else if(data.action_name == 'save'){
								var item_id = getItemId();
								var item_price = getPriceTaobao();
								var item_title = getItemTitle();
								var seller_name = getSellerName();
								var item_link = getItemLink(item_id);
								var item_image = getItemImage();

								window.ReactNativeWebView.postMessage(JSON.stringify({
									"item_price": item_price,
									"item_title": item_title,
									"item_image": item_image,
									"seller_name": seller_name,
									"item_link": item_link,
									"postType": 'save'
								}))
							}
						}

						function linkClick(){
							var full = true;
							//Taobao
							var props = document.getElementsByClassName('modal-sku-content');
							if(props.length > 0){
								var count_selected = 0;
								for(var i = 0; i < props.length; i++){
									var selected_props = props[i].getElementsByClassName('modal-sku-content-item-active');
									if(selected_props != null && selected_props != 'undefined')
									count_selected += selected_props.length;
								}

								if(count_selected < props.length){
									full = false;
								}
							}

							//Tmall
							if(full == true){
								var props = document.querySelectorAll('.sku-list-wrap .items');
								if(props.length > 0){
									var count_selected = 0;
									for(var i = 0; i < props.length; i++){
										var selected_props = props[i].querySelector('a.checked');
										if(selected_props != null && selected_props != 'undefined')
										count_selected += selected_props.length;
									}

									if(count_selected < props.length){
										full = false;
									}
								}
							}

							if(full != true){
								alert('Bạn chưa chọn đầy đủ thuộc tính của sản phẩm!');
							}else{
								sendDataToRN();
							}
						}

						function clickToTranslate(){
							var txtBtnTranslate = document.getElementById('btnTranslateCn').innerText;
							if(txtBtnTranslate == 'Dịch'){
								document.getElementById('btnTranslateCn').innerText = 'Văn bản gốc';
								window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "clickBtnTranslateCn" }));
							}else{
								document.getElementById('btnTranslateCn').innerText = 'Dịch';
								window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "clickBtnTranslateCn" }));
							}
						}

						function sendDataToRN(){
							var type = 'taobao';
							var item_id = getItemId();
							var seller_id = getSellerId();
							var item_price = getPriceTaobao();
							var item_quantity = getItemQuantity();
							var item_title = getItemTitle();
							var seller_name = getSellerName();
							var color_size_name = getColorSizeName();
							var data_value = getDataValue();
							var outer_id = getOuterId(item_id+data_value);
							var item_link = getItemLink(item_id);
							var item_image = getItemImage();

							window.ReactNativeWebView.postMessage(JSON.stringify({
								"type": type,
								"item_id": item_id,
								"seller_id": seller_id,
								"item_price": item_price,
								"item_quantity": item_quantity,
								"item_title": item_title,
								"item_image": item_image,
								"seller_name": seller_name,
								"color_size_name": color_size_name,
								"data_value": data_value,
								"outer_id": outer_id,
								"item_link": item_link
							}))
						}

						function getItemLink(item_id){
							var item_link = '';
							if(window.location.href.indexOf('taobao.com') > 0){
								item_link = 'https://item.taobao.com/item.html?id='+item_id;
							}

							if(window.location.href.indexOf('tmall.com') > 0 || window.location.href.indexOf('tmall.hk') > 0){
								item_link = 'https://detail.tmall.com/item.htm?id='+item_id;
							}

							return item_link;
						}

						function getOuterId(data_value){
							var outer_id = data_value.replace(/\:/g, '').replace(/\;/g, '').replace(/\ /g, '');
							return outer_id;
						}

						function getDataValue(){
							var data_value = '';
							//Taobao
							var element = document.getElementsByClassName('modal-sku-content-item-active');
							if(element.length > 0){
								for(var i = 0; i < element.length; i++){
									var parent = element[i].parentNode.getAttribute("data-pid");
									var child = element[i].getAttribute("data-vid");
									if(i == 0){
										data_value = parent+':'+child;
									}else{
										data_value += ';'+parent+':'+child;
									}
								}
							}
							//Tmall
							if(data_value == ''){
								var element = document.querySelectorAll('.sku-list-wrap a.checked');
								if(element.length > 0){
									for(var i = 0; i < element.length; i++){
										if(i == 0){
											data_value = element[i].getAttribute('data-value');
										}else{
											data_value += ';'+element[i].getAttribute('data-value');
										}
									}
								}
							}

							return data_value;
						}

						function getColorSizeName(){
							var color_size_name = '';
							//Taobao
							var element = document.getElementsByClassName('modal-sku-content-item-active');
							if(element.length > 0){
								for(var i = 0; i < element.length; i++){
									if(i == 0){
										color_size_name = element[i].textContent;
									}else{
										color_size_name += ';'+element[i].textContent;
									}
								}
							}

							//tmall
							if(color_size_name == ''){
								var element = document.querySelectorAll('.sku-list-wrap a.checked');
								if(element.length > 0){
									for(var i = 0; i < element.length; i++){
										if(i == 0){
											color_size_name = element[i].querySelector('span').textContent;
										}else{
											color_size_name += ';'+element[i].querySelector('span').textContent;
										}
									}
								}
							}

							return color_size_name;
						}

						function getSellerName(){
							var seller_name = '';
							//Taobao
							var element = document.getElementsByClassName('shop-title-text');
							if(element.length > 0){
								seller_name = element[0].textContent;
							}

							//tmall
							if(seller_name == ''){
								var sellerNameElement = document.querySelector('.module-shop .shop-name')
								if(sellerNameElement != null){
									seller_name = sellerNameElement.textContent;
								}
							}

							return seller_name;
						}

						function getItemImage(){
							var item_image = '';

							//taobao
							var thumb = document.querySelector('.modal-sku-image img');
							if(thumb != null){
								item_image = thumb.src;
							}

							//tmall
							if(item_image == ''){
								var thumb = document.querySelector('.sku-wrap img');
								if(thumb != null){
									item_image = thumb.src;
								}
							}
							return item_image;
						}

						function getItemTitle(){
							var item_title = '';

							//taobao
							var element = document.querySelectorAll('.title-wrapper .title');
							if(element.length > 0){
								item_title = element[0].textContent;
							}

							//tmall
							if(item_title == ''){
								var titleElement = document.querySelector('.module-title .main.cell');
								if(titleElement != null){
									item_title = titleElement.textContent;
								}
							}

							return item_title;
						}

						function getItemQuantity(){
							var item_quantity = 0;

							//taobao
							var element = document.getElementsByClassName('sku-number-edit');
							if(element.length > 0){
								item_quantity = element[0].value;
							}

							//tmall
							if(item_quantity == 0){
								var quantityElement = document.querySelector('.number-wrap #number');
								if(quantityElement != null){
									item_quantity = quantityElement.value;
								}
							}

							return item_quantity;
						}

						function getPriceTaobao() {
							var item_price = 0;

							//taobao
							var element = document.getElementsByClassName('modal-sku-title-price');
							if(element.length > 0){
								arrPrice = element[0].innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
								item_price = arrPrice[0];
							}

							//tmall
							if(item_price == 0){
								var priceElement = document.querySelector('.price-wrap .price');
								if(priceElement != null){
									var arrPrice = priceElement.innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
									item_price = arrPrice[0];
								}
							}

							item_price = processPrice(item_price);
							return item_price;
						}

						function getSellerId(){
							var seller_id = '';

							//taobao
							var element = document.getElementsByClassName('shop-link-item');
							if(element.length > 0){
								var href = element[0].href;
								var url = new URL(href);
								seller_id = url.searchParams.get("user_id");
							}

							//tmall
							if(seller_id == ''){
								var moduleShop = document.querySelector('.module-shop');
								if(moduleShop != null){
									seller_id = moduleShop.getAttribute('data-spm');
								}
							}

							return seller_id;
						}

						function getItemId(){
							var item_id = 0;
							var url_string = window.location.href;
							var url = new URL(url_string);
							item_id = url.searchParams.get("id");

							return item_id;
						}

						//ham xu lý gia trong truong hop nguoi dung su dung chuc nang tu dong dich cua Chrome
						function processPrice(price) {
							if(price==null || price==0) return 0;
							if(price.indexOf(',')>0)
							{
								var p=String(price).replace('.','');
								p=p.replace(',','.');
								return parseFloat(p);
							}
							else
							return parseFloat(price);
						}

						var btnWrapper = document.querySelector('.modal-btn-wrapper');
						if(btnWrapper != null){
							btnWrapper.style["display"] = "none";
						}else{
							var footerTrade = document.querySelector('.footer.trade');
							if(footerTrade != null){
								footerTrade.style["display"] = "none";
							}
						}
					}
			}catch(e){
				true;
			}
			true;
		`;

		let js1688 = `
			try{
				var footer = document.querySelector('.detail-footer');
				if(footer != null){
					window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnDisabled", "valuePost" : false }));
					footer.style['display'] = 'none';
				}else{
					window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnDisabled", "valuePost" : true }));
				}

				var inputLoaded = document.getElementById('jsLoaded__');
				if(inputLoaded == null){
					var input = document.createElement("input");
					input.type = "hidden";
					input.id = "jsLoaded__";
					document.querySelector('div').appendChild(input);

					window.addEventListener('message', function(e) {
						eventListenerFromRN(e);
					});

					document.addEventListener('message', function(e) {
						eventListenerFromRN(e);
					});

					function eventListenerFromRN(e){
						var data = JSON.parse(e.data);
						if(data.action_name == 'translate'){
							var itemIdTranslate = getItemId();
							var arrTranslate = [];
							var stSkus = document.querySelector('.sku-1st-prop-box');
							var ndSkus = document.querySelector('.sku-2nd-prop-box');
							if(stSkus != null){
								var stTitle = stSkus.querySelector('.sku-1st-prop-name .prop-name-text').textContent;
								if(stTitle != '' && stTitle != null){
									arrTranslate.push(stTitle + ' ::::1'+ '<span translate="no">'+stTitle+'</span>');
							    }
								var stItems = stSkus.querySelectorAll('.sku-1st-prop-item');
								if(stItems.length > 0){
									for(var i = 0; i < stItems.length; i++){
							        	stItemTitle = stItems[i].getAttribute('data-sku-1st-prop-name');
										if(stItemTitle != '' || stItemTitle != null){
							            	arrTranslate.push(stItemTitle+ '<span translate="no">'+stItemTitle+'</span>');
										}
									}
							    }
							}

							if(ndSkus != null){
								var ndTitle = ndSkus.querySelector('.sku-2nd-prop-name .prop-name-text').textContent;
								if(ndTitle != '' && stTitle != null){
									arrTranslate.push(ndTitle + ' ::::2'+ '<span translate="no">'+ndTitle+'</span>');
							    }
								var ndItems = ndSkus.querySelectorAll('.sku-2nd-prop-item');
								if(ndItems.length > 0){
									for(var i = 0; i < ndItems.length; i++){
							        	ndItemsTitle = ndItems[i].getAttribute('data-sku-2nd-prop-name');
										if(ndItemsTitle != '' || ndItemsTitle != null){
							            	arrTranslate.push(ndItemsTitle+ '<span translate="no">'+ndItemsTitle+'</span>');
										}
									}
							    }
							}

							window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "transToVn", "valuePost" : true, "data": arrTranslate, "itemIdTranslate": itemIdTranslate }));
						}else if(data.action_name == 'order'){
							var skuSelectorComponent = document.querySelector('.sku-model')
							if(skuSelectorComponent != null){
								if(skuSelectorComponent.style.display == 'none'){
									var skuBtn = document.querySelector('.specs-wrapper');
									if(skuBtn != null){
										skuBtn.click();
									}
								}else{
									linkClick();
								}
							}else{
								var skuBtn = document.querySelector('.specs-wrapper');
								if(skuBtn != null){
									skuBtn.click();
								}
							}

							setTimeout(function(){
								var skuSelectorComponent = document.querySelector('.sku-model')
								if(skuSelectorComponent != null){
									if(skuSelectorComponent.style.display != 'none'){
										//Them nut dich vao giao dien
										var btnTranslateCn = document.getElementById('btnTranslateCn');
										if(btnTranslateCn == null){
											var firstNode =  document.querySelector('.m-selector-content.two-sku-status');
											var newElement = document.createElement('div');
											newElement.style.padding = '10px';
											newElement.innerHTML = '<button id="btnTranslateCn" style="color: white; background-color: #00c4f4; padding-left: 10px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; font-size: 14px;  border: 1px solid white ; border-radius: 5px;" type="button" data-spm-anchor-id="a21wu.9600033.0.i2">Dịch</button>';
											var elementParent = firstNode.parentNode;
											elementParent.insertBefore(newElement, firstNode);
											var btnTranslateCn = document.getElementById('btnTranslateCn');
											btnTranslateCn.addEventListener("click", clickToTranslate);
										}

										window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnTranslateDisabled", "valuePost" : false }));
									}else{

										window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "btnTranslateDisabled", "valuePost" : true }));
									}


								}
							}, 500);
						}else if(data.action_name == 'translate_to_vn'){
							var trans = data.extend_data;
							if(trans.length > 0){
								for(var i=0; i < trans.length; i++){
									if(trans[i].translatedText != undefined && trans[i].translatedText != ''){
										//Title
										if(trans[i].translatedText.substring(0, trans[i].translatedText.indexOf('::::')) != ""){
											var vnStr = trans[i].translatedText.substring(0, trans[i].translatedText.indexOf('::::'));
											var id = trans[i].translatedText.substring(trans[i].translatedText.indexOf('::::') + 5, trans[i].translatedText.length).replace(/[^0-9]/g, '');
											if(id == 1 && vnStr != ''){
												document.querySelector('.sku-1st-prop-name .prop-name-text').innerHTML = vnStr;
											}else if(id == 2){
												document.querySelector('.sku-2nd-prop-name .prop-name-text').innerHTML = vnStr;
											}
										}else{
											var startAtt = trans[i].translatedText.indexOf('<span translate="no">');
											var endAtt = trans[i].translatedText.indexOf('</span>');
											var attribute = trans[i].translatedText.substring(startAtt+21, endAtt);
											if(attribute != '' && attribute != null){
												var vnStr = trans[i].translatedText.substring(0, trans[i].translatedText.indexOf('<span'));
												var stSkuItem = document.querySelector('.sku-1st-prop-item[data-sku-1st-prop-name="'+attribute+'"]');
												var ndSkuItem = document.querySelector('.sku-2nd-prop-item[data-sku-2nd-prop-name="'+attribute+'"]');
												if(stSkuItem != null){
													stSkuItem.querySelector('a').innerHTML = vnStr
												}

												if(ndSkuItem != null){
													ndSkuItem.querySelector('.main-text').innerHTML = vnStr
												}
											}
										}
									}
								}
							}
						}else if(data.action_name == 'translate_to_cn'){
							var trans = data.extend_data;
							if(trans.length > 0){
								for(var i = 0; i < trans.length; i++){
									if(trans[i] != undefined && trans[i] != ''){
										//Title
										if(trans[i].substring(0, trans[i].indexOf('::::')) != ""){
											var vnCn = trans[i].substring(0, trans[i].indexOf('::::'));
											var id = trans[i].substring(trans[i].indexOf('::::') + 4, trans[i].length).replace(/[^0-9]/g, '');
											if(id == 1 && vnCn != ''){
												document.querySelector('.sku-1st-prop-name .prop-name-text').innerHTML = vnCn;
											}else if(id == 2){
												document.querySelector('.sku-2nd-prop-name .prop-name-text').innerHTML = vnCn;
											}
										}else{
											var startAtt = trans[i].indexOf('<span translate="no">');
											var endAtt = trans[i].indexOf('</span>');
											var attribute = trans[i].substring(startAtt+21, endAtt);
											if(attribute != '' && attribute != null){
												var cnStr = trans[i].substring(0, trans[i].indexOf('<span'));
												var stSkuItem = document.querySelector('.sku-1st-prop-item[data-sku-1st-prop-name="'+attribute+'"]');
												var ndSkuItem = document.querySelector('.sku-2nd-prop-item[data-sku-2nd-prop-name="'+attribute+'"]');
												if(stSkuItem != null){
													stSkuItem.querySelector('a').innerHTML = cnStr
												}

												if(ndSkuItem != null){
													ndSkuItem.querySelector('.main-text').innerHTML = cnStr
												}
											}
										}
									}
								}
							}
						}else if(data.action_name == 'save'){
							var item_id = getItemId();
							var item_title = getItemTitle();
							var item_image = getItemImage();
							var item_link = getItemLink(item_id);
							var seller_name = getSellerName();
							var item_price = getItemPrice();

							window.ReactNativeWebView.postMessage(JSON.stringify({
								"item_title": item_title,
								"item_image": item_image,
								"item_link": item_link,
								"seller_name": seller_name,
								"item_price": item_price,
								"postType": "save"
							}))
						}
					}

					function clickToTranslate(){
						var txtBtnTranslate = document.getElementById('btnTranslateCn').innerText;
						if(txtBtnTranslate == 'Dịch'){
							document.getElementById('btnTranslateCn').innerText = 'Văn bản gốc';
							window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "clickBtnTranslateCn" }));
						}else{
							document.getElementById('btnTranslateCn').innerText = 'Dịch';
							window.ReactNativeWebView.postMessage(JSON.stringify({ "postType" : "clickBtnTranslateCn" }));
						}
					}

					function testJSON(text){
						if (typeof text!=="string"){
							return false;
						}
						try{
							JSON.parse(text);
							return true;
						}
						catch (error){
							return false;
						}
					}

					//ham xu lý gia trong truong hop nguoi dung su dung chuc nang tu dong dich cua Chrome
					function processPrice(price) {
						if(price==null || price==0) return 0;
						if(price.indexOf(',')>0)
						{
							var p=String(price).replace('.','');
							p=p.replace(',','.');
							return parseFloat(p);
						}
						else
						return parseFloat(price);
					}

					function sendDataToRN(skus){
						var type = 'alibaba';
						var item_id = getItemId();
						var item_title = getItemTitle();
						var item_image = getItemImage();
						var item_link = getItemLink(item_id);
						var seller_id = getSellerId();
						var seller_name = getSellerName();
						var item_price = getItemPrice();
						var item_quantity = getTotalQuantity();
						var price_table = getPriceTable();
						var color_size_name = encodeURIComponent(skus);
						var skus = skus;
						// alert(JSON.stringify({
						// 	"type": type,
						// 	"item_id": item_id,
						// 	"item_title": item_title,
						// 	"item_image": item_image,
						// 	"item_link": item_link,
						// 	"seller_id": seller_id,
						// 	"seller_name": seller_name,
						// 	"item_price": item_price,
						// 	"item_quantity": item_quantity,
						// 	"price_table": price_table,
						// 	"color_size_name": color_size_name,
						// 	"skus": skus,
						// }));
						window.ReactNativeWebView.postMessage(JSON.stringify({
							"type": type,
							"item_id": item_id,
							"item_title": item_title,
							"item_image": item_image,
							"item_link": item_link,
							"seller_id": seller_id,
							"seller_name": seller_name,
							"item_price": item_price,
							"item_quantity": item_quantity,
							"price_table": price_table,
							"color_size_name": color_size_name,
							"skus": skus,
						}))
					}

					function linkClick(){
						var totalQuantity = getTotalQuantity();
						var minQuantity = getMinQuantity();

						if(parseInt(totalQuantity) < parseInt(minQuantity)){
							alert('Số lượng mua tối thiểu là '+minQuantity+' sản phẩm!');
						}else{
							//sendDataToRN();
							getSkus();
						}
					}

					function getSkus(){
						var arrTitle = document.querySelectorAll('.offer-number');
						var skus = '';

						if(arrTitle.length > 0){
						    for(let i = 0; i < arrTitle.length; i++){
						        setTimeout(function(){

						           arrTitle[i].parentNode.click();
						           setTimeout(function(){

							           var sku1stName = arrTitle[i].parentNode.querySelector('.offer-name').textContent;
							            var listSku2nd = document.querySelectorAll('.sku-item-wrapper');
							            for(var j=0; j < listSku2nd.length; j++){
							                var quantity = listSku2nd[j].querySelector('.num-control input.input-number').value;
							                if(quantity > 0){
							                    var sku2ndName = listSku2nd[j].querySelector('.sku-text-name').textContent;
																	if(listSku2nd[j].querySelector('.discountPrice-price') != null){
																		var arrItemPrice = listSku2nd[j].querySelector('.discountPrice-price').innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
																		var itemPrice = arrItemPrice[0];
																	}else{
																		 var itemPrice = getItemPrice();
																	}

							                    if(skus == ''){
							                        skus = sku1stName+';'+sku2ndName+'='+quantity+';'+itemPrice;
							                    }else{
							                        skus += '|'+ sku1stName+';'+sku2ndName+'='+quantity+';'+itemPrice;
							                    }
							                }
							            }
                                        if(i == arrTitle.length - 1){
						          sendDataToRN(skus);
						        }
									}, 20 * (j+1));
						      }, 300 * (i+1));
						    }
						}else{
						    var sku1stName = '';
		            var listSku2nd = document.querySelectorAll('.sku-item-wrapper');

		            for(var j=0; j < listSku2nd.length; j++){
		                var quantity = listSku2nd[j].querySelector('.num-control input.input-number').value;
		                if(quantity > 0){
		                    var sku2ndName = listSku2nd[j].querySelector('.sku-text-name').textContent;
												if(listSku2nd[j].querySelector('.discountPrice-price') != null){
													var arrItemPrice = listSku2nd[j].querySelector('.discountPrice-price').innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
													var itemPrice = arrItemPrice[0];
												}else{
													 var itemPrice = getItemPrice();
												}

		                    if(skus == ''){
		                        skus = sku1stName+';'+sku2ndName+'='+quantity+';'+itemPrice;
		                    }else{
		                        skus += '|'+ sku1stName+';'+sku2ndName+'='+quantity+';'+itemPrice;
		                    }
		                }

										if(j == listSku2nd.length - 1){
											sendDataToRN(skus);
										}
		            }

						}
					}

					function getPriceTable(){
						var price_table = '';

						var pricesElement = document.querySelectorAll('.price-item');
            if(pricesElement.length > 0){
                for(var i = 0; i < pricesElement.length; i++){
                    var priceElement = pricesElement[i].querySelector('.price-color');
                    var quantityRangeElement = pricesElement[i].querySelector('.price-description');
                    var str = priceElement.textContent;
                    var strRange = quantityRangeElement.textContent;
                    var item_price = '';
                    var quantity_range = '';

                    if((str.indexOf('-') != -1)){
                        var prices = str.split('-');
                        if(prices.length > 0){
                            arrItemPrice = prices[1].match(/[0-9]*[\.,]?[0-9]+/g);
                            item_price = arrItemPrice[0];
                        }
                    }else{
                        arrItemPrice = str.match(/[0-9]*[\.,]?[0-9]+/g);
                        item_price = arrItemPrice[0];
                    }

                    item_price = processPrice(item_price);
                    arrQuantityRange = strRange.match(/[0-9]*[\.,-≥]?[0-9]+/g);
                    quantity_range = arrQuantityRange[0];

                    if(item_price != '' && quantity_range != ''){
                        if(price_table == ''){
                            price_table = quantity_range+':'+item_price;
                        }else{
                            price_table += ';'+quantity_range+':'+item_price;
                        }
                    }
                }
            }else{
               	var element = document.querySelectorAll('.sku-price-scale');
                var item_price = '';
                var quantity_range = '';

                if(element.length > 0){
                    var str = element[0].textContent;
                    if((str.indexOf('-') != -1)){
                        var prices = str.split('-');
                        if(prices.length > 0){
                            var arrItemPrice = prices[1].match(/[0-9]*[\.,]?[0-9]+/g);
                            item_price = arrItemPrice[0];
                        }
                    }else{
                        var arrItemPrice = element[element.length - 1].innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
                        item_price = arrItemPrice[0];
                    }

                    if(item_price != ''){
                        var element_extend = document.querySelectorAll('.extend-info-desc');
                        if(element_extend.length > 0){
                            quantity_range = element_extend[0].innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
                            if(quantity_range != ''){
                                 price_table = '≥'+quantity_range+':'+item_price;
                            }
                        }
                    }
                }
            }

						return price_table;
					}

					function getItemPrice(){
						var item_price = 0;
						var element = document.querySelectorAll('.sku-price-scale');

						if(element.length > 0){
                            var str = element[0].textContent;
							if((str.indexOf('-') != -1)){
								var prices = str.split('-');
								if(prices.length > 0){
									var arrItemPrice = prices[1].match(/[0-9]*[\.,]?[0-9]+/g);
									item_price = arrItemPrice[0];
								}
							}else{
								var arrItemPrice = element[element.length - 1].innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
								item_price = arrItemPrice[0];
              }
						}else{
            	var element_price = document.querySelectorAll('.price-item .price-color');
            	if(element_price.length > 0){
								var arrItemPrice = element_price[0].innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
								item_price = arrItemPrice[0];
							}
            }

						return processPrice(item_price);
					}

					function getSellerName(){
						var seller_name = '';
						var element = document.querySelectorAll('.store-trt-title');
						if (element.length > 0) seller_name = element[0].textContent;

						return seller_name + ' (1688.com)';
					}

					function getSellerId(){
						var seller_id = '';
						var scripts = document.getElementsByTagName('script');

						if(scripts.length > 0) {
							for(var script = 0; script < scripts.length; script++) {
								var contentScript = scripts[script].innerHTML;

                var str = contentScript;
                var matchesstr = str.matchAll(/sellerUserId@(.*?)"/g);
                var resultMathch = Array.from(matchesstr, x => x[1]);
                if(resultMathch.length > 0){
                       seller_id = resultMathch[0];
                }
							}
						}

						return seller_id;
					}

					function getItemLink(item_id){
						var item_link = '';
						item_link = 'https://detail.1688.com/offer/'+item_id+'.html';

						return item_link;
					}

					function getItemImage(){
						var item_image = '';

						var element = document.querySelectorAll('.item-wrapepr .rax-image');
						if(element.length > 0){
							item_image = element[0].src;
						}

						return item_image;
					}

					function getItemTitle(){
						var item_title = '';

						var first_element = document.querySelectorAll('.title-first-text');
						if (first_element.length > 0) item_title = first_element[0].textContent;
            var second_element = document.querySelectorAll('.title-second-text');
						if (second_element.length > 0) item_title += second_element[0].textContent;

						return item_title;
					}

					function getItemId(){
						var item_id = 0;
						var url_string = window.location.href
						var url = new URL(url_string);
						item_id = url.searchParams.get("offerId");

						if(item_id == null || item_id == 0){
							var scripts = document.getElementsByTagName('script');

							if(scripts.length > 0) {
								for(var script = 0; script < scripts.length; script++) {
									var contentScript = scripts[script].innerHTML;
									var resultTest = testJSON(contentScript);

									if(resultTest){
										var paser = JSON.parse(contentScript);
										if(paser.offerId != undefined && paser.offerId != ""){
											item_id = paser.offerId;
										}
									}
								}
							}
						}

						return item_id;
					}

					function getMinQuantity(){
						var minQuantity = 1;
						var element = document.querySelectorAll('.unit-text');

						if(element.length > 0){
							var str = element[0].textContent;

							if((str.indexOf('-') != -1)){
								var prices = str.split('-');
								if(prices.length > 0){
									minQuantity = prices[0];
								}
							}else{
								arrMinQuantity = element[0].innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
								minQuantity = arrMinQuantity[0];
							}
						}

	 					return minQuantity;
					}

					function getTotalQuantity(){
						var totalQuantity = 0;

						var element = document.querySelectorAll('.count-total-num');

            arrTotalQuantity = element[0].innerHTML.match(/[0-9]*[\.,]?[0-9]+/g);
            totalQuantity = arrTotalQuantity[0];

						return totalQuantity;
					}
					var btnWrapper = document.querySelector('.m-selector-footer .selector-btn-group');
					if(btnWrapper != null){
						btnWrapper.style["display"] = "none";
					}
				}
			}catch(e){
				true;
			}
			true;
		`;

		if(this.state.title == '1688.com'){
			return js1688;
		}else{
			return taobaoJs;
		}
	}

	onMessage(message){
		var self = this;
		var data = JSON.parse(message.nativeEvent.data);

		if(data != null){
			if(data.item_id != undefined && data.item_id != '' && data.item_id != 0){
				_retrieveData('login_info').then((user_info) => {
					if(data.type == 'alibaba'){
						var price_range = [];
						var price_table = data.price_table.split(";");
						if(price_table.length > 0){
							for(const i in price_table){
								var item = price_table[i].split(':');
								var price = item[1];
								var quantity_range = item[0].split('-');

								if(quantity_range.length == 2){
									var begin = quantity_range[0];
									var end = quantity_range[1];
								}else if(quantity_range.length == 1){
									var begin = quantity_range[0].replace(/[^0-9]/g,'');
									var end = '';
								}

								price_range.push({begin: begin, end: end, price: price});

							}
						}

						var arrNewSkus = [];
						var arrSkus = data.skus.split("|");
						for(const index in arrSkus){
							var itemSkusItem = arrSkus[index].split(";");
							var price = itemSkusItem[2];
							var quantity = itemSkusItem[1].split("=")[1];
							var skus = itemSkusItem[0]+";"+itemSkusItem[1].split("=")[0];

							arrNewSkus.push({ price: price, quantity: quantity, property: skus });
						}

						if(arrNewSkus.length == 0){
							arrNewSkus.push({ price: data.item_price, quantity: item_quantity, property: '' });
						}

						for(const ns in arrNewSkus){
							var postData = {
								title_origin: data.item_title,
								price_origin: arrNewSkus[ns].price,
								price_promotion: arrNewSkus[ns].price,
								property: arrNewSkus[ns].property,
								image_origin: data.item_image,
								image_model: data.item_image,
								item_id: data.item_id,
								site: '1688',
								shop_id: data.seller_id,
								shop_name: data.seller_name,
								quantity: arrNewSkus[ns].quantity,
								is_translate: false,
								send_from_app: 1,
								link_origin: data.item_link,
								price_range: price_range
							};

							this.setState({ loading: true });
							addToCart(postData, user_info.userInfo.access_token).then((result) => {
								if(ns == arrNewSkus.length - 1){
									Alert.alert(
								      "Thông báo",
								      "Thêm thành công sản phẩm vào giỏ hàng!",
								      [
								        {
								          text: "Xem giỏ hàng",
								          onPress: () => this.gotoListCartScreen()
								        },
								        { text: "Tiếp tục mua", onPress: () => this.setState({ loading: false }) }
								      ],
								      { cancelable: false }
								    );
								}
							}).catch((error) => {
								Alert.alert('Thông báo', 'Có lỗi xảy ra!', [{text: 'OK', onPress: () => this.setState({ loading: false }) }], {cancelable: false});
							});
						}


					}else{
						const postData = {
							title_origin: data.item_title,
							price_origin: data.item_price,
							price_promotion: data.item_price,
							property: data.color_size_name,
							image_origin: data.item_image,
							image_model: data.item_image,
							item_id: data.item_id,
							site: data.type,
							shop_id: data.type+'_'+data.seller_id,
							shop_name: data.seller_name,
							quantity: data.item_quantity,
							is_translate: false,
							send_from_app: 1,
							link_origin: data.item_link,
						};

						this.setState({ loading: true });
						addToCart(postData, user_info.userInfo.access_token).then((result) => {
							if(result.success == true){
								Alert.alert(
							      "Thông báo",
							      "Thêm thành công sản phẩm vào giỏ hàng!",
							      [
							        {
							          text: "Xem giỏ hàng",
							          onPress: () => this.gotoListCartScreen()
							        },
							        { text: "Tiếp tục mua", onPress: () => this.setState({ loading: false }) }
							      ],
							      { cancelable: false }
							    );
							}else{
								Alert.alert('Thông báo', 'Có lỗi xảy ra!', [{text: 'OK', onPress: () => this.setState({ loading: false }) }], {cancelable: false});
							}
						}).catch((error) => {
							Alert.alert('Thông báo', 'Có lỗi xảy ra!', [{text: 'OK', onPress: () => this.setState({ loading: false }) }], {cancelable: false});
						});
					}
				});
			}

			if(data.postType == 'save'){
				_retrieveData('login_info').then((user_info) => {
					const postData = {
						customer_id: user_info.userInfo.cid,
						product_data: data,
					};
					this.setState({ loading: true });
					saveFavorite(postData).then((result) => {
						if(result.status == 'success'){
							Alert.alert('Thông báo', 'Lưu sản phẩm thành công!', [{text: 'OK', onPress: () => this.setState({ loading: false }) }], {cancelable: false});
						}else{
							Alert.alert('Thông báo', 'Có lỗi xảy ra!', [{text: 'OK', onPress: () => this.setState({ loading: false }) }], {cancelable: false});
						}
					}).catch((error) => {
						Alert.alert('Thông báo', 'Có lỗi xảy ra!', [{text: 'OK', onPress: () => this.setState({ loading: false }) }], {cancelable: false});
					});
				});
			}

			if(data.postType == 'btnDisabled'){
				this.setState({btnDisabled: data.valuePost});
			}

			if(data.postType == 'btnTranslateDisabled'){
				this.setState({btnTranslateDisabled: data.valuePost});
			}

			if(data.postType == 'transToVn'){
				if(this.state.transToVn || data.itemIdTranslate != this.state.itemIdTranslate){
					if(this.state.transVn != undefined && data.itemIdTranslate == this.state.itemIdTranslate){
						this.setState({ transToVn : !this.state.transToVn });
						this._postMessageToWebview('translate_to_vn', this.state.transVn);
					}else{
						if(data.valuePost && data.data.length > 0){
							if(data.itemIdTranslate != this.state.itemIdTranslate){
								this.setState({transCn: data.data});
							}

							this.setState({ loading: true });
							translateArrayKeyword(data.data).then((result) => {
								this.setState({ loading: false });
								if(result.data.translations != undefined && result.data.translations.length > 0){
									this.setState({
										transToVn : false,
										transVn: result.data.translations,
										itemIdTranslate: data.itemIdTranslate
									});
									//console.log(result.data.translations);
									this._postMessageToWebview('translate_to_vn', result.data.translations);

								}

							}).catch((error) => {
								console.error(`Error is: ${error}`);
							});
						}
					}
				}else{ //Translate to CN
					this.setState({ transToVn : !this.state.transToVn });

					if(this.state.transCn != undefined){
						this._postMessageToWebview('translate_to_cn', this.state.transCn);
					}
				}

			}

			//
			if(data.postType == 'clickBtnTranslateCn'){
				this._postMessageToWebview('translate');
			}
		}
	}

	gotoListCartScreen(){
		this.props.navigation.navigate(MainScreen, {active_screen: 'cart'});
		this.setState({ loading: false });
	}

	_postMessageToWebview(action_name, extend_data = undefined){
		var data = { action_name : action_name, extend_data : extend_data};
		this.webView.postMessage(JSON.stringify(data));
	}

	onNavigationStateChange(navState) {
		this.setState({
			canGoBack: navState.canGoBack
		});
	}

	ActivityIndicatorLoadingView() {
		return (
			<ActivityIndicator
			color="#009688"
			size="large"
			style={styles.ActivityIndicatorStyle}
			/>
		);
	}

	render() {
		return (<View
				style={this.state.visible === true ? styles.stylOld : styles.styleNew}>
					<Loader loading={this.state.loading} />
					<WebView
						ref={(view) => this.webView = view}
						style={styles.WebViewStyle}
						javaScriptEnabled={true}
						javaScriptEnabledAndroid={true}
						injectedJavaScript={this._getJsCode()}
						onMessage={this.onMessage}
						source={{ uri: this.state.url }}
						domStorageEnabled={true}
						renderLoading={this.ActivityIndicatorLoadingView}
						startInLoadingState={true}
						onNavigationStateChange={this.onNavigationStateChange.bind(this)}
						// onLoadStart={() => Platform.OS === 'ios' ? console.log('loading') : this.showSpinner()}
						// onLoad={() => this.hideSpinner()}
						/>
						<View  style={{ flex: 1, flexDirection: 'row', position:'absolute', bottom: 60, zIndex:1, height: 60, width: 200}}>
							{false && !this.state.btnTranslateDisabled && (
							<View style={{flex: 1, justifyContent: 'center'}} >
								<Button style={{alignSelf: 'flex-end', backgroundColor: "#00c4f4"}} rounded success iconRight onPress={() => this._postMessageToWebview('translate')}>
									<Text style={{color: "#FFF", paddingLeft: 10, fontSize: 16, fontWeight: 'bold'}}>Dịch</Text>
									<Icon name='swap' style={{fontSize: 28, paddingRight: 5, color: '#fff'}}/>
								</Button>
							</View>)
							}
						</View>
						<View style={{ flex: 1, flexDirection: 'row', position:'absolute', backgroundColor: '#ececec', bottom: 0, zIndex:1, height: 60, width: Dimensions.get('window').width}}>
							<View style={{ flex: 1, backgroundColor: appConfig.baseColor}}>
								<Button onPress={() => this._postMessageToWebview('order') } disabled={this.state.btnDisabled} full large style={{ backgroundColor: this.state.btnDisabled ? 'gray' : appConfig.baseColor}}>
									<Text style={{ fontSize: 16, color: this.state.btnDisabled ? '#ececec' : '#FFF', fontWeight: 'bold'}}>Thêm vào giỏ hàng</Text>
								</Button>
							</View>
						</View>
				</View>)
  }
}

const styles = StyleSheet.create({
	stylOld: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	styleNew: {
		flex: 1,
	},
	WebViewStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1,
	},
	ActivityIndicatorStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
