import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input, Content, ListItem } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modalbox';
import DatePicker from 'react-native-datepicker'
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListPackage, getListRequestDelivery } from "../../requests/Ship";
import Loader from '../Loader';
import {  MainScreen } from '../../ScreenNames';

var screen = Dimensions.get('window');

class ModalSearch extends Component{
	constructor(props){
		super(props);
		this.state = {
			code: '',
			status: '',
			start: '',
			end: '',
		};
	}

	_search(){
		var params = {
			code: this.state.code,
			status: this.state.status,
			start: this.state.start,
			end: this.state.end,
		}
		this.props.parentView._doSearch(params);
	}

	showModalSearch(){
		this.refs.modalSearch.open();
	}

	closeModalSearch(){
		this.refs.modalSearch.close();
	}

	changeCurStatus(value: string) {
		this.setState({
			status: value
		});
	}

	render(){
		return(
			<Modal
				ref={'modalSearch'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:250,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5}}>
				<Item regular>
					<Input
						style={{fontSize: 13}}
						placeholder="Mã YC"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({code: text})}
						value={this.state.code}
					/>
				</Item>
				<Item picker>
					<Text style={{fontSize: 13}}>Trạng thái: </Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						style={{ width: undefined }}
						selectedValue={this.state.status}
						onValueChange={this.changeCurStatus.bind(this)}
						>
						<Picker.Item label="Tất cả" value="" />
						{Object.keys(this.props.request_delivery_status).map((value, i) => {
							return <Picker.Item key={i} value={value} label={this.props.request_delivery_status[value]} />
						})}
					</Picker>
				</Item>
				<Item style={{paddingVertical: 10, paddingRight: 10}}>
				<DatePicker
					style={{width: '50%', marginRight: 5 }}
					mode="date"
					date={this.state.start}
					placeholder="Thời gian từ ngày"
					format="YYYY-MM-DD"
					confirmBtnText="Đồng ý"
					cancelBtnText="Hủy"
					showIcon={false}
					onDateChange={(date) => {this.setState({start: date})}}
				  />
				  <DatePicker
  					style={{width: '50%', marginLeft: 5}}
  					mode="date"
					date={this.state.end}
  					placeholder="Tới ngày"
  					format="YYYY-MM-DD"
  					confirmBtnText="Đồng ý"
  					cancelBtnText="Hủy"
  					showIcon={false}
  					onDateChange={(date) => {this.setState({end: date})}}
  				  />
				</Item>

				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this._search() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 14}}>Tìm kiếm</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class ModalListPackage extends Component{
	constructor(props){
		super(props);
		this.state = {
			tableHead: ['Mã kiện', 'Mã đơn', 'Cân nặng', 'Ngày tạo'],
      		tableData: [],
		}
	}

	showModalistPackage(packages){
		var tableData = [];
		this.refs.modalListPackage.open();

		for(const i in packages){
			tableData.push([packages[i].logistic_package_barcode, packages[i].has_order ? packages[i].order.code : '', packages[i].calculate_weight, packages[i].created_at])
		}

		this.setState({ tableData: tableData });

	}

	closeModalistPackage(){
		this.refs.modalListPackage.close();
	}

	render(){
		return(
			<Modal
				ref={'modalListPackage'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:200,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
				<View style={{flex: 1, borderRadius: 5, padding: 5}}>
					<Content>
		            	<Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
				          <Row data={this.state.tableHead} flexArr={[1, 1.3, 1, 1]} style={{ height: 40, backgroundColor: '#f1f8ff' }} textStyle={{ margin: 6 }}/>
				          <Rows data={this.state.tableData} flexArr={[1, 1.3, 1, 1]} textStyle={{ margin: 6 }}/>
				        </Table>
					</Content>
				</View>
			</Modal>
		);
	}
}

class FlatListItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			btn_status_bgcolor: appConfig.baseColor
		}
	}

	_viewListPackage(){
		this.props.parentFlatList.viewListPackage(this.props.item.packages);
	}

	componentDidMount(){
		var color = appConfig.baseColor;
		if(this.props.item.status == 'CANCEL'){
			color = 'red';
		}

		if(this.props.item.status == 'RECEIVED'){
			color = 'green';
		}

		if(this.props.item.status == 'CREATE'){
			color = appConfig.baseColor;
		}

		this.setState({ btn_status_bgcolor: color });
	}

	render(){
		return(
			<View style={{ padding: 10, margin: 5, backgroundColor: 'white', borderRadius: 10}}>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 14, color: appConfig.baseColor }}>Mã</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 14, color: appConfig.baseColor }}>{this.props.item.code}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Trạng thái</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{fontSize: 13, textAlign: 'center', justifyContent: 'center', backgroundColor: this.state.btn_status_bgcolor, padding: 5, borderRadius: 5, color: 'white'}}>{this.props.item.status_title}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Số kiện</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<TouchableOpacity onPress={() => this._viewListPackage() }>
							<Text style={{fontSize: 13, textAlign: 'center', justifyContent: 'center', padding: 5, borderRadius: 5, color: appConfig.baseColor, borderWidth: 1, borderColor: appConfig.baseColor }}>{this.props.item.packages.length} kiện</Text>
						</TouchableOpacity>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Cân nặng</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.weight} kg</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Ship / COD</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.shipping_fee == null ? '-' : this.props.item.shipping_fee} / {this.props.item.cod == null ? '-' : this.props.item.cod}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Thời gian</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.created_at}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Địa chỉ</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.user_address.reciver_name}, {this.props.item.user_address.reciver_phone}, {this.props.item.user_address.detail}, {this.props.item.user_address.district_label}, {this.props.item.user_address.province}</Text>
					</View>
				</View>
			</View>
		)
	}
}

export default class ListRequestDelivery extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			per_page: 10,
			page: 1,
			lastpage: false,
			total: 0,
			data: [],
			request_delivery_status: [],
			code: '',
			status: '',
			start: '',
			end: ''
		}
	}
	_onPressSearch(){
		this.refs.modalSearch.showModalSearch();
	}

	freshdataRequestDelivery(){
		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				code: this.state.code,
				status: this.state.status,
				start: this.state.start,
				end: this.state.end,
			};
			if(this.state.lastpage) return;
			this.setState({ 
				isLoading: true,
				request_delivery_status: user_info.userInfo.request_delivery_status
			});
			getListRequestDelivery(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ data: [] });
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	_doSearch(params){
		this.setState({
			per_page: 10,
			page: 1,
			lastpage: false,
			total: 0,
			data: [],
			code: params.code,
			status: params.status,
			start: params.start,
			end: params.end,
		});

		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				code: params.code,
				status: params.status,
				start: params.start,
				end: params.end,
			};

			if(this.state.lastpage) return;
			this.setState({ isLoading: true });
			getListRequestDelivery(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				//Dong modal search
				this.refs.modalSearch.closeModalSearch();
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ 
						data: [],
						total: 0
				 	});
				}
				
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshdataRequestDelivery();
	}

	viewListPackage(packages){
		this.refs.modalListPackage.showModalistPackage(packages);
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<Button style={{ justifyContent: 'flex-start', backgroundColor: '#ececec'}} onPress={() => this._onPressSearch() } iconLeft full light>
					<Text style={{ fontSize: 13, color: appConfig.baseColor }}>{this.state.total} (Yêu cầu)</Text>
					<Icon style={{ color: appConfig.baseColor }} name='search' />
					<Text style={{ color: appConfig.baseColor, fontWeight: 'bold', fontSize: 14 }}>Tìm kiếm</Text>
				</Button>
				<FlatList
					data={this.state.data}
					renderItem={({item, index}) => {
						return(
							<FlatListItem request_delivery_status={this.state.request_delivery_status} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.id.toString()}
					onEndReached={() => {this.freshdataRequestDelivery()}}
					onEndReachedThreshold={0.1}
					extraData={this.state.data}
				>

				</FlatList>
				<ModalSearch request_delivery_status={this.state.request_delivery_status} ref={'modalSearch'} parentView={this}></ModalSearch>
				<ModalListPackage ref={'modalListPackage'} parentView={this}></ModalListPackage>
			</View>
		)
	}
}
