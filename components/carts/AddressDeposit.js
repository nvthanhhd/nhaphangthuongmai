import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input, Thumbnail, Content } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, Image, TouchableOpacity } from 'react-native';
import NumberFormat from 'react-number-format';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListCartDepositView, cartDeposit } from "../../requests/Cart";
import Loader from '../Loader';
import { MainScreen, ListAddressScreen, ListOrderScreen } from '../../ScreenNames';
import _ from 'lodash';

var screen = Dimensions.get('window');

export default class AddressDeposit extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			str_data: this.props.navigation.getParam('data'),
			isLoading: false,
			carts: [],
			user_address: [],
			province_title: '',
			district_title: '',
			result: []
		}
	}

	freshData(){
		_retrieveData('login_info').then((user_info) => {
			this.setState({ 
				isLoading: true,
				all_provinces: user_info.userInfo.all_provinces,
				all_districtsall: user_info.userInfo.all_districts
			});

			getListCartDepositView(this.state.str_data, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					var index_province = _.findIndex(user_info.userInfo.all_provinces, {'id': result.user_address.province_id });
					var index_district = _.findIndex(user_info.userInfo.all_districts[result.user_address.province_id], {'id': result.user_address.district_id });
					this.setState({
						carts: result.carts,
						user_address: result.user_address,
						province_title: user_info.userInfo.all_provinces[index_province].label,
						district_title: user_info.userInfo.all_districts[result.user_address.province_id][index_district].label,
						result: result
					});
				}else{
					this.setState({ carts: [] });
				}


			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	changeAddressDefault(){
		this.props.navigation.navigate(ListAddressScreen);
	}

	_deposit(){
		_retrieveData('login_info').then((user_info) => {
			var postData = {
				data: this.state.str_data,
				address_id: this.state.user_address.id
			}

			this.setState({ isLoading: true });
			cartDeposit(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					Alert.alert('Thông báo', 'Đã đặt cọc thành công '+result.order_id.length+' đơn hàng');
					this.props.navigation.navigate(ListOrderScreen);
				}else{
					Alert.alert('Thông báo', 'Đặt cọc thất bại!');
				}


			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.willFocus = this.props.navigation.addListener('willFocus', () => {
	    	this.freshData();
	    });
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<Content>
					<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, borderBottomWidth: 0}}>
						<View style={{flex: 1}}>
						<View style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{ flex: 1, padding: 10 }}>
								<Text style={{ fontSize: 14, fontWeight: 'bold' }}>Địa chỉ nhận hàng</Text> 
							</View>
							<View style={{ flex: 1, padding: 10, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
								<TouchableOpacity onPress={() => this.changeAddressDefault() }>
									<Text style={{ fontSize: 14, color: appConfig.baseColor }}>Thay đổi</Text>
								</TouchableOpacity>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
								<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-end'}}>
									<Icon name="person" style={{color: 'gray', fontSize: 20}}/>
									<Text style={{fontSize: 14}}>{this.state.user_address.reciver_name}</Text>
								</View>
							</View>
							<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
								<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-end'}}>
									<Icon name="call" style={{color: 'gray', fontSize: 20}}/>
									<Text style={{fontSize: 14}}>{this.state.user_address.reciver_phone}</Text>
								</View>
							</View>
							<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
								<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-end'}}>
									<Icon name="home" style={{color: 'gray', fontSize: 20}}/>
									<Text style={{fontSize: 14}}>{this.state.user_address.detail}, {this.state.district_title}, {this.state.province_title}</Text>
								</View>
							</View>
						</View>
						</View>
					</Item>
					<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, borderBottomWidth: 0}}>
						<View style={{flex: 1}}>
							<View style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
								<View style={{ flex: 1, padding: 10 }}>
									<Text style={{ fontSize: 14, fontWeight: 'bold' }}>Thông tin đơn hàng</Text> 
								</View>
							</View>
							<View style={{flex: 1, padding: 10, borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
								<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
									<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
										<Text style={{fontSize: 14}}>Người bán</Text>
									</View>
									<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
										<NumberFormat value={this.state.result.total_carts} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 14 }}>{value}</Text>}/>
									</View>
								</View>
								<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
									<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
										<Text style={{fontSize: 14}}>Tổng số link</Text>
									</View>
									<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
										<NumberFormat value={this.state.result.total_link} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 14 }}>{value}</Text>}/>
									</View>
								</View>
								<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
									<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
										<Text style={{fontSize: 14}}>Phí dịch vụ (tạm tính)</Text>
									</View>
									<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
										<NumberFormat value={this.state.result.total_buying_fee} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 14 }}>{value} đ</Text>}/>
									</View>
								</View>
								<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
									<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
										<Text style={{fontSize: 14}}>Tạm tính ({this.state.result.total_quantity} sản phẩm)</Text>
									</View>
									<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
										<NumberFormat value={this.state.result.total_amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 14 }}>{value} đ</Text>}/>
									</View>
								</View>
								<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
									<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
										<Text style={{fontSize: 14}}>Đặt cọc {this.state.result.deposit_percent} %</Text>
									</View>
									<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
										<NumberFormat value={this.state.result.deposit_amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 14, fontWeight: 'bold' }}>{value} đ</Text>}/>
									</View>
								</View>
								<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
									<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
										<Text style={{fontSize: 14}}>Số dư tài khoản</Text>
									</View>
									<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
										<NumberFormat value={this.state.result.account_balance} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 14 }}>{value} đ</Text>}/>
									</View>
								</View>
								{!this.state.result.can_deposit && this.state.result.need_amount < 0 ? (
									<View>
										<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
											<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
												<Text style={{fontSize: 14}}>Bạn còn thiếu</Text>
											</View>
											<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
												<NumberFormat value={Math.abs(this.state.result.need_amount)} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 14, color: 'red' }}>{value} đ</Text>}/>
											</View>
										</View>
										<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 10, alignItems: 'center'}}>
											<Text style={{fontWeight: 'bold', fontSize: 16, color: 'red'}}>Vui lòng nạp thêm tiền vào tài khoản</Text>
										</View>
									</View>
								) : (<View></View>)}
								{this.state.result.can_deposit && (
									<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 10, alignItems: 'center'}}>
										 <Button block small style={{ backgroundColor: appConfig.baseColor }} onPress={() => this._deposit() }>
								            <Text>Đặt cọc ngay</Text>
								          </Button>
									</View>
								)}
							</View>
						</View>
					</Item>
				</Content>
			</View>
		)
	}
}
