import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input, Thumbnail } from 'native-base';
import { Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, Image, TouchableOpacity } from 'react-native';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListSaveFavorite, removeFavorite } from "../../requests/Cart";
import Loader from '../Loader';
import { DetailWebOrderScreen, MainScreen } from '../../ScreenNames';

class FlatListItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			show: true
		}
	}

	_deleteThisProduct(){
		Alert.alert('Thông báo', 'Bạn có muốn xoá sản phẩm này?', [
			{text: 'No', onPress: () => console.log('Canceled'), style: 'cancel'},
			{text: 'Yes', onPress: () => {
				_retrieveData('login_info').then((user_info) => {
					const postData = { id: this.props.item.id };

					removeFavorite(postData, user_info.userInfo.access_token).then((result) => {
						if(result.success == true){
							this.setState({ show: false });
						}
					}).catch((error) => {
						console.error(`Error is: ${error}`);
					});
				});
			}}
		]);
	}

	_gotoOrder(){
		var url = this.props.item.link;
		var title = "";
		var item_id = "";

		if(url.indexOf('taobao.com') > -1){
			title = 'taobao.com';
			var item_id = this._getIdFromUrl(url);
		}else if(url.indexOf('tmall.com') > -1){
			title = 'tmall.com';
			var item_id = this._getIdFromUrl(url);
		}else if(url.indexOf('1688.com') > -1){
			title = '1688.com';
		}

		if(item_id != "" && item_id != null && item_id != undefined){
			url = "https://m.intl.taobao.com/detail/detail.html?id="+item_id;
		}

		this.props.navigation.navigate(DetailWebOrderScreen, {title: title, url: url });
	}

	_getIdFromUrl(url){
		let regex = /[?&]([^=#]+)=([^&#]*)/g,
			params = {},
			match;

			match = regex.exec(url);

			return match[2];
	}

	_getImageSite(site){
		switch(site){
			case 'tmall':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/tmall_icon.png')}></Image>);
				break;
			case 'taobao':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/taobao_icon.png')}></Image>);
				break;
			case '1688':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/1688_icon.png')}></Image>);
				break;
			default:
				return (<Image style={{width: 20, height: 20}} source={require('../../images/1688_icon.png')}></Image>);
		}
	}

	render(){
		if(!this.state.show){
			return (<View></View>);
		}

		return(
			<View style={{ flex: 1, flexDirection: 'row', padding: 10, margin: 5, backgroundColor: 'white', borderRadius: 10}}>
				<TouchableOpacity style={{ flex: 1 }} onPress={() => this._gotoOrder()}>
					<Thumbnail style={{borderWidth: 1, borderColor: '#ececec'}} square source={{ uri: this.props.item.avatar }} />
				</TouchableOpacity>
				<View style={{flex: 4}}>
					<TouchableOpacity style={{flex: 1, paddingBottom: 5}} onPress={() => this._gotoOrder() }>
						<Text style={{fontSize: 13}}>{this._getImageSite(this.props.item.site)} - {this.props.item.product_name}</Text>
					</TouchableOpacity>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<View style={{flex: 1}}>
							<Button style={{alignItems: 'center', justifyContent: 'center'}} small primary danger onPress={() => {this._deleteThisProduct() }}>
								<Label style={{ fontSize: 13, color: 'white' }}>Xoá</Label>
							</Button>
						</View>
						<View style={{flex: 4}}>
						</View>
					</View>
				</View>
			</View>
		)
	}
}

export default class ListFavorite extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			per_page: 10,
			page: 1,
			lastpage: false,
			data: [],
			total: 0
		}
	}

	freshDataProducts(){
		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
			};

			if(this.state.lastpage) return;
			this.setState({ isLoading: true });
			getListSaveFavorite(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.data.data == null || result.data.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data.data),
						per_page: this.state.per_page,
						page: this.state.page + 1,
						total: result.total,
						lastpage: result.data.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ data: [] });
				}

			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshDataProducts();
	}

	render(){
		if(this.state.total == 0){
			return(
				<View style={{backgroundColor: '#f1f1f1', flex: 1, justifyContent: 'center', alignItems: 'center'}}>
					<Text>Không có sản phẩm nào được lưu</Text>
				</View>
			)
		}

		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<FlatList
					data={this.state.data}
					renderItem={({item, index}) => {
						return(
							<FlatListItem navigation={this.props.navigation} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.id.toString()}
					onEndReached={() => {this.freshDataProducts()}}
					onEndReachedThreshold={0.1}
					extraData={this.state.data}
				>

				</FlatList>
			</View>
		)
	}
}
