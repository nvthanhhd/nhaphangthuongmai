import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input, Thumbnail, CheckBox, Content, StyleProvider, getTheme } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, Image, TouchableOpacity } from 'react-native';
import NumberFormat from 'react-number-format';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListCart, removeShop, removeShopItem, saveNoteShop, saveNoteItem, saveFavorite, changeQuantity, checkServiceShop
		, changeShippingTranspTypeName, changeServiceInsure, saveShippingAmount, saveFreightBill } from "../../requests/Cart";
import { AddressDepositScreen } from '../../ScreenNames';
import Loader from '../Loader';
import NumericInput from 'react-native-numeric-input';
import RadioGroup from 'react-native-radio-buttons-group';
import Modal from 'react-native-modalbox';
import _ from 'lodash';

var screen = Dimensions.get('window');
var list_checked = [];

class ModalNoteItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			note: '',
		};
	}

	saveNoteItem(){
		this.props.parentView.saveNoteItem(this.state.note);
		this.closeModalNoteItem();
	}

	showModalNoteItem(note){
		this.setState({ note: note });
		this.refs.modalNoteItem.open();
	}

	closeModalNoteItem(){
		this.refs.modalNoteItem.close();
	}

	render(){
		return(
			<Modal
				ref={'modalNoteItem'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 40,
					height:60,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5, flexDirection: 'row' }}>
				<Item regular style={{flex: 3, marginVertical: 5, borderRadius: 5}}>
					<Input
						style={{fontSize: 13 }}
						placeholder="Ghi chú sản phẩm"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({note: text})}
						value={this.state.note}
					/>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this.saveNoteItem() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 5,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 13 }}>Lưu</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class ModalNoteShop extends Component{
	constructor(props){
		super(props);
		this.state = {
			note: '',
		};
	}

	saveNoteShop(){
		this.props.parentView.saveNoteShop(this.state.note);
		this.closeModalNoteShop();
	}

	showModalNoteShop(note){
		this.setState({ note: note });
		this.refs.modalNoteShop.open();
	}

	closeModalNoteShop(){
		this.refs.modalNoteShop.close();
	}

	render(){
		return(
			<Modal
				ref={'modalNoteShop'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: screen.width/2,
					height:160,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5, paddingTop: 10}}>
				<Item regular>
					<Input
						style={{padding: 5}}
						placeholder="Ghi chú"
						returnKeyType='next'
						autoCorrect={false}
						multiline={true}
    					numberOfLines={3}
						onChangeText={(text) => this.setState({note: text})}
						value={this.state.note}
					/>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this.saveNoteShop() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: 'white'}}>Lưu</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class ShopItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			show: true,
			favorite_id: this.props.item.favorite_id,
			note: this.props.item.note,
			quantity: this.props.item.quantity,
			price_item_vnd: this.props.item.price_calculator_vnd,
			price_item: this.props.item.price_calculator,
			amount_item: this.props.item.amount,
			amount_item_vnd: this.props.item.amount_vnd,
			checked_item: true
		}
	}

	componentDidMount() {
		this.props.onRef(this)
	}

	UNSAFE_componentWillUnmount() {
		this.props.onRef(null)
	}

	_checkedItem(checked_item){
		this.setState({ checked_item: checked_item});
		var shop_id = this.props.parentView.props.item.shop_id;
		var index_shop = _.findIndex(list_checked, { 'shop_id' : shop_id });

		if(checked_item != this.state.checked_item){
			if(index_shop >= 0){
			if(!checked_item){
				var index_item = _.findIndex(list_checked[index_shop].cart_items, {'item_id': this.props.item.id });
				delete list_checked[index_shop].cart_items.splice(index_item, 1);

				//Nếu xoá hết item trong shop thì xoá luôn shop
				if(list_checked[index_shop].cart_items.length == 0){
					delete list_checked.splice(index_shop, 1);
				}

				this.props.parentView.setState({ checked_shop: false });
				this.props.parentView.props.parentFlatList.setState({ checked_all_shop: false });
			}else{
				list_checked[index_shop].cart_items.push({ item_id: this.props.item.id, amount_vnd: this.state.amount_item_vnd });

				if(list_checked[index_shop].cart_items.length == this.props.parentView.state.cart_items.length){
					this.props.parentView.setState({ checked_shop: true });
					//
					if(list_checked.length == this.props.parentView.props.parentFlatList.state.carts.length){
						this.props.parentView.props.parentFlatList.setState({
							checked_all_shop: true
						});
					}
				}
			}
		}else{ //trường hợp ko có shop thì chỉ có add chứ ko có remove item
			if(checked_item){
				//Tạo shop
				list_checked.push({ shop_id: shop_id, cart_items: [{ item_id: this.props.item.id, amount_vnd: this.state.amount_item_vnd }]});
				var index_shop = _.findIndex(list_checked, { 'shop_id' : shop_id });
				if(list_checked[index_shop].cart_items.length == this.props.parentView.state.cart_items.length){
					this.props.parentView.setState({ checked_shop: true });
					//
					if(list_checked.length == this.props.parentView.props.parentFlatList.state.carts.length){
						this.props.parentView.props.parentFlatList.setState({
							checked_all_shop: true
						});
					}
				}
			}
		}

		this.props.parentView.props.parentFlatList.resetTotalAmount();
		}
	}

	showModalNoteItem(note){
		this.refs.modalNoteItem.showModalNoteItem(note);
	}

	saveNoteItem(note){
		_retrieveData('login_info').then((user_info) => {
			var postData = {
				id: this.props.item.id,
				note: note,
			}

			saveNoteItem(postData, user_info.userInfo.access_token).then((result) => {
				if(result.success == true){
					this.setState({ note: note });
				}else{
					Alert.alert('Thông báo', 'Không thể lưu ghi chú', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	_removeShopItem(){
		Alert.alert('Thông báo', 'Bạn có muốn xoá sản phẩm này?', [
			{text: 'No', onPress: () => console.log('Canceled'), style: 'cancel'},
			{text: 'Yes', onPress: () => {
				_retrieveData('login_info').then((user_info) => {
					const postData = { item_id: this.props.item.id };

					removeShopItem(postData, user_info.userInfo.access_token).then((result) => {
						if(result.success == true){
							var shop_id = this.props.parentView.props.item.shop_id;
							//Remove item from list_checked
							if(this.state.checked_item){
								var index_shop = _.findIndex(list_checked, { 'shop_id' : shop_id });

								if(index_shop >= 0){
									var index_item = _.findIndex(list_checked[index_shop].cart_items, {'item_id': this.props.item.id });
									delete list_checked[index_shop].cart_items.splice(index_item, 1);

									//Nếu xoá hết item trong shop thì xoá luôn shop
									if(list_checked[index_shop].cart_items.length == 0){
										delete list_checked.splice(index_shop, 1);
										//this.props.parentView.setState({ show: false });
									}
									this.props.parentView.props.parentFlatList.resetTotalAmount();
								}
							}

							//Remove item from state carts
							var carts = this.props.parentView.props.parentFlatList.state.carts;
							var i_shop = _.findIndex(carts, { 'shop_id' : shop_id});
							var i_item = _.findIndex(carts[i_shop].cart_items, {'id' : this.props.item.id });
							delete carts[i_shop].cart_items.splice(i_item, 1);

							if(carts[i_shop].cart_items.length == 0){
								delete carts.splice(i_shop, 1);
							}
							//this.setState({ show: false });
							this.props.parentView.props.parentFlatList.setState({carts : carts});
						}else{
							Alert.alert('Không xoá được sản phẩm này!');
						}
					}).catch((error) => {
						console.error(`Error is: ${error}`);
					});
				});
			}}
		]);
	}

	saveFavorite(){
		Alert.alert('Thông báo', 'Bạn có muốn '+(this.state.favorite_id == null ? 'lưu' : 'bỏ lưu')+' sản phẩm này?', [
			{text: 'No', onPress: () => console.log('Canceled'), style: 'cancel'},
			{text: 'Yes', onPress: () => {
				_retrieveData('login_info').then((user_info) => {
					var postData = {
						item_id: this.props.item.id,
						type: this.state.favorite_id == null ? 'ADD' : 'REMOVE'
					};

					if(this.state.favorite_id != null){
						postData.favorite_id = this.state.favorite_id;
					}

					saveFavorite(postData, user_info.userInfo.access_token).then((result) => {
						if(result.success == true){
							this.setState({favorite_id: result.favorite_id});
						}else{
							Alert.alert('Không '+(this.state.favorite_id == null ? 'lưu' : 'bỏ lưu')+' được sản phẩm này!');
						}
					}).catch((error) => {
						console.error(`Error is: ${error}`);
					});
				});
			}}
		]);
	}

	_changeQuantity(quantity){
		_retrieveData('login_info').then((user_info) => {
			const postData = {
				item_id: this.props.item.id,
				quantity: quantity
			};

			changeQuantity(postData, user_info.userInfo.access_token).then((result) => {
				if(result.success == true){
					this.setState({
						quantity: quantity,
						price_item_vnd: result.price_item_vnd,
						price_item: result.price_item,
						amount_item: result.amount_item,
						amount_item_vnd: result.amount_item_vnd
					});

					var shopInfo = {
						amount_vnd: result.amount_shop_vnd,
						fixed_vnd: result.fixed_fee_vnd,
						buying_vnd: result.buying_fee_vnd,
						checking_vnd: result.checking_fee_vnd,
						total_amount_vnd: result.total_amount_shop_vnd,
						total_order_quantity: result.total_order_quantity,
						total_link: result.total_link
					}

					var shop_id = this.props.parentView.props.item.shop_id;
					var index_shop = _.findIndex(list_checked, { 'shop_id' : shop_id });
					if(this.state.checked_item){
						if(index_shop >= 0){
							var index_item = _.findIndex(list_checked[index_shop].cart_items, {'item_id': this.props.item.id });
							delete list_checked[index_shop].cart_items.splice(index_item, 1);

							list_checked[index_shop].cart_items.push({ item_id: this.props.item.id, amount_vnd: result.amount_item_vnd });
						}
					}

					this.props.parentView.changeFeeInfo(shopInfo);
				}else{
					Alert.alert('Không thể thay đổi số lượng!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
	}

	render(){
		if(!this.state.show){
			return (<View></View>);
		}

		return (
			<View style={{flex: 1, paddingVertical: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
				<View style={{flex: 0.5, justifyContent: 'center', alignItems: 'flex-start', marginRight: 5}}>
					<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.checked_item} color={appConfig.baseColor} onPress={() => this._checkedItem(!this.state.checked_item) }/>
				</View>
				<View style={{ flex: 1.2, padding: 10}}>
					<View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
						<Thumbnail style={{ borderWidth: 1, borderColor: '#ececec'}} square source={{uri: this.props.item.image }} />
					</View>
					<View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginTop: 10}}>
						<NumericInput initValue={parseInt(this.state.quantity)} totalWidth={65} totalHeight={35} type='plus-minus' minValue={1} rounded={true} onChange={value => this._changeQuantity(value)} />
					</View>
				</View>
				<View style={{flex: 5, paddingHorizontal: 5, justifyContent: 'center', alignItems: 'flex-start'}}>
					<View style={{flex: 1}}>
						<Text style={{fontSize: 13}}>{this.props.item.title}</Text>
					</View>
					<View style={{flex: 1, marginTop: 5}}>
						<Text style={{fontSize: 13, color: 'gray'}}>{this.props.item.property}</Text>
					</View>
					<View style={{flex: 1, marginTop: 5}}>
						<Text style={{fontSize: 13}}>{this.state.quantity} x <NumberFormat value={this.state.price_item_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/> ({this.state.price_item} ¥)</Text>
					</View>
					<View style={{flex: 1, marginTop: 5}}>
						<Text style={{fontSize: 13}}><NumberFormat value={this.state.amount_item_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/> ({this.state.amount_item} ¥)</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
						<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-end'}}>
							<TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} onPress={() => this._removeShopItem()} >
								<Icon name="trash" style={{fontSize: 20, color: 'red' }} />
								<Text style={{ color : 'red', fontSize: 13 }}>Xoá</Text>
							</TouchableOpacity>
						</View>
						<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-end'}}>
							<TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} onPress={() => this.showModalNoteItem(this.state.note)}>
								<Icon name="edit" type="FontAwesome" style={{fontSize: 20, color: appConfig.baseColor }} />
								<Text style={{ color : appConfig.baseColor, fontSize: 13 }}>Ghi chú</Text>
							</TouchableOpacity>
						</View>
						<View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end'}}>
							<TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} onPress={() => this.saveFavorite()}>
								<Icon name="heart" style={{fontSize: 20, color: this.state.favorite_id == null ? 'gray' : appConfig.baseColor }} />
								<Text style={{ color : this.state.favorite_id == null ? 'gray' : appConfig.baseColor, fontSize: 13 }}>{this.state.favorite_id == null ? 'Lưu' : 'Bỏ lưu'}</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
				<ModalNoteItem ref={'modalNoteItem'} parentView={this}></ModalNoteItem>
			</View>
		)
	}
}

class FlatListItem extends Component{
	constructor(props){
		super(props);
		const radio_transport = [
			{
				label: 'Thường',
				value: 'NORMAL',
				selected: this.props.item.shipping_transport_type_name == 'NORMAL' ? true : false,
				size: 18

			},
			{
				label: 'Nhanh',
				value: 'FAST',
				selected: this.props.item.shipping_transport_type_name == 'FAST' ? true : false,
				size: 18
			}
		];
		this.state = {
			isLoading: false,
			checking: _.indexOf(this.props.item.services, 'CHECKING') >= 0 ? true : false,
			wood_crating: _.indexOf(this.props.item.services, 'WOOD_CRATING') >= 0 ? true : false,
			surf: _.indexOf(this.props.item.services, 'SURF') >= 0 ? true : false,
			shipping_china_vietnam: _.indexOf(this.props.item.services, 'SHIPPING_CHINA_VIETNAM') >= 0 ? true : false,
			china_vietnam_fast: _.indexOf(this.props.item.services, 'CHINA_VIETNAM_FAST') >= 0 ? true : false,
			shipping_china_vietnam_saving: _.indexOf(this.props.item.services, 'SHIPPING_CHINA_VIETNAM_SAVING') >= 0 ? true : false,
			china_vietnam_transp: _.indexOf(this.props.item.services, 'CHINA_VIETNAM_TRANSP') >= 0 ? true : false,
			normal: undefined,
			fast: undefined,
			radio_transport: radio_transport,
			show: true,
			note: this.props.item.note,
			amount_vnd: this.props.item.amount_vnd,
			fixed_vnd: this.props.item.fixed_vnd,
			buying_vnd: this.props.item.buying_vnd,
			checking_vnd: this.props.item.checking_vnd,
			total_amount_vnd: this.props.item.total_amount_vnd,
			shipping_transport_type_name: this.props.item.shipping_transport_type_name,
			service_insure: this.props.item.service_insure,
			shipping_amount: this.props.item.shipping_amount != null ? this.props.item.shipping_amount.toString() : '',
			freight_bill: this.props.item.freight_bill,
			checked_shop: true,
			cart_items: this.props.item.cart_items,
			total_link: this.props.item.total_link,
			total_order_quantity: this.props.item.total_quantity
		}
	}

	_getImageSite(site){
		switch(site){
			case 'tmall':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/tmall_icon.png')}></Image>);
				break;
			case 'taobao':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/taobao_icon.png')}></Image>);
				break;
			case '1688':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/1688_icon.png')}></Image>);
				break;
			default:
				return (<Image style={{width: 20, height: 20}} source={require('../../images/1688_icon.png')}></Image>);
		}
	}

	componentDidMount() {
		this.props.onRef(this)
	}

	UNSAFE_componentWillUnmount() {
		this.props.onRef(null)
	}

	_checkedShop(checked_shop){
		this.checkAllItemInShop(checked_shop);
	}

	showModalNoteShop(note){
		this.refs.modalNoteShop.showModalNoteShop(note);
	}

	saveNoteShop(note){
		_retrieveData('login_info').then((user_info) => {
			var postData = {
				id: this.props.item.id,
				note: note,
			}

			saveNoteShop(postData, user_info.userInfo.access_token).then((result) => {
				if(result.success == true){
					this.setState({ note: note });
				}else{
					Alert.alert('Thông báo', 'Không thể lưu ghi chú', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	_removeShop(){
		Alert.alert('Thông báo', 'Bạn có muốn xoá shop này?', [
			{text: 'No', onPress: () => console.log('Canceled'), style: 'cancel'},
			{text: 'Yes', onPress: () => {
				_retrieveData('login_info').then((user_info) => {
					const postData = {
						shop_id: this.props.item.shop_id,
						id: this.props.item.id
					};
					this.props.parentFlatList.setState({isLoading: true});
					removeShop(postData, user_info.userInfo.access_token).then((result) => {
						if(result.success == true){
							this.props.parentFlatList.setState({isLoading: false});
							//After remove shop
							var index_shop = _.findIndex(list_checked, {'shop_id' : this.props.item.shop_id });
							if(index_shop >= 0){
								var cart_items = list_checked[index_shop].cart_items;
								delete list_checked.splice(index_shop, 1);

								this.props.parentFlatList.resetTotalAmount();
							}

							//
							var carts = this.props.parentFlatList.state.carts;
							var index = _.findIndex(carts, { 'shop_id' : this.props.item.shop_id});
							delete carts.splice(index, 1);

							this.props.parentFlatList.setState({carts : carts});
							//this.setState({ show: false });
						}else{
							Alert.alert('Không xoá được shop này!');
						}
					}).catch((error) => {
						console.error(`Error is: ${error}`);
					});
				});
			}}
		]);
	}

	changeFeeInfo(info){
		this.setState({
			amount_vnd: info.amount_vnd,
			fixed_vnd: info.fixed_vnd,
			buying_vnd: info.buying_vnd,
			checking_vnd: info.checking_vnd,
			total_amount_vnd: info.total_amount_vnd,
			total_link: info.total_link != undefined ? info.total_link : this.state.total_link,
			total_order_quantity: info.total_order_quantity != undefined ? info.total_order_quantity : this.state.total_order_quantity
		});

		this.props.parentFlatList.resetTotalAmount();
	}

	changeCheckServices(service_code, is_check){
		_retrieveData('login_info').then((user_info) => {
			const postData = {
				cart_id: this.props.item.id,
				service_code: service_code,
				checkbox: is_check ? 'check' : 'uncheck'
			};
			this.setState({ isLoading: true });
			checkServiceShop(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					_.indexOf(result.services, 'CHECKING') >= 0 ? this.setState({ checking: true }) : this.setState({ checking: false });
					_.indexOf(result.services, 'WOOD_CRATING') >= 0 ? this.setState({ wood_crating: true }) : this.setState({ wood_crating: false });
					_.indexOf(result.services, 'SURF') >= 0 ? this.setState({ surf: true }) : this.setState({ surf: false });
					_.indexOf(result.services, 'SHIPPING_CHINA_VIETNAM') >= 0 ? this.setState({ shipping_china_vietnam: true }) : this.setState({ shipping_china_vietnam: false });
					_.indexOf(result.services, 'CHINA_VIETNAM_FAST') >= 0 ? this.setState({ china_vietnam_fast: true }) : this.setState({ china_vietnam_fast: false });
					_.indexOf(result.services, 'SHIPPING_CHINA_VIETNAM_SAVING') >= 0 ? this.setState({ shipping_china_vietnam_saving: true }) : this.setState({ shipping_china_vietnam_saving: false });
					if(_.indexOf(result.services, 'CHINA_VIETNAM_TRANSP') >= 0){
						this.setState({ china_vietnam_transp: true });
						this.changeShippingTranspTypeName(this.state.shipping_transport_type_name);
					}else{
						this.setState({ china_vietnam_transp: false });
					}

					var infoFee = {
						amount_vnd: result.fee.amount_shop_vnd,
						fixed_vnd: result.fee.fixed_fee_vnd,
						buying_vnd: result.fee.buying_fee_vnd,
						checking_vnd: result.fee.checking_fee_vnd,
						total_amount_vnd: result.fee.total_amount_shop_vnd
					}

					this.changeFeeInfo(infoFee);
				}else{
					Alert.alert('Không thể thực hiện thao tác này!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
	}

	changeShippingTranspTypeName(type_name){
		_retrieveData('login_info').then((user_info) => {
			const postData = {
				cart_id: this.props.item.id,
				value: type_name,
			};
			this.setState({ isLoading: true });
			changeShippingTranspTypeName(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					this.setState({ shipping_transport_type_name: type_name});
				}else{
					Alert.alert('Không thể thực hiện thao tác này!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
	}

	changeServiceInsure(service_insure){
		_retrieveData('login_info').then((user_info) => {
			const postData = {
				cart_id: this.props.item.id,
				check: service_insure == 1 ? 'check' : 'uncheck',
			};
			this.setState({ isLoading: true });
			changeServiceInsure(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					this.setState({ service_insure: service_insure});
				}else{
					Alert.alert('Không thể thực hiện thao tác này!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
	}

	saveShippingAmount(){
		_retrieveData('login_info').then((user_info) => {
			const postData = {
				cart_id: this.props.item.id,
				amount: this.state.shipping_amount,
			};
			this.setState({ isLoading: true });
			saveShippingAmount(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					Alert.alert('Thông báo', 'Cập nhật thành công', [{text: 'OK' }], {cancelable: false});
				}else{
					Alert.alert('Không thể thực hiện thao tác này!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
	}

	saveFreightBill(){
		_retrieveData('login_info').then((user_info) => {
			const postData = {
				cart_id: this.props.item.id,
				freight_bill: this.state.freight_bill,
			};
			this.setState({ isLoading: true });
			saveFreightBill(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					Alert.alert('Thông báo', 'Cập nhật thành công', [{text: 'OK' }], {cancelable: false});
				}else{
					Alert.alert('Không thể thực hiện thao tác này!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
	}

	checkAllItemInShop(checked_shop){
		this.props.item.cart_items.map((item, index) => {
			this[`shopitem${index}`]._checkedItem(checked_shop);
		})

		this.setState({checked_shop: checked_shop });
		this.props.parentFlatList.resetTotalAmount();
	}

	render(){
		if(!this.state.show){
			return (<View></View>);
		}

		return(
			<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, borderBottomWidth: 0}}>
				<Loader loading={this.state.isLoading} />
				<View style={{ flex: 1}}>
					<View style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
						<View style={{flex: 0.5, justifyContent: 'center', alignItems: 'flex-start', marginRight: 5}}>
							<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.checked_shop} color={appConfig.baseColor} onPress={() => this.checkAllItemInShop(!this.state.checked_shop) }/>
						</View>
						<View style={{flex: 7.2, padding: 10}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13, color: appConfig.baseColor }}>{this._getImageSite(this.props.item.site)} {this.props.item.shop_name}</Text>
							</View>
							<View style={{ flex: 1, color: 'gray' }}>
								<Text style={{ color: 'gray', fontSize: 13 }}>Số lượng: ({this.state.total_order_quantity}) sản phẩm / ({this.state.total_link}) link </Text>
							</View>
						</View>
					</View>
					{this.props.item.cart_items.map((item, index) => (
		            	<ShopItem onRef={ref => (this[`shopitem${index}`] = ref)} key={index} item={item} parentView={this} />
					))}
					<View style={{flex: 1, paddingVertical: 10, borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
						<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>

								<View style={{flex: 0.5}}>
									<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.checking} onPress={() => this.changeCheckServices('CHECKING', !this.state.checking)} color="gray"/>
								</View>
								<View style={{flex: 1 }}>
									<Text style={{ fontSize: 13 }}>Kiểm hàng</Text>
								</View>
							</View>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
								<View style={{flex: 0.5}}>
									<CheckBox style={{borderRadius: 100, height: 18, width: 18 }} checked={this.state.wood_crating} onPress={() => this.changeCheckServices('WOOD_CRATING', !this.state.wood_crating)} color="gray"/>
								</View>
								<View style={{flex: 1 }}>
									<Text style={{ fontSize: 13,  }}>Đóng gỗ</Text>
								</View>
							</View>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
								<View style={{flex: 0.5}}>
									<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.surf} onPress={() => this.changeCheckServices('SURF', !this.state.surf)} color="gray"/>
								</View>
								<View style={{flex: 1 }}>
									<Text style={{ fontSize: 13 }}>Đóng xốp</Text>
								</View>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
								<View style={{flex: 0.5}}>
									<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.shipping_china_vietnam } onPress={() => this.changeCheckServices('SHIPPING_CHINA_VIETNAM', !this.state.shipping_china_vietnam)} color="gray"/>
								</View>
								<View style={{flex: 1 }}>
									<Text style={{ fontSize: 13 }}>CPT</Text>
								</View>
							</View>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
								<View style={{flex: 0.5}}>
									<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.china_vietnam_fast } onPress={() => this.changeCheckServices('CHINA_VIETNAM_FAST', !this.state.china_vietnam_fast)} color="gray"/>
								</View>
								<View style={{flex: 1 }}>
									<Text style={{ fontSize: 13 }}>CPN</Text>
								</View>
							</View>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
								<View style={{flex: 0.5}}>
									<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.shipping_china_vietnam_saving} onPress={() => this.changeCheckServices('SHIPPING_CHINA_VIETNAM_SAVING', !this.state.shipping_china_vietnam_saving)} color="gray"/>
								</View>
								<View style={{flex: 1 }}>
									<Text style={{ fontSize: 13 }}>CPTK</Text>
								</View>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
								<View style={{flex: 0.5}}>
									<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.china_vietnam_transp} onPress={() => this.changeCheckServices('CHINA_VIETNAM_TRANSP', !this.state.china_vietnam_transp)} color="gray"/>
								</View>
								<View style={{flex: 1 }}>
									<Text style={{ fontSize: 13 }}>Ship hộ</Text>
								</View>
							</View>
							{this.state.china_vietnam_transp ?
								(
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
								<View style={{flex: 0.5}}>
									<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.service_insure == 0 ? false : true} onPress={() => this.changeServiceInsure(this.state.service_insure == 0 ? 1 : 0)} color="gray"/>
								</View>
								<View style={{flex: 1 }}>
									<Text style={{ fontSize: 13 }}>Bảo hiểm</Text>
								</View>
							</View>
							) : (<View style={{flex: 1, flexDirection: 'row'}}></View>)}
							<View style={{flex: 1, flexDirection: 'row'}}></View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start'}}>
								{this.state.china_vietnam_transp ? (
								<RadioGroup radioButtons={this.state.radio_transport} flexDirection='row' onPress={(data) => this.changeShippingTranspTypeName(data.find(e => e.selected == true).value)} />) : (<View></View>)
								}
							</View>
						</View>
						{(this.state.service_insure == 0 ? false : true) && this.state.china_vietnam_transp ?
						(<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
							<View style={{flex: 5}}>
								<Input
									style={{borderRadius: 5, borderColor: 'gray', borderWidth: 0.5, fontSize: 13, marginHorizontal: 5}}
									placeholder="Giá trị đơn hàng để tính phí bảo hiểm"
									keyboardType="number-pad"
									returnKeyType='go'
									autoCorrect={false}
									onChangeText={(text) => this.setState({shipping_amount: text})}
									value={this.state.shipping_amount}
								/>
							</View>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
								<Button primary onPress={() => this.saveShippingAmount()}>
									<Text style={{fontSize: 13}}>Lưu</Text>
								</Button>
							</View>
						</View>) : (<View></View>)
						}
						{this.state.china_vietnam_transp ?
						(<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
							<View style={{flex: 5}}>
								<Input
									style={{borderRadius: 5, borderColor: 'gray', borderWidth: 0.5, fontSize: 13, marginHorizontal: 5}}
									placeholder='Nhập mã vận đơn, cách nhau bở dấu ","'
									keyboardType="number-pad"
									returnKeyType='go'
									autoCorrect={false}
									onChangeText={(text) => this.setState({freight_bill: text})}
									value={this.state.freight_bill}
								/>
							</View>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
								<Button primary onPress={() => this.saveFreightBill()}>
									<Text style={{fontSize: 13}}>Lưu</Text>
								</Button>
							</View>
						</View>) : (<View></View>)
						}
					</View>
					<View style={{flex: 1, paddingVertical: 10, borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
						<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Tiền hàng</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.state.amount_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Phí cố định</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.state.fixed_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Phí dịch vụ</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.state.buying_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Phí kiểm đếm</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.state.checking_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Phí vận chuyển</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.props.item.shipping_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, paddingHorizontal: 5, paddingVertical: 2, flexDirection: 'row'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13, fontWeight: 'bold'}}>Tổng tiền</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.state.total_amount_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, fontWeight: 'bold' }}>{value} đ</Text>}/>
							</View>
						</View>
					</View>
					<View style={{flex: 1, paddingVertical: 10, borderBottomWidth: 1, borderBottomColor: '#ececec', flexDirection: 'row'}}>
						<View style={{flex: 1, paddingLeft: 5}}>
							<TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} onPress={() => this._removeShop()}>
								<Icon name="trash" style={{fontSize: 18, color: 'red' }} />
								<Text style={{ color : 'red', fontSize: 13 }}>Xoá shop</Text>
							</TouchableOpacity>
						</View>
						<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-end'}}>
							<TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} onPress={() => this.showModalNoteShop(this.state.note)}>
								<Icon name="edit" type="FontAwesome" style={{fontSize: 18, color: appConfig.baseColor }} />
								<Text style={{ color : appConfig.baseColor, fontSize: 13 }}>Ghi chú</Text>
							</TouchableOpacity>
						</View>
						<View style={{flex: 1, paddingRight: 5, alignItems: 'flex-end'}}>
							<TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} onPress={() => this.props.parentFlatList._goToDeposit(this.props.item.shop_id )} >
								<Icon name="paper-plane" style={{fontSize: 18, color: appConfig.baseColor }} />
								<Text style={{ color : appConfig.baseColor, fontSize: 13, fontWeight: 'bold' }}>ĐẶT HÀNG NGAY</Text>
							</TouchableOpacity>
						</View>
					</View>
				 </View>
				<ModalNoteShop ref={'modalNoteShop'} parentView={this}></ModalNoteShop>
			</Item>
		)
	}
}

export default class ListCart extends Component{
	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			per_page: 10,
			page: 1,
			lastpage: false,
			carts: [],
			checked_all_shop: true,
			total_amount: 0,
		}
	}

	freshDataCart(){
		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
			};

			if(this.state.lastpage) return;
			this.setState({
				isLoading: true,
			});
			getListCart(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.carts == null || result.carts.length == 0 || this.state.lastpage) return;
					this.setState({
						carts: this.state.carts.concat(result.carts),
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.carts.length < this.state.per_page ? true : false
					});

					this.changeTotalAmount(result.carts);

				}else{
					this.setState({ carts: [] });
				}


			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshDataCart();
	}

	changeTotalAmount(carts, total_amount, list_shop_checked){
		var total_amount = total_amount == undefined ? this.state.total_amount : total_amount;
		var list_shop_checked = list_shop_checked == undefined ? list_checked : list_shop_checked;
		for(const i in carts){
			total_amount = total_amount + carts[i].amount_vnd
			var cart_items = [];
			for(const j in carts[i].cart_items){
				cart_items.push({ item_id: carts[i].cart_items[j].id, amount_vnd: carts[i].cart_items[j].amount_vnd});

			}
			list_shop_checked.push({ shop_id: carts[i].shop_id, cart_items: cart_items});
		}

		list_checked = list_shop_checked;

		this.setState({
			total_amount: total_amount,
		});
	}

	checkAllShop(){
		this.state.carts.map((item, index) => {
			this[`shop${index}`]._checkedShop(!this.state.checked_all_shop);
		})

		this.setState({checked_all_shop: !this.state.checked_all_shop });
		this.resetTotalAmount();
	}

	resetTotalAmount(){
		var total_amount = 0;
		for(const i in list_checked){
			for(const j in list_checked[i].cart_items){
				total_amount = list_checked[i].cart_items[j].amount_vnd + total_amount;
			}
		}

		this.setState({ total_amount: total_amount });
	}

	_goToDeposit(shop_id = ''){
		var arrDeposit = [];
		if(shop_id != ''){
			var index_shop = _.findIndex(list_checked, {'shop_id': shop_id});
			if(index_shop >= 0){
				for(const i in list_checked[index_shop].cart_items){
					arrDeposit.push(list_checked[index_shop].shop_id + '|' + list_checked[index_shop].cart_items[i].item_id);
				}
			}else{
				Alert.alert('Thông báo', 'Vui lòng chọn ít nhất 1 sản phẩm để đặt hàng');
				return;
			}
		}else{
			for(const i in list_checked){
				for(const j in list_checked[i].cart_items){
					arrDeposit.push(list_checked[i].shop_id + '|' + list_checked[i].cart_items[j].item_id);
				}
			}
		}

		if(arrDeposit.join(',') == ''){
			Alert.alert('Thông báo', 'Vui lòng chọn ít nhất 1 sản phẩm để đặt hàng');
			return;
		}else{
			this.props.navigation.navigate(AddressDepositScreen, {data: arrDeposit.join(',')});
		}
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<FlatList
					data={this.state.carts}
					renderItem={({item, index}) => {
						return(
							<FlatListItem onRef={ref => (this[`shop${index}`] = ref)} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.id.toString()}
					onEndReached={() => {this.freshDataCart()}}
					onEndReachedThreshold={0.1}
					extraData={this.state.carts}
				>

				</FlatList>
				<View style={{height: 50, backgroundColor: '#e6f7ff'}}>
				<View style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
					<View style={{ flex: 2, flexDirection: 'row'}}>
						<View style={{flex: 2, justifyContent: 'center', alignItems: 'flex-start', marginRight: 5}}>
							<CheckBox style={{borderRadius: 100, height: 18, width: 18}} checked={this.state.checked_all_shop} color={appConfig.baseColor} onPress={() => this.checkAllShop() }/>
						</View>
						<View style={{flex: 7}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
								<Text style={{fontSize: 13 }}>Chọn tất cả</Text>
							</View>
						</View>
					</View>
					<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center'}}>
						<NumberFormat value={this.state.total_amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 14, fontWeight: 'bold', color: appConfig.baseColor }}>{value} đ</Text>}/>
					</View>
					<View style={{flex: 2, justifyContent: 'center', alignItems: 'flex-end', marginRight: 5}}>
						<Button primary small onPress={() => this._goToDeposit() }>
							<Text style={{fontSize: 14}}>Đặt hàng</Text>
						</Button>
					</View>
				</View>
				</View>
			</View>
		)
	}
}
