import React, {Component} from 'react';
import {
  View,
  Text,
  Item,
  Input,
  Icon,
  Container,
  Content,
  Button,
  Left,
  Right,
} from 'native-base';
import {
  StyleSheet,
  Image,
  TextInput,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  Dimensions,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {postDataLogin} from '../requests/User';
import {_retrieveData, _storeData, getExpireDate} from '../Helpers';
import {appConfig} from '../AppConfig';
import {
  LoginScreen,
  RegistrationScreen,
  MainScreen,
  ProfileScreen,
  EditProfileScreen,
  ListOrderScreen,
  ListTransactionScreen,
  ListPackageScreen,
  ListFavoriteScreen,
  ListAddressScreen,
  ListRequestDeliveryScreen,
  ListComplainScreen,
  PagePayScreen,
  PagePriceScreen,
  CreateRequestDeliveryScreen,
  DetailComplainScreen,
  DetailOrderScreen,
  CreateComplainScreen,
  SearchProductScreen,
  DetailWebOrderScreen,
  ListCartScreen,
  AddressDepositScreen,
} from '../ScreenNames';
import Registration from './Registration';
import Main from './MainScreen';
import Profile from './Profile';
import EditProfile from './EditProfile';
import PagePay from './PagePay';
import PagePrice from './PagePrice';

import ListTransaction from './wallet/ListTransaction';
import ListPackage from './ships/ListPackage';
import ListRequestDelivery from './ships/ListRequestDelivery';
import CreateRequestDelivery from './ships/CreateRequestDelivery';
import ListFavorite from './carts/ListFavorite';
import ListCart from './carts/ListCart';
import AddressDeposit from './carts/AddressDeposit';
import ListAddress from './users/ListAddress';
import ListOrder from './orders/ListOrder';
import DetailOrder from './orders/DetailOrder';
import ListComplain from './orders/ListComplain';
import DetailComplain from './orders/DetailComplain';
import CreateComplain from './orders/CreateComplain';
import Loader from './Loader';
import Notifications from '../Notifications';

class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      txtEmail: '', //nd.bvduy@gmail.com
      txtPassword: '', //1
      txtErrorLogin: '',
      showPassword: false,
    };
  }

  onSubmitFormLogin() {
    if (this.state.txtEmail == '') {
      this.setState({txtErrorLogin: 'Vui lòng nhập vào email hoặc username'});
      return false;
    }

    if (this.state.txtPassword == '') {
      this.setState({txtErrorLogin: 'Vui lòng nhập vào mật khẩu'});
      return false;
    }

    //Post data to server
    const postData = {
      email: this.state.txtEmail,
      password: this.state.txtPassword,
      fcm_token: this.refs.notifications.state.fcmToken,
    };
    console.log(postData);
    this.setState({loading: true});
    postDataLogin(postData)
      .then(result => {
        this.setState({loading: false});
        if (result.error == undefined) {
          this.setState({txtErrorLogin: ''});
          const login_key = 'login_info';
          const login_info = {
            expireTime: getExpireDate(result.expires_in / 60),
            userInfo: result,
          };

          //Set AsyncStorage Login
          const login_key_value = JSON.stringify(login_info);
          _storeData(login_key, login_key_value);

          //Set AsyncStorage content_notity
          const content_notity = JSON.stringify({
            content_notify: result.content_notify,
          });
          _storeData('content_notify', content_notity);
          this.props.navigation.navigate(MainScreen, {
            navigation: this.props.navigation,
          });
        } else {
          this.setState({txtErrorLogin: 'Sai tài khoản hoặc mật khẩu'});
        }
      })
      .catch(error => {
        Alert.alert(
          'Thông báo',
          'Có lỗi xảy ra trong quá trình đăng nhập',
          [{text: 'OK', onPress: () => this.setState({loading: false})}],
          {cancelable: false},
        );
      });
  }

  UNSAFE_componentWillMount() {
    _retrieveData('login_info').then(user_info => {
      if (user_info != undefined && user_info != null) {
        this.props.navigation.navigate(MainScreen, {
          navigation: this.props.navigation,
        });
      }
    });
  }

  componentDidUpdate() {
    //this.refs.notifications.getDeviceToken();
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} enabled>
        <Notifications ref={'notifications'} />
        <Loader loading={this.state.loading} />
        <View style={styles.logoContainer}>
          <Image
            onPress={Keyboard.dismiss}
            style={styles.logo}
            source={require('../images/logo_nhtm.png')}
          />
        </View>
        <View style={styles.infoContainer}>
          <Item style={styles.item}>
            <Icon active style={{opacity: 0.5}} name="person" />
            <TextInput
              style={{height: 40, width: 350}}
              placeholder='Tài khoản hoặc email'
              keyboardType="email-address"
              returnKeyType='next'
              autoCorrect={false}
              onChangeText={text => this.setState({txtEmail: text})}
              value={this.state.txtEmail}
              onSubmitEditing={() => this.refs.txtPassword.focus()}
            />
          </Item>
          <Item style={styles.item}>
            <Icon active style={{opacity: 0.5}} name="lock" />
            <TextInput
              style={{height: 40, width: Dimensions.get('window').width - 70}}
              placeholder='Mật khẩu'
              returnKeyType='go'
              secureTextEntry={this.state.showPassword ? false : true}
              onChangeText={text => this.setState({txtPassword: text})}
              value={this.state.txtPassword}
              ref={'txtPassword'}
            />
            <Icon
              active
              name={this.state.showPassword ? 'eye-off' : 'eye'}
              style={{left: -5, opacity: 0.8}}
              onPress={() =>
                this.setState({showPassword: !this.state.showPassword})
              }
            />
          </Item>
          <Text style={{marginTop: 5, color: 'red'}}>
            {this.state.txtErrorLogin}
          </Text>
          <Button
            onPress={this.onSubmitFormLogin.bind(this)}
            block
            style={{marginTop: 10, backgroundColor: appConfig.baseColor}}>
            <Text>Đăng nhập</Text>
          </Button>
          <View style={{flexDirection: 'row'}}>
            <Button style={{marginLeft: 'auto'}} transparent>
              <Text
                onPress={() =>
                  this.props.navigation.navigate(RegistrationScreen)
                }
                style={{color: appConfig.baseColor}}>
                Đăng ký
              </Text>
            </Button>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === 'ios' ? 0 : 50,
    backgroundColor: 'white',
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 86,
    height: 86,
  },
  infoContainer: {
    flex: 2,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 30,
  },
  item: {
    paddingTop: 10,
  },
});

export const RootStack = createStackNavigator(
  {
    LoginScreen: {
      screen: LoginView,
      navigationOptions: {
        headerShown: false,
      },
    },
    RegistrationScreen: {
      screen: Registration,
      navigationOptions: {
        title: 'Đăng ký thành viên',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    MainScreen: {
      screen: Main,
      navigationOptions: {
        headerShown: false,
        //  gesturesEnabled: false,
      },
    },
    ProfileScreen: {
      screen: Profile,
      navigationOptions: {
        headerShown: false,
      },
    },
    EditProfileScreen: {
      screen: EditProfile,
      navigationOptions: {
        title: 'Chỉnh sửa thông tin cá nhân',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    ListOrderScreen: {
      screen: ListOrder,
      navigationOptions: {
        title: 'Danh sách đơn hàng',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    ListTransactionScreen: {
      screen: ListTransaction,
      navigationOptions: {
        title: 'Lịch sử giao dịch',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    ListPackageScreen: {
      screen: ListPackage,
      navigationOptions: {
        title: 'Danh sách kiện hàng',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    ListRequestDeliveryScreen: {
      screen: ListRequestDelivery,
      navigationOptions: {
        title: 'Danh sách yêu cầu giao hàng',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    CreateRequestDeliveryScreen: {
      screen: CreateRequestDelivery,
      navigationOptions: {
        title: 'Tạo yêu cầu giao hàng',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    ListFavoriteScreen: {
      screen: ListFavorite,
      navigationOptions: {
        title: 'Sản phẩm yêu thích',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    ListAddressScreen: {
      screen: ListAddress,
      navigationOptions: {
        title: 'Địa chỉ của bạn',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    ListComplainScreen: {
      screen: ListComplain,
      navigationOptions: {
        title: 'Danh sách khiếu nại',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    DetailComplainScreen: {
      screen: DetailComplain,
      navigationOptions: {
        title: 'Chi tiết khiếu nại',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    CreateComplainScreen: {
      screen: CreateComplain,
      navigationOptions: {
        title: 'Tạo khiếu nại',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    DetailOrderScreen: {
      screen: DetailOrder,
      navigationOptions: {
        title: 'Chi tiết đơn hàng',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    PagePayScreen: {
      screen: PagePay,
      navigationOptions: {
        title: 'Thanh toán',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    PagePriceScreen: {
      screen: PagePrice,
      navigationOptions: {
        title: 'Biểu phí',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    ListCartScreen: {
      screen: ListCart,
      navigationOptions: {
        title: 'Giỏ hàng',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
    AddressDepositScreen: {
      screen: AddressDeposit,
      navigationOptions: {
        title: 'Địa chỉ và đặt cọc',
        headerTitleStyle: {
          textAlign: 'center',
          borderColor: '#ff0000',
          color: 'white',
        },
        headerStyle: {backgroundColor: appConfig.baseColor},
        headerTitleAlign: 'center',
      },
    },
  },
  {
    initialRouteName: LoginScreen,
  },
);

const AppContainer = createAppContainer(RootStack);

export default class Login extends Component {
  render() {
    return <AppContainer />;
  }
}
