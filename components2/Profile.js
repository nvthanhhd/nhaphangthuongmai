import React, { Component } from 'react';
import { View, TouchableHighlight } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Content, List, ListItem, Text, Separator, Left, Right, Icon, Button, Thumbnail, Body } from 'native-base';
import { LoginScreen, EditProfileScreen, ListOrderScreen, ListTransactionScreen, ListPackageScreen, ListFavoriteScreen, 
		ListRequestDeliveryScreen, ListComplainScreen, ListAddressScreen, PagePayScreen, PagePriceScreen, CreateRequestDeliveryScreen  } from '../ScreenNames';
import { _clear, _retrieveData } from '../Helpers';
import { appConfig } from "../AppConfig";
import { postLogout, getUserInfo } from "../requests/User";
import NumberFormat from 'react-number-format';

export default class Profile extends Component {
	constructor(props){
		super(props);
		this.state = {
			logout: false,
			user_info: {},
			hotline_home: '',
			fcmToken: ''
		};
	}

	componentDidMount(){
		_retrieveData('login_info').then((user_info) => {
			getUserInfo(user_info.userInfo.access_token).then((result) => {
				if(result.error == undefined){
					this.setState({
						user_info: result.me,
						hotline_home: result.hotline_home,
					});
				}else{
					Alert.alert('Thông báo', 'Có lỗi trong quá trình lấy dữ liệu!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
		//this.props.mainView.refreshDataHome();
	}

	async _logOut(){
		let fcm_token = await AsyncStorage.getItem('fcmToken');
		_retrieveData('login_info').then((user_info) => {
			var postData = { fcm_token: fcm_token, access_token: user_info.userInfo.access_token };
			postLogout(postData).then((result) => {
				console.log('Da xoa fcmToken');
			}).catch((error) => {
				console.error('Khong xoa dc fcmToken!');
			});
		});

		this.setState({ logout: true });
	}

	render(){
		if(this.state.logout){
			_clear().then(() => {
				this.props.navigation.navigate(LoginScreen);
			});
		}

		return(
			<View style={{flex: 1}}>
				<View style={{flex: 1}}>
				<Content>
					<ListItem last onPress={() => this.props.navigation.navigate(EditProfileScreen, {user_info: this.state.user_info })}>
			        	<View style={{flex: 1}}>
			        		<View style={{ flex: 1, flexDirection: 'row'}}>
								<View style={{ flex: 1, paddingHorizontal: 10, paddingVertical: 5}}>
									<Thumbnail source={{uri: this.state.user_info.avatar_thumbnail_url}} />
								</View>
								<View style={{ flex: 8, flexDirection: 'row'}}>
									<View style={{flex: 5, alignItems: 'flex-start', paddingLeft: 20}}>
										<View style={{flex: 1}}><Text style={{fontSize: 14}}>{this.state.user_info.name}</Text></View>
										<View style={{flex: 1}}><Text style={{fontSize: 13}}>{this.state.user_info.email}</Text></View>
										<View style={{flex: 1}}><Text style={{fontSize: 13}}>{this.state.user_info.username} | {this.state.user_info.code}</Text></View>
										
									</View>
									<View style={{flex: 1, right: -20, justifyContent: 'center', alignItems: 'center'}}>
										<Icon active name="angle-right" type="FontAwesome"/>
									</View>
								</View>
							</View>
							
			        	</View> 
			        </ListItem>
			        <ListItem last>
			        	<View style={{flex: 1, flexDirection: 'row'}}>
							<View style={{flex: 2, borderRightWidth: 2, borderRightColor: '#ececec'}}>
								<View style={{flex: 1}}>
									<NumberFormat value={this.state.user_info.account_balance} displayType={'text'} thousandSeparator={true} renderText={value => <Text style={{ color: appConfig.baseColor, fontWeight: 'bold', fontSize: 13}}>{value} đ</Text>}/>
								</View>
								<View style={{flex: 1}}>
									<Text style={{fontSize: 13}}>Số dư tài khoản</Text>
								</View>
							</View>
							<View style={{flex: 2, borderRightWidth: 2, borderRightColor: '#ececec'}}>
								<View style={{flex: 1}}>
									<NumberFormat value={this.state.user_info.tong_giao_dich_tich_luy} displayType={'text'} thousandSeparator={true} renderText={value => <Text style={{ color: appConfig.baseColor, fontWeight: 'bold', fontSize: 13}}>{value} đ</Text>}/>
								</View>
								<View style={{flex: 1}}>
									<Text style={{fontSize: 13}}>Tổng GD tích lũy</Text>
								</View>
							</View>
							<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
								<Icon style={{fontSize: 18, color: '#F2A446' }} name="star" />
								<Text style={{fontSize: 13, fontWeight: 'bold', color: '#F2A446', textTransform: 'uppercase' }}> {this.state.user_info.level_name}</Text>
							</View>
						</View>
			        </ListItem>
					<Separator >
		            	<Text style={{fontSize: 14, color: 'black', fontWeight: 'bold'}}>Quản lý nhập hàng</Text>
		          	</Separator>
			        <ListItem last onPress={() => this.props.navigation.navigate(ListOrderScreen, {navigation: this.props.navigation})}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="copy" />
							<Text style={{ fontSize: 13 }}>Danh sách đơn hàng</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
		         	<ListItem last onPress={() => this.props.navigation.navigate(ListComplainScreen, {navigation: this.props.navigation})}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="sad" />
							<Text style={{ fontSize: 13 }}>Danh sách khiếu nại</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
		          	<ListItem last onPress={() => this.props.navigation.navigate(ListPackageScreen, {navigation: this.props.navigation})}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="cube" type="FontAwesome" />
							<Text style={{ fontSize: 13 }}>Quản lý kiện hàng</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
		          	<Separator >
		            	<Text style={{fontSize: 14, color: 'black', fontWeight: 'bold'}}>Quản lý giao hàng</Text>
		          	</Separator>
		          	<ListItem last onPress={() => this.props.navigation.navigate(ListAddressScreen, {navigation: this.props.navigation})}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="map-marker" type="FontAwesome"/>
							<Text style={{ fontSize: 13 }}>Địa chỉ của bạn</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
			        <ListItem last onPress={() => this.props.navigation.navigate(CreateRequestDeliveryScreen, {navigation: this.props.navigation})}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="paper-plane"/>
							<Text style={{ fontSize: 13 }}>Tạo YC giao hàng</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
		         	<ListItem last onPress={() => this.props.navigation.navigate(ListRequestDeliveryScreen, {navigation: this.props.navigation})}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="paper" />
							<Text style={{ fontSize: 13 }}>Danh sách YC giao hàng</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
		          	<Separator >
		            	<Text style={{fontSize: 14, color: 'black', fontWeight: 'bold'}}>Quản lý ví tiền</Text>
		          	</Separator>
			        <ListItem last onPress={() => this.props.navigation.navigate(ListTransactionScreen, {navigation: this.props.navigation})}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="exchange" type="FontAwesome"/>
							<Text style={{ fontSize: 13 }}>Lịch sử giao dịch</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
		          	<Separator >
		            	<Text style={{fontSize: 14, color: 'black', fontWeight: 'bold'}}>Góc thông tin</Text>
		          	</Separator>
			        <ListItem last onPress={() => this.props.navigation.navigate(PagePriceScreen)}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="calculator"/>
							<Text style={{ fontSize: 13 }}>Biểu phí</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
		          	<ListItem last onPress={() => this.props.navigation.navigate(PagePayScreen)}>
		            	<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="card"/>
							<Text style={{ fontSize: 13 }}>Thanh toán</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
		          	</ListItem>
					<Separator>
						<Text style={{fontSize: 14, color: 'black', fontWeight: 'bold'}}>Khác</Text>
					</Separator>
					<ListItem last onPress={() => this.props.navigation.navigate(ListFavoriteScreen, {navigation: this.props.navigation})}>
						<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="heart"/>
							<Text style={{ fontSize: 13 }}>Sản phẩm đã lưu</Text>
						</Left>
						<Right>
							<Icon active name="angle-right" type="FontAwesome"/>
						</Right>
					</ListItem>
					<ListItem last>
						<Left>
							<Icon style={{fontSize: 18, marginRight: 5}} name="help-buoy"/>
							<Text style={{ fontSize: 13 }}>Hotline tư vấn: {this.state.hotline_home}</Text>
						</Left>
					</ListItem>
					<Button full style={{ backgroundColor: '#e53232' }} onPress={() => this._logOut() }>
						<Text style={{ fontSize: 14 }}>Đăng xuất</Text>
						<Icon style={{ color: '#fff', fontSize: 18 }} type="FontAwesome" name="sign-out" />
					</Button>
				</Content>
				</View>
			</View>
		);
	}
}
