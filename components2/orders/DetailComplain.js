import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input, CheckBox, Container, Header, Tab, Tabs, TabHeading, List, ListItem, Content, Thumbnail } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity, Image } from 'react-native';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getDetailComplain, getListComment, newComment } from "../../requests/Order";
import Loader from '../Loader';
import NumberFormat from 'react-number-format';
import ImageView from 'react-native-image-view';
import { MainScreen } from '../../ScreenNames';

class TabComments extends Component{
	constructor(props){
		super(props);
		this.state = {
			object_id: this.props.object_id,
            object_type: 'COMPLAINT',
            new_message: '',
            comments: []
		}
	}

	freshDataComments(){
		_retrieveData('login_info').then((user_info) => {
			var params = {
				object_id: this.state.object_id,
				object_type: this.state.object_type
			}
			this.setState({  isLoading: true });
			getListComment(params, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					this.setState({
						comments: result.comments
					})
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	createComment(){
		if(this.state.new_message == ""){
			Alert.alert('Thông báo', 'Vui lòng chọn nhập vào tin nhắn', [{text: 'OK' }], {cancelable: false});
			return false;
		}

		_retrieveData('login_info').then((user_info) => {
			var postData = {
				object_id: this.state.object_id,
				object_type: this.state.object_type,
				message: this.state.new_message
			}
			this.setState({  isLoading: true });
			newComment(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({  isLoading: false });
				if(result.success == true){
					this.setState({ new_message: '' });
					this.freshDataComments()
				}else{
					Alert.alert('Thông báo', 'Không thể gửi tin nhắn', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshDataComments();
	}

	render(){
		return (
			<View style={{flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<View style={{height: 60, marginTop: 5}}>
					<View style={{flex: 1, flexDirection: 'row', backgroundColor: '#fff', marginLeft: 5, margin: 5, borderBottomWidth: 0, borderBottomColor: '#ececec' }}>
						<View style={{flex: 6, paddingRight: 10}}>
							<Item regular style={{ height: 45}}>
								<Input
									style={{ fontSize: 13 }}
									placeholder="Nhập tin nhắn tại đây"
									returnKeyType='next'
									autoCorrect={false}
									onChangeText={(text) => this.setState({new_message: text})}
									value={this.state.new_message}
								/>
							</Item>
						</View>
						<View style={{flex: 1 }}>
							<Button style={{ backgroundColor: appConfig.baseColor, padding: 5, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.createComment()}>
								<Label style={{ color: 'white', fontSize: 14 }}>Gửi</Label>
							</Button>
						</View>
					</View>
				</View>
				<Content style={{backgroundColor: '#FFF'}}>
				{this.state.comments.map((comment, index) => (
	            	<Item key={index} style={{backgroundColor: '#fcf8e3', marginLeft: 5, margin: 5, padding: 10, borderBottomWidth: 0}}>
						<View style={{flex: 1, flexDirection: 'row'}} >
							<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Thumbnail style={{ borderWidth: 1, borderColor: '#ececec'}} square small source={{uri: comment.user.avatar }} />
							</View>
							<View style={{flex: 6, justifyContent: 'center', alignItems: 'flex-start'}}>
								<View style={{flex: 1}}>
									<Text style={{fontSize: 13, color: 'gray'}}>{comment.user.name} - {comment.created_at}</Text>
								</View>
								<View style={{flex: 1}}>
									<Text style={{fontSize: 13}}>{comment.message}</Text>
								</View>
							</View>
						</View>
					</Item>
				))}
	        	</Content>
        	</View>
		)
	}
}

class TabInfo extends Component{
	constructor(props){
		super(props);
		this.state = {
			imageIndex: 0,
            isImageViewVisible: false,
            
		}
	}

	render(){
		var total_image = this.props.files.length;
		return (
			<Content style={{backgroundColor: '#f1f1f1'}}>
				<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, padding: 10, borderBottomWidth: 0}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
							<Text style={{fontSize: 13}}>Mã khiếu nại: <Text style={{color: appConfig.baseColor, fontSize: 14}}> {this.props.complain.code}</Text></Text>
						</View>
						<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
							<Text style={{fontSize: 13, textAlign: 'center', backgroundColor: '#FFC107', padding: 5, borderRadius: 5, color: 'white'}}>{this.props.complain.status_title}</Text>
						</View>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, borderBottomWidth: 0}}>
					<View style={{ flex: 1}}>
						<View style={{flex: 1, padding: 10, justifyContent: 'center', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<Text style={{fontSize: 13}}>{this.props.complain.title}</Text>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Lý do</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.complain.type_title}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Phương án xử lý</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.complain.process_type_title}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Yêu cầu</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.props.complain.customer_request_amount} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Đề nghị</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.props.complain.refund_amount} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Mô tả</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.complain.customer_description}</Text>
							</View>
						</View>
					 </View>
				</Item>
				<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, borderBottomWidth: 0}}>
					<View style={{ flex: 1}}>
						<View style={{flex: 1, padding: 10, justifyContent: 'center', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<Text style={{fontSize: 13}}>Ảnh khiếu nại</Text>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
		                    {this.props.files.map((image, index) => (
		                    	<TouchableOpacity key={index} onPress={() => { this.setState({ imageIndex: index, isImageViewVisible: true, }); }} >
		                        	<Thumbnail style={{ margin: 5}} square source={{ uri: appConfig.baseUrl2 + image.file_path }} />
		                        </TouchableOpacity>
		                    ))}
						</View>
					 </View>
				</Item>
				<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, borderBottomWidth: 0}}>
					<View style={{ flex: 1}}>
						<View style={{flex: 1, padding: 10, justifyContent: 'center', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<Text style={{fontSize: 13}}>Lịch sử khiếu nại</Text>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Chờ tiếp nhận</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.complain.created_at != null && this.props.complain.created_at != "" ? this.props.complain.created_at : "--"}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Đang giải quyết</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.complain.process_at != null && this.props.complain.process_at != "" ? this.props.complain.process_at : "--"}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Đã hoàn tiền</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.complain.refund_at != null && this.props.complain.refund_at != "" ? this.props.complain.refund_at : "--"}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Từ chối</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.complain.reject_at != null && this.props.complain.reject_at != "" ? this.props.complain.reject_at: '--'}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Đã huỷ</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.complain.cancel_at != null && this.props.complain.cancel_at != "" ? this.props.complain.cancel_at : "--"}</Text>
							</View>
						</View>
					 </View>
				</Item>
				 
                <ImageView
				    images={this.props.images}
				    imageIndex={this.state.imageIndex}
				    isVisible={this.state.isImageViewVisible}
				    onClose={() => this.setState({isImageViewVisible: false})}
				    renderFooter={(currentImage) => (<View style={{ margin: 10 }}><Text style={{color: '#fff'}}>{currentImage.title + 1}/{total_image}</Text></View>)}
				/>
        	</Content>
		)
	}
}

export default class DetailComplain extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = { 
			id: this.props.navigation.getParam('id'),
			complain: [],
			order: [],
			files: [],
			images: []
		}
	}

	componentDidMount(){
		_retrieveData('login_info').then((user_info) => {
			this.setState({  isLoading: true });
			getDetailComplain(this.state.id, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					this.setState({
						complain: result.complaint,
						order: result.order,
						files: result.files
					})

					var images = [];
					if(result.files.length > 0){
						const images = [];
						result.files.map((image, index) => {
							images.push({source : {uri: appConfig.baseUrl2 + image.file_path }, title: index });
						});

						this.setState({images : images });
					}

				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	render(){
		return (
			<Container>
				<Loader loading={this.state.isLoading} />
				<Tabs tabBarUnderlineStyle={{height:5, backgroundColor : appConfig.baseColor}}>
					<Tab heading="Thông tin" 
						tabStyle={{backgroundColor: '#fff'}} 
						activeTabStyle={{backgroundColor: '#fff'}} 
						textStyle={{color: appConfig.baseColor, fontSize: 14}}
						activeTextStyle={{color: appConfig.baseColor, fontSize: 14}} >
						<TabInfo complain={this.state.complain} order={this.state.order} files={this.state.files} images={this.state.images}/>
					</Tab>
					<Tab heading="Trao đổi với nhân viên" 
						tabStyle={{backgroundColor: '#fff'}} 
						activeTabStyle={{backgroundColor: '#fff'}} 
						textStyle={{color: appConfig.baseColor, fontSize: 14}}
						activeTextStyle={{color: appConfig.baseColor, fontSize: 14}}>
						<TabComments object_id={this.state.complain.id}/>
					</Tab>
				</Tabs>
			</Container>
		)
	}
}
