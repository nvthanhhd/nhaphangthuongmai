import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input, Container, Header, Tab, Tabs, TabHeading, List
		, ListItem, Content, Thumbnail, Switch, Separator, ScrollableTab } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity, Image } from 'react-native';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getDetailOrder, getListComment, newComment, newOrderNote, saveNoteItem, saveNoteItemPrivate } from "../../requests/Order";
import Loader from '../Loader';
import NumberFormat from 'react-number-format';
import Modal from 'react-native-modalbox';
import { CreateComplainScreen, MainScreen } from '../../ScreenNames';

var screen = Dimensions.get('window');
var order_items = [];
var order_fee = [];
var order_transactions = [];
var order_complains = [];
var order_packages = [];

class ModalNoteItemPrivate extends Component{
	constructor(props){
		super(props);
		this.state = {
			note_private: '',
			item_id: ''
		};
	}

	saveNoteItemPrivate(){
		this.props.parentView.saveNoteItemPrivate(this.state.note_private, this.state.item_id);
		this.closeModalNoteItemPrivate();
	}

	showModalNoteItemPrivate(note_private, item_id){
		this.setState({ note_private: note_private, item_id: item_id });
		this.refs.modalNoteItemPrivate.open();
	}

	closeModalNoteItemPrivate(){
		this.refs.modalNoteItemPrivate.close();
	}

	render(){
		return(
			<Modal
				ref={'modalNoteItemPrivate'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:60,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5, flexDirection: 'row' }}>
				<Item regular style={{flex: 3, marginVertical: 5}}>
					<Input
						style={{fontSize: 13}}
						placeholder="Ghi chú cá nhân"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({note_private: text})}
						value={this.state.note_private}
					/>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this.saveNoteItemPrivate() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 5,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 14}}>Lưu</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class ModalNoteItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			note: '',
			item_id: ''
		};
	}

	saveNoteItem(){
		this.props.parentView.saveNoteItem(this.state.note, this.state.item_id);
		this.closeModalNoteItem();
	}

	showModalNoteItem(note, item_id){
		this.setState({ note: note, item_id: item_id });
		this.refs.modalNoteItem.open();
	}

	closeModalNoteItem(){
		this.refs.modalNoteItem.close();
	}

	render(){
		return(
			<Modal
				ref={'modalNoteItem'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:60,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5, flexDirection: 'row' }}>
				<Item regular style={{flex: 3, marginVertical: 5}}>
					<Input
						style={{fontSize: 13}}
						placeholder="Ghi chú sản phẩm"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({note: text})}
						value={this.state.note}
					/>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this.saveNoteItem() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 5,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 14}}>Lưu</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class TabPackages extends Component{
	constructor(props){
		super(props);
	}

	render(){
		return (
			<View style={{flex: 1}}>
				{order_packages.map((item, index) => (
            	<Item key={index} style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, padding: 10}}>
            		<View style={{flex: 1}}>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13 }}>Mã kiện</Text>
							</View>
							<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.logistic_package_barcode}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Trạng thái</Text>
							</View>
							<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{fontSize: 13, textAlign: 'center', justifyContent: 'center', padding: 5, borderRadius: 5, backgroundColor: '#'+item.color, color: '#fff' }}>{item.status_title}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Cân nặng</Text>
							</View>
							<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.calculate_weight} kg</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Kích thước (d/r/c) (cm)</Text>
							</View>
							<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.length_package} / {item.width_package} / {item.height_package}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Cập nhật</Text>
							</View>
							<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.updated_at}</Text>
							</View>
						</View>
					</View>
				</Item>
				))}
        	</View>
		)
	}
}

class TabComplains extends Component{
	constructor(props){
		super(props);
		this.state = {
		}
	}

	render(){
		return (
			<View style={{flex: 1}}>
				{order_complains.data.map((item, index) => (
            	<Item key={index} style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, padding: 10}}>
            		<View style={{flex: 1}}>
            			<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
							<Text style={{ fontSize: 13,justifyContent: 'center', alignItems: 'center' }}>{item.title}</Text>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13 }}>Mã khiếu nại</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.code}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Thời gian</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.created_at}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Trạng thái</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{fontSize: 13, textAlign: 'center', justifyContent: 'center', backgroundColor: '#FFC107', padding: 5, borderRadius: 5, color: 'white'}}>{item.status_title}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Tiền hoàn</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								{ item.refund_amount != null ? (
								<NumberFormat value={item.refund_amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
								) : (<Text>--</Text>)}
							</View>
						</View>
					</View>
				</Item>
				))}
        	</View>
		)
	}
}

class TabTransactions extends Component{
	constructor(props){
		super(props);
		this.state = {
		}
	}

	render(){
		return (
			<View style={{flex: 1}}>
				{order_transactions.data.map((item, index) => (
            	<Item key={index} style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, padding: 10}}>
            		<View style={{flex: 1}}>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13 }}>Mã</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.transaction_code}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Thời gian</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.created_at}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Loại giao dịch</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.transaction_type_title}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Giá trị</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<NumberFormat value={item.amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, color: item.amount < 0 ? 'red' : 'green' }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<Text style={{ fontSize: 13 }}>Nội dung</Text>
							</View>
							<View style={{flex: 2, flexDirection: 'row', justifyContent: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{item.transaction_note}</Text>
							</View>
						</View>
					</View>
				</Item>
				))}
        	</View>
		)
	}
}

class TabFee extends Component{
	constructor(props){
		super(props);
		this.state = {
		}
	}

	render(){
		return (
			<View style={{flex: 1, backgroundColor: '#fff'}}>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Phí cố định</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.fixed_fee} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Phí dịch vụ</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.buying_fee} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Phí kiểm đếm</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.checking_fee} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Phí đóng gỗ riêng</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.wood_crating_fee} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Phí đóng xốp</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.surf_fee} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Tổng tiền phí dịch vụ</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.fee_amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Phí vận chuyển tiêu chuẩn</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.shipping_fee} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Tiền VC nội địa</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.domestic_shipping_fee_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Tiền hàng</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Tổng chi phí</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.total_amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, fontWeight: 'bold' }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Đã thanh toán</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.paymented} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, fontWeight: 'bold' }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>Cần thanh toán</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.need_payment} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, fontWeight: 'bold', color: 'red' }}>{value} đ </Text>}/>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingLeft: 10, borderBottomColor: '#fff'}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<Text style={{ fontSize: 13 }}>KNDV trả lại</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
						<NumberFormat value={order_fee.refund_complaint} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, fontWeight: 'bold' }}>{value} đ </Text>}/>
					</View>
				</Item>
        	</View>
		)
	}
}

class TabItems extends Component{
	constructor(props){
		super(props);
		this.state = {
			note_item: undefined,
			note_item_private: undefined,
		}
	}

	saveNoteItem(note, item_id){
		_retrieveData('login_info').then((user_info) => {
			var postData = {
				item_id: item_id,
				note: note,
			}

			this.setState({  isLoading: true });
			saveNoteItem(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({  isLoading: false });
				if(result.success == true){
					this.setState({ note_item: note });
				}else{
					Alert.alert('Thông báo', 'Không thể lưu ghi chú', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	openModalNoteItem(note, item_id){
		this.refs.modalNoteItem.showModalNoteItem(note, item_id);
	}


	saveNoteItemPrivate(note_private, item_id){
		_retrieveData('login_info').then((user_info) => {
			var postData = {
				item_id: item_id,
				note: note_private,
			}

			this.setState({  isLoading: true });
			saveNoteItemPrivate(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({  isLoading: false });
				if(result.success == true){
					this.setState({ note_item_private: note_private });
				}else{
					Alert.alert('Thông báo', 'Không thể lưu ghi chú cá nhân', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	openModalNoteItemPrivate(note_private, item_id){
		this.refs.modalNoteItemPrivate.showModalNoteItemPrivate(note_private, item_id);
	}

	createNewComplain(order_code, item_id){
		this.props.navigation.navigate(CreateComplainScreen, {order_code: order_code, item_id: item_id});
	}

	render(){
		return (
			<View style={{flex: 1}}>
					{order_items.map((item, index) => (
	            	<Item key={index} style={{backgroundColor: '#fff', marginLeft: 5, margin: 5, paddingHorizontal: 5, paddingVertical: 10, borderBottomWidth: 1}}>
	            		<View style={{flex: 1}}>
							<View style={{flex: 1, flexDirection: 'row'}} >
								<View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start'}}>
									<Thumbnail style={{ borderWidth: 1, borderColor: '#ececec'}} square source={{uri: item.image }} />
								</View>
								<View style={{flex: 5, justifyContent: 'center', alignItems: 'flex-start', marginLeft: 5}}>
									<View style={{flex: 1, paddingBottom: 5}}>
										<Text style={{fontSize: 13 }}>{item.title}</Text>
									</View>
									<View style={{flex: 1, paddingBottom: 5}}>
										<Text style={{fontSize: 13, color: 'gray'}}>{item.property}</Text>
									</View>
									<View style={{flex: 1, paddingBottom: 5}}>
										<Text style={{fontSize: 13}}>Số lượng: <NumberFormat value={item.order_quantity} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value}</Text>}/></Text>
									</View>
									<View style={{flex: 1, paddingBottom: 5}}>
										<Text style={{fontSize: 13}}>
											Tiền hàng: <NumberFormat value={item.amount_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ </Text>}/>
											(<NumberFormat value={item.amount} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} ¥</Text>}/>)
										</Text>
									</View>
									
								</View>
							</View>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-end'}}>
									<TouchableOpacity style={{flexDirection: 'row'}} onPress={ () => this.openModalNoteItem(this.state.note_item != undefined ? this.state.note_item : item.note, item.id) } >
										<Icon name="edit" type="FontAwesome" style={{fontSize: 18, color: appConfig.baseColor }} />
										<Text style={{ color : appConfig.baseColor, fontSize: 13 }}>Ghi chú</Text>
									</TouchableOpacity>
								</View>
								<View style={{flex: 2, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end'}}>
									<TouchableOpacity style={{flexDirection: 'row'}} onPress={ () => this.openModalNoteItemPrivate(this.state.note_item_private != undefined ? this.state.note_item_private : item.note_private, item.id) } >
										<Icon name="edit" type="FontAwesome" style={{fontSize: 18, color: appConfig.baseColor }} />
										<Text style={{ color : appConfig.baseColor, fontSize: 13 }}>Ghi chú cá nhân</Text>
									</TouchableOpacity>
								</View>
								<View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end'}}>
									<TouchableOpacity style={{flexDirection: 'row'}} onPress={ () => this.createNewComplain(this.props.order_code, item.id) } >
										<Text style={{ color : 'red', fontSize: 13, borderRadius: 5, borderColor: 'red', padding: 5, borderWidth: 1 }}>Khiếu nại</Text>
									</TouchableOpacity>
								</View>
							</View>
						</View>
					</Item>
				))}
				<ModalNoteItem ref={'modalNoteItem'} parentView={this}></ModalNoteItem>
				<ModalNoteItemPrivate ref={'modalNoteItemPrivate'} parentView={this}></ModalNoteItemPrivate>
        	</View>
		)
	}
}

class TabHistory extends Component{
	constructor(props){
		super(props);
		this.state = {
		}
	}

	render(){
		return (
			<View style={{flex: 1}}>
				<Content style={{backgroundColor: '#FFF'}}>
					{this.props.order.deposited_at != null && this.props.order.deposited_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Đặt cọc</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.deposited_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
					{this.props.order.wait_paid_at != null && this.props.order.wait_paid_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Chờ pai</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.wait_paid_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.paided_at != null && this.props.order.paided_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Đã pai</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.paided_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.bought_at != null && this.props.order.bought_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Đã mua</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.bought_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.seller_delivery_at != null && this.props.order.seller_delivery_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Người bán giao</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.seller_delivery_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.received_from_seller_at != null && this.props.order.received_from_seller_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Kho trung quốc nhận</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.received_from_seller_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.transporting_at != null && this.props.order.transporting_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Vận chuyển</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.transporting_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.waiting_delivery_at != null && this.props.order.waiting_delivery_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Nhập kho VN</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.waiting_delivery_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.delivering_at != null && this.props.order.delivering_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Đang giao hàng</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.delivering_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.received_at != null && this.props.order.received_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Đã nhận hàng</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.received_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
          			{this.props.order.cancelled_at != null && this.props.order.cancelled_at != "" ? (
	            	<ListItem icon>
			            <Left>
			                <Icon style={{ color: 'green', fontSize: 28}} active name="checkmark-circle" />
			            </Left>
			            <Body>
			              <Text style={{fontSize: 13}}>Đã huỷ</Text>
			            </Body>
			            <Right>
			            	<Text style={{fontSize: 13}}>{this.props.order.cancelled_at}</Text>
			            </Right>
          			</ListItem>) : (<View></View>)}
	        	</Content>
        	</View>
		)
	}
}

class TabComments extends Component{
	constructor(props){
		super(props);
		this.state = {
			object_id: this.props.object_id,
            object_type: 'ORDER',
            new_message: '',
            comments: []
		}
	}

	freshDataComments(){
		_retrieveData('login_info').then((user_info) => {
			var params = {
				object_id: this.state.object_id,
				object_type: this.state.object_type
			}
			this.setState({  isLoading: true });
			getListComment(params, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					this.setState({
						comments: result.comments
					})
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	createComment(){
		if(this.state.new_message == ""){
			Alert.alert('Thông báo', 'Vui lòng nhập vào tin nhắn', [{text: 'OK' }], {cancelable: false});
			return false;
		}

		_retrieveData('login_info').then((user_info) => {
			var postData = {
				object_id: this.state.object_id,
				object_type: this.state.object_type,
				message: this.state.new_message
			}
			this.setState({  isLoading: true });
			newComment(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({  isLoading: false });
				if(result.success == true){
					this.setState({ new_message: '' });
					this.freshDataComments()
				}else{
					Alert.alert('Thông báo', 'Không thể gửi tin nhắn', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshDataComments();
	}

	render(){
		return (
			<View style={{flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<View style={{height: 60, marginTop: 5}}>
					<View style={{flex: 1, flexDirection: 'row', backgroundColor: '#fff', marginLeft: 5, margin: 5, borderBottomWidth: 0, borderBottomColor: '#ececec' }}>
						<View style={{flex: 6, paddingRight: 10}}>
							<Item regular style={{ height: 45}}>
								<Input
									style={{fontSize: 13}}
									placeholder="Nhập tin nhắn tại đây"
									returnKeyType='next'
									autoCorrect={false}
									onChangeText={(text) => this.setState({new_message: text})}
									value={this.state.new_message}
								/>
							</Item>
						</View>
						<View style={{flex: 1 }}>
							<Button style={{ backgroundColor: appConfig.baseColor, padding: 5, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.createComment()}>
								<Label style={{ color: 'white', fontSize: 14 }}>Gửi</Label>
							</Button>
						</View>
					</View>
				</View>
				<Content style={{backgroundColor: '#FFF'}}>
				{this.state.comments.map((comment, index) => (
	            	<Item key={index} style={{backgroundColor: '#fcf8e3', marginLeft: 5, margin: 5, padding: 10, borderBottomWidth: 0}}>
						<View style={{flex: 1, flexDirection: 'row'}} >
							<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Thumbnail style={{ borderWidth: 1, borderColor: '#ececec'}} square small source={{uri: comment.user.avatar }} />
							</View>
							<View style={{flex: 6, justifyContent: 'center', alignItems: 'flex-start'}}>
								<View style={{flex: 1}}>
									<Text style={{fontSize: 13, color: 'gray'}}>{comment.user.name} - {comment.created_at}</Text>
								</View>
								<View style={{flex: 1}}>
									<Text style={{fontSize: 13}}>{comment.message}</Text>
								</View>
							</View>
						</View>
					</Item>
				))}
	        	</Content>
        	</View>
		)
	}
}

class ModalNote extends Component{
	constructor(props){
		super(props);
		this.state = {
			note: '',
		};
	}

	saveNote(){
		this.props.parentView.saveNote(this.state.note);
		this.closeModalNote();
	}

	showModalNote(note){
		this.setState({ note: note });
		this.refs.modalNote.open();
	}

	closeModalNote(){
		this.refs.modalNote.close();
	}

	render(){
		return(
			<Modal
				ref={'modalNote'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:200,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5}}>
				<Item regular>
					<Input
						style={{fontSize: 13}}
						placeholder="Ghi chú"
						returnKeyType='next'
						autoCorrect={false}
						multiline={true}
    					numberOfLines={5}
						onChangeText={(text) => this.setState({note: text})}
						value={this.state.note}
					/>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this.saveNote() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 14}}>Lưu</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class TabInfo extends Component{
	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			note: undefined
		}
	}

	saveNote(note){
		_retrieveData('login_info').then((user_info) => {
			var postData = {
				id: this.props.order.id,
				note: note,
			}

			this.setState({  isLoading: true });
			newOrderNote(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({  isLoading: false });
				if(result.success == true){
					this.setState({ note: note });
				}else{
					Alert.alert('Thông báo', 'Không thể lưu ghi chú', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	showModalNote(note){
		this.refs.modalNote.showModalNote(note);
	}

	createNewComplainOrder(){
		this.props.navigation.navigate(CreateComplainScreen, {order_code: this.props.order.code});
	}

	render(){
		
		return (
			<Content style={{backgroundColor: '#f1f1f1'}}>
				<Loader loading={this.state.isLoading} />
				<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, padding: 10, borderBottomWidth: 0}}>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<View style={{flex: 1.3, justifyContent: 'center', alignItems: 'flex-start'}}>
							<Text style={{fontSize: 13}}>Mã đơn hàng: <Text style={{color: appConfig.baseColor}}> {this.props.order.code}</Text></Text>
						</View>
						<View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', flexDirection: 'row'}}>
							<Text style={{fontSize: 13, textAlign: 'center', backgroundColor: '#'+this.props.order_status.color, padding: 5, borderRadius: 5, color: 'white'}}>{this.props.order.status_title}</Text>
							<TouchableOpacity style={{flexDirection: 'row'}} onPress={() => this.createNewComplainOrder() }>
								<Text style={{ color : 'red', fontSize: 13, borderRadius: 5, borderColor: 'red', padding: 5, borderWidth: 1, marginLeft: 5 }}>Khiếu nại</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Item>
				<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, borderBottomWidth: 0}}>
					<View style={{ flex: 1}}>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Tổng chi phí</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<NumberFormat value={this.props.order.total_amount_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, fontWeight: 'bold' }}>{value} đ</Text>}/>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Tỷ lệ đặt cọc</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.order.deposit_percent} %</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Cân nặng tính phí</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.order.weight_calculate == null ? ' -- ' : this.props.order.weight_calculate} kg</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Bảo hiểm</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								{this.props.order.service_insure != 0 ? (
									<Text style={{fontSize: 13}}>{this.props.order.service_insure_percent} % / <NumberFormat value={this.props.order.service_insure_amount_vnd} displayType={'text'} decimalScale={2} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/></Text>) : (<View></View>)
								}
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Dịch vụ</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.services_title}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Vận đơn</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13}}>{this.props.list_freight_bill.join(', ')}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Địa chỉ giao hàng</Text>
							</View>
							<View style={{flex: 1.5, justifyContent: 'center', alignItems: 'flex-end'}}>
								<Text style={{fontSize: 13, fontWeight: 'bold'}}>{this.props.user_address.reciver_name} - {this.props.user_address.reciver_phone} - {this.props.user_address.detail} - {this.props.user_address.district_label} - {this.props.user_address.province_label}</Text>
							</View>
						</View>
						<View style={{flex: 1, padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ececec'}}>
							<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
								<Text style={{fontSize: 13}}>Ghi chú</Text>
							</View>
							<View style={{flexDirection: 'row', flex: 1.5 }}>
								<View style={{flex: 3, justifyContent: 'center', alignItems: 'flex-end'}}>
									<Text style={{fontSize: 13}}>{this.state.note != undefined ? this.state.note : this.props.order.note}</Text>
								</View>
								<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
									<Button iconRight transparent small onPress={() => this.showModalNote(this.state.note != undefined ? this.state.note : this.props.order.note)}>
										<Icon name="edit" type="FontAwesome" style={{fontSize: 20, color: appConfig.baseColor }} />
									</Button>
								</View>
							</View>
						</View>
					 </View>
				</Item>
				
				<Item style={{backgroundColor: '#fff', margin: 5, marginLeft: 5, borderRadius: 5, borderBottomWidth: 0}}>
					<Tabs renderTabBar={()=> <ScrollableTab />} tabBarUnderlineStyle={{height:5, backgroundColor : '#e6f7ff'}} >
						<Tab heading="Sản phẩm" 
							tabStyle={{backgroundColor: '#fff'}} 
							activeTabStyle={{backgroundColor: '#e6f7ff'}} 
							textStyle={{color: appConfig.baseColor, fontSize: 13}}
							activeTextStyle={{color: appConfig.baseColor, fontSize: 13}}>
							<TabItems order_code={this.props.order.code} navigation={this.props.navigation}/>
						</Tab>
						<Tab heading="Tài chính" 
							tabStyle={{backgroundColor: '#fff'}} 
							activeTabStyle={{backgroundColor: '#e6f7ff'}} 
							textStyle={{color: appConfig.baseColor, fontSize: 13}}
							activeTextStyle={{color: appConfig.baseColor, fontSize: 13}}>
							<TabFee/>
						</Tab>
						<Tab heading="Kiện hàng" 
							tabStyle={{backgroundColor: '#fff'}} 
							activeTabStyle={{backgroundColor: '#e6f7ff'}} 
							textStyle={{color: appConfig.baseColor, fontSize: 13}}
							activeTextStyle={{color: appConfig.baseColor, fontSize: 13}}>
							<TabPackages/>
						</Tab>
						<Tab heading="Giao dịch" 
							tabStyle={{backgroundColor: '#fff'}} 
							activeTabStyle={{backgroundColor: '#e6f7ff'}} 
							textStyle={{color: appConfig.baseColor, fontSize: 13}}
							activeTextStyle={{color: appConfig.baseColor, fontSize: 13}}>
							<TabTransactions/>
						</Tab>
						<Tab heading="Khiếu nại" 
							tabStyle={{backgroundColor: '#fff'}} 
							activeTabStyle={{backgroundColor: '#e6f7ff'}} 
							textStyle={{color: appConfig.baseColor, fontSize: 13}}
							activeTextStyle={{color: appConfig.baseColor, fontSize: 13}}>
							<TabComplains />
						</Tab>
					</Tabs>
				</Item>
				<ModalNote ref={'modalNote'} parentView={this}></ModalNote>
        	</Content>
		)
	}
}

export default class DetailOrder extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = { 
			order_id: this.props.navigation.getParam('order_id'),
			order: undefined,
			order_status: '',
			user_address: [],
			list_freight_bill: [],
			services_title: '',
		}
	}

	componentDidMount(){
		_retrieveData('login_info').then((user_info) => {
			this.setState({ isLoading: true });
			getDetailOrder(this.state.order_id, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					order_items = result.order_items.data;
					order_fee = result.fee;
					order_transactions = result.transactions;
					order_complains = result.complaints;
					order_packages = result.packages;

					this.setState({
						order: result.order,
						order_status: user_info.userInfo.order_status[result.order.status],
						user_address: result.user_address,
						services_title: result.services_title,
					})
				}

				//
				var list_freight_bill = [];
				for(const i in result.order.freight_bill){
					list_freight_bill.push(result.order.freight_bill[i].freight_bill);
				}

				this.setState({ list_freight_bill: list_freight_bill });

			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	render(){
		return (
			<Container>
				<Loader loading={this.state.isLoading} />
				{this.state.order != undefined ? (
				<Tabs tabBarUnderlineStyle={{height:5, backgroundColor : appConfig.baseColor}}>
					<Tab heading="Thông tin" 
						tabStyle={{backgroundColor: '#fff'}} 
						activeTabStyle={{backgroundColor: '#fff'}} 
						textStyle={{color: appConfig.baseColor, fontSize: 14}}
						activeTextStyle={{color: appConfig.baseColor, fontSize: 14}} >
						<TabInfo services_title= {this.state.services_title} 
								list_freight_bill={this.state.list_freight_bill} 
								order={this.state.order} 
								user_address={this.state.user_address} 
								order_status={this.state.order_status}
								navigation={this.props.navigation}
						/>
					</Tab>
					<Tab heading="Lịch sử" 
						tabStyle={{backgroundColor: '#fff'}} 
						activeTabStyle={{backgroundColor: '#fff'}} 
						textStyle={{color: appConfig.baseColor, fontSize: 14}}
						activeTextStyle={{color: appConfig.baseColor, fontSize: 14}}>
						<TabHistory order={this.state.order} />
					</Tab>
					<Tab heading="Trao đổi" 
						tabStyle={{backgroundColor: '#fff'}} 
						activeTabStyle={{backgroundColor: '#fff'}} 
						textStyle={{color: appConfig.baseColor, fontSize: 14}}
						activeTextStyle={{color: appConfig.baseColor, fontSize: 14}}>
						<TabComments object_id={this.state.order.id}/>
					</Tab>
				</Tabs>) : (<View></View>)}
			</Container>
		)
	}
}
