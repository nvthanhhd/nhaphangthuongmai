import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modalbox';
import DatePicker from 'react-native-datepicker'
import NumberFormat from 'react-number-format';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListComplain } from "../../requests/Order";
import Loader from '../Loader';
import { DetailComplainScreen, MainScreen } from '../../ScreenNames';

var screen = Dimensions.get('window');

class ModalSearch extends Component{
	constructor(props){
		super(props);
		this.state = {
			code: '',
			order_code: '',
			item_id: '',
			process_type: '',
			status: ''
		};
	}

	_search(){
		var params = {
			code: this.state.code,
			order_code: this.state.order_code,
			item_id: this.state.item_id,
			process_type: this.state.process_type,
			status: this.state.status
		}
		this.props.parentView._doSearch(params);
	}

	showModalSearch(){
		this.refs.modalSearch.open();
	}

	closeModalSearch(){
		this.refs.modalSearch.close();
	}

	changeStatus(value: string) {
		this.setState({
			status: value
		});
	}

	changeProcessType(value: string) {
		this.setState({
			process_type: value
		});
	}

	render(){
		return(
			<Modal
				ref={'modalSearch'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:350,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5}}>
				<Item regular style={{marginTop: 10}}>
					<Input
						style={{fontSize: 13}}
						placeholder="Mã khiếu nại"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({code: text})}
						value={this.state.code}
					/>
				</Item>
				<Item regular>
					<Input
						style={{fontSize: 13}}
						placeholder="Mã đơn hàng"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({order_code: text})}
						value={this.state.order_code}
					/>
				</Item>
				<Item regular>
					<Input
						style={{fontSize: 13}}
						placeholder="Mã sản phẩm"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({item_id: text})}
						value={this.state.item_id}
					/>
				</Item>
				<Item picker>
					<Text style={{fontSize: 13}}>Trạng thái: </Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						style={{ width: undefined }}
						selectedValue={this.state.status}
						onValueChange={this.changeStatus.bind(this)}
						>
						<Picker.Item label="Tất cả" value="" />
						{Object.keys(this.props.complaint_status).map((key, i) => {
							return <Picker.Item key={i} value={key} label={this.props.complaint_status[key]} />
						})}
					</Picker>
				</Item>
				<Item picker>
					<Text style={{fontSize: 13}}>Phương án xử lý: </Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						style={{ width: undefined }}
						selectedValue={this.state.process_type}
						onValueChange={this.changeProcessType.bind(this)}
						>
						<Picker.Item label="Tất cả" value="" />
						{Object.keys(this.props.complaint_type).map((key, i) => {
							return <Picker.Item key={i} value={key} label={this.props.complaint_type[key]} />
						})}
					</Picker>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this._search() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 14}}>Tìm kiếm</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class FlatListItem extends Component{
	constructor(props){
		super(props);
	}

	gotoDetailComplain(id){
		this.props.parentFlatList.gotoDetailComplain(id);
	}

	render(){
		return(
			<TouchableOpacity onPress={ () => this.gotoDetailComplain(this.props.item.id) } >
			<View style={{ padding: 10, margin: 5, backgroundColor: 'white', borderRadius: 10}}>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 14, color: appConfig.baseColor }}>{this.props.item.title}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Mã khiếu nại</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13, borderWidth: 1, borderColor: 'gray', borderRadius: 5, padding: 5}}>{this.props.item.code}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Thời gian</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.created_at}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Yêu cầu / Tiền hoàn</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>
							{this.props.item.customer_request_amount == null ? '-' : <NumberFormat value={this.props.item.customer_request_amount} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>} / {this.props.item.refund_amount == null ? '-' : <NumberFormat value={this.props.item.refund_amount} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>}
							</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Phương án xử lý</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.process_type_title}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Trạng thái</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{fontSize: 13, textAlign: 'center', justifyContent: 'center', backgroundColor: '#FFC107', padding: 5, borderRadius: 5, color: 'white'}}>{this.props.item.status_title}</Text>
					</View>
				</View>
			</View>
			</TouchableOpacity>
		)
	}
}

export default class ListComplain extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			per_page: 10,
			page: 1,
			lastpage: false,
			total: 0,
			data: [],
			complaint_status: [],
			complaint_type: [],
			order_code: '',
			status: '',
			code: '',
			item_id: '',
			process_type: ''
		}
	}

	gotoDetailComplain(id){
		this.props.navigation.navigate(DetailComplainScreen, {id: id, navigation: this.props.navigation });
	}

	_onPressSearch(){
		this.refs.modalSearch.showModalSearch();
	}

	freshdataComplains(){
		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				order_code: this.state.order_code,
				status: this.state.status,
				code: this.state.code,
				item_id: this.state.item_id,
				process_type: this.state.process_type,
			};
			if(this.state.lastpage) return;
			this.setState({ 
				isLoading: true,
				complaint_status: user_info.userInfo.complaint_status,
				complaint_type: user_info.userInfo.complaint_type
			});
			getListComplain(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ data: [] });
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	_doSearch(params){
		this.setState({
			per_page: 10,
			page: 1,
			lastpage: false,
			total: 0,
			data: [],
			order_code: params.order_code,
			status: params.status,
			code: params.code,
			item_id: params.item_id,
			process_type: params.process_type,
		});

		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				order_code: params.order_code,
				status: params.status,
				code: params.code,
				item_id: params.item_id,
				process_type: params.process_type,
			};

			if(this.state.lastpage) return;
			this.setState({ isLoading: true });
			getListComplain(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				//Dong modal search
				this.refs.modalSearch.closeModalSearch();
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ 
						data: [],
						total: 0
				 	});
				}

				
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshdataComplains();
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<Button style={{ justifyContent: 'flex-start', backgroundColor: '#ececec'}} onPress={() => this._onPressSearch() } iconLeft full light>
					<Text style={{ fontSize: 13, color: appConfig.baseColor }}>{this.state.total} (Khiếu nại)</Text>
					<Icon style={{ color: appConfig.baseColor }} name='search' />
					<Text style={{ color: appConfig.baseColor, fontWeight: 'bold', fontSize: 14 }}>Tìm kiếm</Text>
				</Button>
				<FlatList
					data={this.state.data}
					renderItem={({item, index}) => {
						return(
							<FlatListItem complaint_type={this.state.complaint_type} complaint_status={this.state.complaint_status} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.id.toString()}
					onEndReached={() => {this.freshdataComplains()}}
					onEndReachedThreshold={0.1}
					extraData={this.state.data}
				>

				</FlatList>
				<ModalSearch complaint_type={this.state.complaint_type} complaint_status={this.state.complaint_status} ref={'modalSearch'} parentView={this}></ModalSearch>
			</View>
		)
	}
}
