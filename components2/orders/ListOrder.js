import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input, Thumbnail, CheckBox, Radio } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity, Image } from 'react-native';
import _ from 'lodash';
import Modal from 'react-native-modalbox';
import NumberFormat from 'react-number-format';
import DatePicker from 'react-native-datepicker'
import RadioGroup from 'react-native-radio-buttons-group';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListOrder, postCancelOrder, bookOrderDeposit } from "../../requests/Order";
import Loader from '../Loader';
import { DetailOrderScreen, MainScreen } from '../../ScreenNames';

var screen = Dimensions.get('window');

class ModalSearch extends Component{
	constructor(props){
		super(props);
		const radio_transport = [
			{
				label: 'Thường',
				value: 'NORMAL'
			},
			{
				label: 'Nhanh',
				value: 'FAST'
			}
		];

		this.state = {
			status: '',
			order_code: '',
			start: '',
			end: '',
			site: [],
			services: [],
			shipping_transport_type_name: 'NORMAL',
			check_taobao: false,
			check_tmall: false,
			check_1688: false,
			check_cpt: false,
			check_cpn: false,
			check_cptk: false,
			check_shipho: false,
			check_kiemhang: false,
			check_donggo: false,
			check_dongxop: false,
			radio_transport: radio_transport
		};
	}

	_search(){
		var site = [];
		var services = [];
		//site
		if(this.state.check_taobao){ site.push('taobao'); }
		if(this.state.check_tmall){ site.push('tmall'); }
		if(this.state.check_1688){ site.push('1688'); }

		//services
		if(this.state.check_cpt){ services.push('SHIPPING_CHINA_VIETNAM'); }
		if(this.state.check_cpn){ services.push('CHINA_VIETNAM_FAST'); }
		if(this.state.check_cptk){ services.push('SHIPPING_CHINA_VIETNAM_SAVING'); }
		if(this.state.check_shipho){ services.push('CHINA_VIETNAM_TRANSP'); }
		if(this.state.check_kiemhang){ services.push('CHECKING'); }
		if(this.state.check_donggo){ services.push('WOOD_CRATING'); }
		if(this.state.check_dongxop){ services.push('SURF'); }

		var params = {
			status: this.state.status,
			order_code: this.state.order_code,
			start: this.state.start,
			end: this.state.end,
			site: site,
			services: services,
			shipping_transport_type_name: this.state.check_shipho ? this.state.shipping_transport_type_name : ''
		};

		this.props.parentView._doSearch(params);
	}

	showModalSearch(){
		this.refs.modalSearch.open();
	}

	closeModalSearch(){
		this.refs.modalSearch.close();
	}

	changeOrderStatus(value: string) {
		this.setState({
			status: value
		});
	}

	render(){
		return(
			<Modal
				ref={'modalSearch'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height: 450,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5}}>
				<Item regular>
					<Input
						style={{fontSize: 13}}
						placeholder="Mã đơn hàng"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({order_code: text})}
						value={this.state.order_code}
					/>
				</Item>
				<Item style={{paddingVertical: 10, paddingRight: 10}}>
				<DatePicker
					style={{width: '50%', marginRight: 5 }}
					mode="date"
					date={this.state.start}
					placeholder="Thời gian tạo từ ngày"
					format="DD-MM-YYYY"
					confirmBtnText="Đồng ý"
					cancelBtnText="Hủy"
					showIcon={false}
					onDateChange={(date) => {this.setState({start: date})}}
				  />
				  <DatePicker
  					style={{width: '50%', marginLeft: 5}}
  					mode="date"
					date={this.state.end}
  					placeholder="Tới ngày"
  					format="DD-MM-YYYY"
  					confirmBtnText="Đồng ý"
  					cancelBtnText="Hủy"
  					showIcon={false}
  					onDateChange={(date) => {this.setState({end: date})}}
  				  />
				</Item>
				<Item picker>
					<Text style={{fontSize: 13}}>Trạng thái đơn hàng: </Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						style={{ width: undefined }}
						selectedValue={this.state.status}
						onValueChange={this.changeOrderStatus.bind(this)}
						>
						<Picker.Item label="Tất cả" value="" />
						{Object.values(this.props.order_status).map((value, i) => {
							return <Picker.Item key={i} value={value.key} label={value.title} />
						})}
					</Picker>
				</Item>
				<Item picker style={{flexDirection: 'row', padding: 13}}>
					<View style={{flex: 1}}>
						<Text style={{fontSize: 13}}>Site: </Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<View style={{flex: 0.5}}>
							<CheckBox style={{borderRadius: 100}} checked={this.state.check_taobao} onPress={() => this.setState({check_taobao: !this.state.check_taobao})} color="gray"/>
						</View>
						<View style={{flex: 1 }}>
							<Text style={{fontSize: 13}}> Taobao</Text>
						</View>
					</View>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<View style={{flex: 0.5}}>
							<CheckBox style={{borderRadius: 100}} checked={this.state.check_tmall} onPress={() => this.setState({check_tmall: !this.state.check_tmall})} color="gray"/>
						</View>
						<View style={{flex: 1 }}>
							<Text style={{fontSize: 13}}> Tmall</Text>
						</View>
					</View>
					<View style={{flex: 1, flexDirection: 'row'}}>
						<View style={{flex: 0.5}}>
							<CheckBox style={{borderRadius: 100}} checked={this.state.check_1688} onPress={() => this.setState({check_1688: !this.state.check_1688})} color="gray"/>
						</View>
						<View style={{flex: 1 }}>
							<Text style={{fontSize: 13}}> 1688</Text>
						</View>
					</View>
				</Item>
				<Item style={{flex: 1, flexDirection: 'column', alignItems: 'flex-start'}}>
					<View style={{flex: 1, marginBottom: 10}}>
						<Text style={{fontSize: 13}}>Dịch vụ: </Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
						<View style={{flex: 1, flexDirection: 'row'}}>
							<View style={{flex: 0.5}}>
								<CheckBox style={{borderRadius: 100}} checked={this.state.check_cpt} onPress={() => this.setState({check_cpt: !this.state.check_cpt})} color="gray"/>
							</View>
							<View style={{flex: 1 }}>
								<Text style={{fontSize: 13}}>CPT</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row'}}>
							<View style={{flex: 0.5}}>
								<CheckBox style={{borderRadius: 100}} checked={this.state.check_cpn} onPress={() => this.setState({check_cpn: !this.state.check_cpn})} color="gray"/>
							</View>
							<View style={{flex: 1 }}>
								<Text style={{fontSize: 13}}>CPN</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row'}}>
							<View style={{flex: 0.5}}>
								<CheckBox style={{borderRadius: 100}} checked={this.state.check_cptk} onPress={() => this.setState({check_cptk: !this.state.check_cptk})} color="gray"/>
							</View>
							<View style={{flex: 1 }}>
								<Text style={{fontSize: 13}}>CPTK</Text>
							</View>
						</View>
					</View>
					<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
						<View style={{flex: 1, flexDirection: 'row'}}>
							<View style={{flex: 0.5}}>
								<CheckBox style={{borderRadius: 100}} checked={this.state.check_kiemhang} onPress={() => this.setState({check_kiemhang: !this.state.check_kiemhang})} color="gray"/>
							</View>
							<View style={{flex: 1 }}>
								<Text style={{fontSize: 13}}>Kiểm hàng</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row'}}>
							<View style={{flex: 0.5}}>
								<CheckBox style={{borderRadius: 100}} checked={this.state.check_donggo} onPress={() => this.setState({check_donggo: !this.state.check_donggo})} color="gray"/>
							</View>
							<View style={{flex: 1 }}>
								<Text style={{fontSize: 13}}>Đóng gỗ</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row'}}>
							<View style={{flex: 0.5}}>
								<CheckBox style={{borderRadius: 100}} checked={this.state.check_dongxop} onPress={() => this.setState({check_dongxop: !this.state.check_dongxop})} color="gray"/>
							</View>
							<View style={{flex: 1 }}>
								<Text style={{fontSize: 13}}>Đóng xốp</Text>
							</View>
						</View>
					</View>
					<View style={{flex: 1, flexDirection: 'row', marginBottom: 10}}>
						<View style={{flex: 1, flexDirection: 'row'}}>
							<View style={{flex: 0.5}}>
								<CheckBox style={{borderRadius: 100}} checked={this.state.check_shipho} onPress={() => this.setState({check_shipho: !this.state.check_shipho})} color="gray"/>
							</View>
							<View style={{flex: 1 }}>
								<Text style={{fontSize: 13}}>Ship hộ</Text>
							</View>
						</View>
						<View style={{flex: 2, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start'}}>
							{this.state.check_shipho ? (
							<RadioGroup radioButtons={this.state.radio_transport} flexDirection='row' size="16" onPress={(data) => this.setState({ shipping_transport_type_name : data.find(e => e.selected == true).value })} />) : (<View></View>) 
							}
						</View>
					</View>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this._search() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: '#fff', fontSize: 14}}>Tìm kiếm</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class FlatListItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			status_title: '',
			status_color: '#fff'
		 };
	}

	_getOrderStatus(status){
		if(this.props.order_status[status] != undefined){
			this.setState({
				status_title: this.props.order_status[status].title,
				status_color: '#'+this.props.order_status[status].color
			});
		}
	}

	_getImageSite(site){
		switch(site){
			case 'tmall':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/tmall_icon.png')}></Image>);
				break;
			case 'taobao':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/taobao_icon.png')}></Image>);
				break;
			case '1688':
				return (<Image style={{width: 20, height: 20}} source={require('../../images/1688_icon.png')}></Image>);
				break;
			default:
				return (<Image style={{width: 20, height: 20}} source={require('../../images/1688_icon.png')}></Image>);
		}
	}


	_doGoToDetailOrder(order_id){
		this.props.navigation.navigate(DetailOrderScreen, { order_id: order_id, navigation: this.props.navigation });
	}

	componentDidMount(){
		this._getOrderStatus(this.props.item.status);
	}

	render(){
		return(
			<TouchableOpacity onPress={ () => this._doGoToDetailOrder(this.props.item.id) }  style={{ padding: 10, margin: 5, backgroundColor: '#fff', borderRadius: 10, zIndex: 999 }}>
				<View style={{flex: 1, flexDirection: 'row', paddingVertical: 5, marginBottom: 5, borderBottomWidth: 0}} thumbnail>
					<View style={{flex: 1}}>
						<Thumbnail style={{borderWidth: 1, borderColor: '#ececec'}} square source={{ uri: this.props.item.avatar }} />
					</View>
					<View style={{flex: 5, paddingLeft: 10}}>
						<View style={{flex: 1}}>
							<View style={{flex: 3, justifyContent: 'flex-start', alignItems: 'flex-start'}}>
								<Text style={{ fontSize: 13, color: appConfig.baseColor, fontWeight: 'bold', padding: 1}}>{this.props.item.code}</Text>
							</View>
							<View style={{ flex: 2, position: 'absolute', right: 0}}>
								<Text style={{fontSize: 13, textAlign: 'center', justifyContent: 'center', backgroundColor: this.state.status_color, padding: 5, borderRadius: 5, color: 'rgb(255,255,255)'}}>{this.state.status_title}</Text>
							</View>
						</View>
						<View style={{flex: 1}}>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13}}>Số lượng: {this.props.item.total_order_quantity}</Text>
							</View>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13}}>{Platform.OS == 'ios' ? 'Shop:' : this._getImageSite(this.props.item.site)} {this.props.item.shop_name == null ? ' -- ' : this.props.item.shop_name}</Text>
							</View>
						</View>
					</View> 
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Tiền hàng</Text>
					</View>
					<View style={{flex: 1, alignItems: 'flex-end'}}>
						<NumberFormat value={this.props.item.fee.amount} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Phí VC nội địa TQ</Text>
					</View>
					<View style={{flex: 1, alignItems: 'flex-end'}}>
						<NumberFormat value={this.props.item.fee.domestic_shipping_fee_vnd} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13}}>{value} đ</Text>}/>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Tổng phí dịch vụ</Text>
					</View>
					<View style={{flex: 1, alignItems: 'flex-end'}}>
						<NumberFormat value={this.props.item.fee.fee_amount} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13 }}>{value} đ</Text>}/>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5, borderBottomWidth: 1, borderBottomColor: '#ececec', paddingBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Tổng giá trị đơn</Text>
					</View>
					<View style={{flex: 1, alignItems: 'flex-end'}}>
						<NumberFormat value={this.props.item.fee.total_amount} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, color: appConfig.baseColor }}>{value} đ</Text>}/>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Đã thanh toán</Text>
					</View>
					<View style={{flex: 1, alignItems: 'flex-end'}}>
						<NumberFormat value={this.props.item.fee.paymented} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, color: '#008000', fontWeight: 'bold' }}>{value} đ</Text>}/>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Cần thanh toán</Text>
					</View>
					<View style={{flex: 1, alignItems: 'flex-end'}}>
						<NumberFormat value={this.props.item.fee.need_payment} displayType={'text'} decimalScale={0} thousandSeparator={true} renderText={value => <Text style={{fontSize: 13, fontWeight: 'bold' }}>{value} đ</Text>}/>
					</View>
				</View>
			</TouchableOpacity>
		)
	}
}

export default class ListOrder extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			per_page: 10,
			page: 1,
			lastpage: false,
			total: 0,
			data: [],
			order_status: [],
			status: '',
			order_code: '',
			start: '',
			end: '',
			site: [],
			services: [],
			shipping_transport_type_name: ''
		}
	}
	_onPressSearch(){
		this.refs.modalSearch.showModalSearch();
	}

	freshDataOrder(){
		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				status: this.state.status,
				order_code: this.state.order_code,
				start: this.state.start,
				end: this.state.end,
				site: this.state.site,
				services: this.state.services,
				shipping_transport_type_name: this.state.shipping_transport_type_name
			};

			if(this.state.lastpage) return;
			//Get List Order
			this.setState({ 
				isLoading: true ,
				order_status: user_info.userInfo.order_status,
			});
			getListOrder(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ data: [] });
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	_doSearch(params){
		this.setState({
			per_page: 10,
			page: 1,
			lastpage: false,
			data: [],
			total: 0,
			status: params.status,
			order_code: params.order_code,
			start: params.start,
			end: params.end,
			site: params.site,
			services: params.services,
			shipping_transport_type_name: params.shipping_transport_type_name
		});

		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				status: params.status,
				order_code: params.order_code,
				start: params.start,
				end: params.end,
				site: params.site,
				services: params.services,
				shipping_transport_type_name: params.shipping_transport_type_name
			};
			if(this.state.lastpage) return;
			this.setState({ isLoading: true });
			getListOrder(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				//Dong modal search
				this.refs.modalSearch.closeModalSearch();
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ 
						data: [],
						total: 0
				 	});
				}

				
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshDataOrder();
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<Button style={{ justifyContent: 'flex-start', backgroundColor: '#ececec'}} onPress={() => this._onPressSearch() } iconLeft full>
					<Text style={{ fontSize: 13, color: appConfig.baseColor }}>{this.state.total} (Đơn hàng)</Text>
					<Icon style={{ color: appConfig.baseColor }} name='search' />
					<Text style={{ color: appConfig.baseColor, fontSize: 14, fontWeight: 'bold' }}>Tìm kiếm</Text>
				</Button>
				<FlatList
					data={this.state.data}
					renderItem={({item, index}) => {
						return(
							<FlatListItem order_status={this.state.order_status} navigation={this.props.navigation} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.id.toString()}
					onEndReached={() => {this.freshDataOrder()}}
					onEndReachedThreshold={0.1}
					extraData={this.state.data}
				>

				</FlatList>
				<ModalSearch order_status={this.state.order_status} ref={'modalSearch'} parentView={this}></ModalSearch>
			</View>
		)
	}
}
