import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListNotify, updateIsReadNotify, readAllNotify } from "../../requests/User";
import Loader from '../Loader';
import { ListTransactionScreen, DetailOrderScreen, MainScreen } from '../../ScreenNames';


class FlatListItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			notify_status: this.props.item.notify_status
		}
	}

	_goToDetailNotify(){
		if(this.props.item.object_type == "ORDER" && this.props.item.object_id > 0){
			//goto detail order
			this.props.navigation.navigate(DetailOrderScreen, { order_id: this.props.item.object_id, navigation: this.props.navigation });
		}

		if(this.props.item.object_type == "FINANCE"){
			//go to transaction list
			this.props.navigation.navigate(ListTransactionScreen, {navigation: this.props.navigation})
		}

		//REad notify
		if(this.props.item.notify_status == 'VIEW'){
			_retrieveData('login_info').then((user_info) => {
				const postData = { id: this.props.item.id };

				this.setState({isLoading: true });
				updateIsReadNotify(postData, user_info.userInfo.access_token).then((result) => {
					if(result.success == true){
						this.setState({ notify_status: 'WAS_VIEW', isLoading: false });
					}
				}).catch((error) => {
					Alert.alert('Thông báo', 'Có lỗi xảy ra trong quá trình cập nhật', [{text: 'OK' }], {cancelable: false});
				});
			});
		}
	}

	componentDidMount(){
		this.setState({ is_read: this.props.item.is_read });
	}

	render(){
		return(
			<TouchableOpacity onPress={ () => this._goToDetailNotify() }  >
				<View style={{ padding: 10, margin: 5, backgroundColor: 'white', borderRadius: 10}}>
					<View style={{flex: 1}}>
						<Text style={{fontSize: 13, fontWeight: 'bold'}}>{this.props.item.title}</Text>
					</View>
					<View style={{flex: 1}}>
						<Text style={{fontSize: 13, color: this.state.notify_status == 'VIEW' ? appConfig.baseColor : 'black'}}>{this.props.item.content}</Text>
					</View>
					<View style={{flex: 1}}>
						<Text style={{fontSize: 13, color: 'gray'}}>{this.props.item.created_at}</Text>
					</View>
				</View>
			</TouchableOpacity>
		)
	}
}

export default class ListNotify extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			per_page: 10,
			page: 1,
			lastpage: false,
			total: 0,
			data: [],
		}
	}

	freshDataNotifications(){
		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
			};

			if(this.state.lastpage) return;
			this.setState({ isLoading: true });
			getListNotify(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ data: [] });
				}


			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}


	_readAllNotify(){
		_retrieveData('login_info').then((user_info) => {
			this.setState({isLoading: true });
			readAllNotify(user_info.userInfo.access_token).then((result) => {
				if(result.success == true){
					this.setState({ page: 1, lastpage: false, data: []})
					this.freshDataNotifications();
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra trong quá trình cập nhật', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshDataNotifications();
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
					<Button disabled style={{  flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#ececec'}} iconLeft full light>
						<View style={{flex: 1}}>
							<Text style={{ fontSize: 13, color: appConfig.baseColor }}>{this.state.total} (Thông báo)</Text>
						</View>
						<View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
							<Button onPress={() => this._readAllNotify() } small style={{margin: 5, backgroundColor: appConfig.baseColor}}><Text style={{fontSize: 13 }}>Đọc tất cả</Text></Button>
						</View>
					</Button>

				<FlatList
					data={this.state.data}
					renderItem={({item, index}) => {
						return(
							<FlatListItem navigation={this.props.navigation} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.id.toString()}
					onEndReached={() => {this.freshDataNotifications()}}
					onEndReachedThreshold={0.1}
					extraData={this.state.data}
				>

				</FlatList>
			</View>
		)
	}
}
