import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import Modal from 'react-native-modalbox';
import NumberFormat from 'react-number-format';
import DatePicker from 'react-native-datepicker'
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListTransactionHistory } from "../../requests/Wallet";
import Loader from '../Loader';
import { MainScreen } from '../../ScreenNames';

var screen = Dimensions.get('window');

class ModalSearch extends Component{
	constructor(props){
		super(props);
		this.state = {
			transaction_code: '',
			transaction_type: '',
			start: '',
			end: '',
		};
	}

	_search(){
		var params = {
			transaction_code: this.state.transaction_code,
			transaction_type: this.state.transaction_type,
			start: this.state.start,
			end: this.state.end
		}
		this.props.parentView._doSearch(params);
	}

	showModalSearch(){
		this.refs.modalSearch.open();
	}

	closeModalSearch(){
		this.refs.modalSearch.close();
	}

	changeTypeSelected(value: string) {
		this.setState({ transaction_type: value });
	}

	render(){
		return(
			<Modal
				ref={'modalSearch'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height: 250,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5}}>
				<Item regular>
					<Input
						style={{fontSize: 13}}
						placeholder="Mã giao dịch"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({transaction_code: text})}
						value={this.state.transaction_code}
					/>
				</Item>
				<Item style={{paddingVertical: 10, paddingRight: 10}}>
				<DatePicker
					style={{width: '50%', marginRight: 5 }}
					mode="date"
					date={this.state.start}
					placeholder="Thời gian từ ngày"
					format="DD-MM-YYYY"
					confirmBtnText="Đồng ý"
					cancelBtnText="Hủy"
					showIcon={false}
					onDateChange={(date) => {this.setState({start: date})}}
				  />
				  <DatePicker
  					style={{width: '50%', marginLeft: 5}}
  					mode="date"
					date={this.state.end}
  					placeholder="Tới ngày"
  					format="DD-MM-YYYY"
  					confirmBtnText="Đồng ý"
  					cancelBtnText="Hủy"
  					showIcon={false}
  					onDateChange={(date) => {this.setState({end: date})}}
  				  />
				</Item>
				<Item picker>
					<Text style={{fontSize: 13}}>Loại giao dịch: </Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						style={{ width: undefined }}
						selectedValue={this.state.transaction_type}
						onValueChange={this.changeTypeSelected.bind(this)}
						>
						<Picker.Item label="Tất cả" value="" />
						{Object.keys(this.props.transaction_status).map((type, i) => {
							return <Picker.Item key={i} value={type} label={this.props.transaction_status[type]} />
						})}
					</Picker>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this._search() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 14}}>Tìm kiếm</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class FlatListItem extends Component{


	render(){
		return(
			<View style={{ padding: 10, margin: 5, backgroundColor: 'white', borderRadius: 10}}>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 14, color: appConfig.baseColor }}>Mã giao dịch</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 14, color: appConfig.baseColor }}>{this.props.item.transaction_code}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Thời gian</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13}}>{this.props.item.created_at_format}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Số tiền</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<NumberFormat value={this.props.item.amount} displayType={'text'} thousandSeparator={true} renderText={value => <Text style={{ color: this.props.item.amount < 0 ? 'red' : 'green', fontSize: 13}}>{this.props.item.amount < 0 ? '' : '+'}{value} đ</Text>}/>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Số dư</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<NumberFormat value={this.props.item.ending_balance} displayType={'text'} thousandSeparator={true} renderText={value => <Text style={{ fontSize: 13}}>{value} đ</Text>}/>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Nội dung</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13}}>{this.props.item.transaction_note}</Text>
					</View>
				</View>
			</View>
		)
	}
}

export default class ListTransaction extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			per_page: 10,
			page: 1,
			lastpage: false,
			total_transactions: 0,
			transactions: [],
			transaction_status: [],
			transaction_type: '',
			transaction_code: '',
			start: '',
			end: ''
	}
	}
	_onPressSearch(){
		this.refs.modalSearch.showModalSearch();
	}

	freshDataTransaction(){
		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.start,
				transaction_type: this.state.transaction_type,
				transaction_code: this.state.transaction_code,
				start: this.state.start,
				end: this.state.end
			};

			if(this.state.lastpage) return;
			this.setState({ 
				isLoading: true,
				transaction_status: user_info.userInfo.transaction_status
			});
			getListTransactionHistory(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.transactions == null || result.transactions.length == 0 || this.state.lastpage) return;
					this.setState({
						transactions: this.state.transactions.concat(result.transactions),
						total_transactions: result.total_transactions,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.transactions.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ transactions: [] });
				}


			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	_doSearch(params){
		this.setState({
			per_page: 10,
			page: 1,
			lastpage: false,
			transactions: [],
			total_transactions: 0,
			transaction_type: params.transaction_type,
			transaction_code: params.transaction_code,
			start: params.start,
			end: params.end
		});

		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				transaction_type: params.transaction_type,
				transaction_code: params.transaction_code,
				start: params.start,
				end: params.end
			};

			if(this.state.lastpage) return;
			this.setState({ isLoading: true });
			getListTransactionHistory(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				//Dong modal search
				this.refs.modalSearch.closeModalSearch();
				if(result.success == true){
					if (result.transactions == null || result.transactions.length == 0 || this.state.lastpage) return;
					this.setState({
						transactions: this.state.transactions.concat(result.transactions),
						total_transactions: result.total_transactions,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.transactions.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ 
						transactions: [],
						total_transactions: 0
					});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshDataTransaction();
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<Button style={{ justifyContent: 'flex-start', backgroundColor: '#ececec'}} onPress={() => this._onPressSearch() } iconLeft full light>
					<Text style={{ fontSize: 13, color: appConfig.baseColor }}>{this.state.total_transactions} (Giao dịch)</Text>
					<Icon style={{ color: appConfig.baseColor }} name='search' />
					<Text style={{ color: appConfig.baseColor, fontSize: 14, fontWeight: 'bold' }}>Tìm kiếm</Text>
				</Button>
				<FlatList
					data={this.state.transactions}
					renderItem={({item, index}) => {
						return(
							<FlatListItem item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.transaction_code.toString()}
					onEndReached={() => {this.freshDataTransaction()}}
					onEndReachedThreshold={0.1}
					extraData={this.state.transactions}
				>

				</FlatList>
				<ModalSearch transaction_status={this.state.transaction_status} ref={'modalSearch'} parentView={this}></ModalSearch>
			</View>
		)
	}
}
