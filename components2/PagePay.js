import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import {Platform} from 'react-native';
import { Button, Icon } from 'native-base';
import { appConfig } from "../AppConfig";
import { _retrieveData } from '../Helpers';
import { MainScreen } from '../ScreenNames';

export default class PagePay extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = { url_page: '' };
	}

	componentDidMount(){
		_retrieveData('login_info').then((user_info) => {
			this.setState({ url_page: appConfig.baseUrl + user_info.userInfo.url_payment })
		});
	}

	render() {
		return (
			<WebView source={{ uri: Platform.OS === 'ios' ? 'https://nhaphangthuongmai.com/huong-dan-nap-tien.html?only_content=1' : 'http://admin.nhaphangthuongmai.com/huong-dan-nap-tien.html?only_content=1' }} />
		);
	}
}
