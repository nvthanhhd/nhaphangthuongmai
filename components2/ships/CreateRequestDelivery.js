import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input, CheckBox } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import Modal from 'react-native-modalbox';
import DatePicker from 'react-native-datepicker'
import NumberFormat from 'react-number-format';
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListWaitDelivery, getDeliveryInfo, createDelivery } from "../../requests/Ship";
import Loader from '../Loader';
import {  MainScreen } from '../../ScreenNames';

var screen = Dimensions.get('window');

class ModalCodNote extends Component{
	constructor(props){
		super(props);
		this.state = {
			cod: '',
			note: '',
		};
	}

	saveCodeNote(){
		this.props.parentView.saveCodeNote(this.state.cod, this.state.note);
		this.closeModalCodNote();
	}

	resetCodNote(){
		this.setState({ cod: '', note: ''});
	}

	showModalCodNote(){
		this.refs.modalCodNote.open();
	}

	closeModalCodNote(){
		this.refs.modalCodNote.close();
	}

	render(){
		return(
			<Modal
				ref={'modalCodNote'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:200,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5}}>
				<Item regular style={{marginTop: 10, marginBottom: 10}}>
					<Input
						style={{fontSize: 13}}
						placeholder="COD"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({cod: text})}
						value={this.state.cod}
					/>
				</Item>
				<Item regular>
					<Input
						style={{fontSize: 13}}
						placeholder="Ghi chú"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({note: text})}
						value={this.state.note}
					/>
				</Item>
				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this.saveCodeNote() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 14}}>Lưu</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class FlatListItem extends Component{
	constructor(props){
		super(props);
		this.state = { 
			item_selected: false,
			check_all_item: false
		}
	}

	selectPackage(){
		this.setState({ item_selected: !this.state.item_selected });
		this.props.parentFlatList.addAndRemoveItem(this.props.item.id, this.props.item.order_id, !this.state.item_selected);
	}

	componentWillReceiveProps(newProps) {
		if(newProps.check_all_item != undefined && newProps.check_all_item != this.state.check_all_item){
			this.setState({
				item_selected: newProps.check_all_item,
				check_all_item: newProps.check_all_item
			});
		}
	}

	render(){
		return(
			<View style={{ padding: 10, margin: 5, backgroundColor: 'white', borderRadius: 10}}>
				<View style={{flex: 1, flexDirection: 'row'}}>
					<View style={{flex: 1, alignItems: 'flex-start', justifyContent: 'center'}}>
						<CheckBox style={{borderRadius: 100}} onPress={() => this.selectPackage()} checked={this.state.item_selected} color={appConfig.baseColor} />
					</View>
					<View style={{flex: 7, borderLeftColor: '#ececec', borderLeftWidth: 1, paddingLeft: 10}}>
						<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13, color : appConfig.baseColor }}>Kiện hàng</Text>
							</View>
							<View style={{flex: 1.5, alignItems: 'flex-end'}}>
								<Text style={{ fontSize: 13, color : appConfig.baseColor}}>{this.props.item.logistic_package_barcode}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13}}>Đơn hàng</Text>
							</View>
							<View style={{flex: 1.5, alignItems: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{this.props.item.order_code}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13}}>Kho hiện tại</Text>
							</View>
							<View style={{flex: 1.5, alignItems: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{this.props.item.current_warehouse_name}</Text>
							</View>
						</View>
						<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
							<View style={{flex: 1}}>
								<Text style={{ fontSize: 13}}>Cân nặng</Text>
							</View>
							<View style={{flex: 1.5, alignItems: 'flex-end'}}>
								<Text style={{ fontSize: 13 }}>{this.props.item.calculate_weight} kg</Text>
							</View>
						</View>
					</View>
				</View>
				
			</View>
		)
	}
}

export default class CreateRequestDelivery extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};
	
	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			data: [],
			selected: {},
			check_all_item: false,
			cod: '',
			note: '',
			user_address: []
		}
	}

	freshdataWaitDelivery(){
		_retrieveData('login_info').then((user_info) => {
			this.setState({  isLoading: true });
			getListWaitDelivery(user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.data == null || result.data.length == 0 ) return;
					this.setState({
						data: result.data,
						user_address: result.user_address
					});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	addAndRemoveItem(id, order_id, is_add){
		 var selected = this.state.selected;

		 if(is_add){
		 	selected[id] = {package_id: id, order_id: order_id };
		 }else{
		 	delete selected[id];
		 }

		 this.setState({ selected: selected });
	}

	onCheckAllItem(){
		var selected = {};
		this.setState({ 
			check_all_item: !this.state.check_all_item,
		 });
		if(!this.state.check_all_item){
			for(const i in this.state.data){
				selected[this.state.data[i].id] = {package_id: this.state.data[i].id, order_id: this.state.data[i].order_id };
			}
		}
		this.setState({selected: selected });
		
	}

	createRequestDelivery(){
		var selected = this.state.selected;
		if(Object.values(selected).length == 0){
			Alert.alert('Thông báo', 'Vui lòng chọn kiện yêu cầu giao hàng', [{text: 'OK' }], {cancelable: false});
			return false;
		}

		_retrieveData('login_info').then((user_info) => {
			//Get params
			const keyValuePairs = [];
			var i = 0;
			for (const key in selected) {
				for (const key_child in selected[key]) {
					keyValuePairs.push(encodeURIComponent('data['+i+']['+key_child+']') + '=' + encodeURIComponent(selected[key][key_child]));
				}
				i++;
			}
			var queryStringParams = keyValuePairs.join('&');
			//
			getDeliveryInfo(queryStringParams, user_info.userInfo.access_token).then((result) => {
				if(result.success == true){
					if(result.money < result.account_balance && result.need_money == 0){
						//Đủ điều kiện tạo YC
						this.doCreateRequestDelivery();
					}else if(result.need_money > 0){
						Alert.alert('Thông báo', 'Không thể tạo YC giao hàng, còn thiếu '+ result.need_money + ' đ', [{text: 'OK' }], {cancelable: false});
					}
				}else{
					Alert.alert('Thông báo', 'Không thể tạo YC giao hàng', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	doCreateRequestDelivery(){
		_retrieveData('login_info').then((user_info) => {
			var postData = {
				package_ids: Object.keys(this.state.selected),
				cod: this.state.cod, 
				note: this.state.note
			}
			this.setState({  isLoading: true });

			createDelivery(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					Alert.alert('Thông báo', 'Tạo thành công YC giao hàng', [{text: 'OK' }], {cancelable: false});
					this.setState({
						selected: {},
						check_all_item: false,
						cod: '',
						note: '',
					});
					this.resetCodNote();
					this.freshdataWaitDelivery();
				}else{
					Alert.alert('Thông báo', 'Không thể tạo YC giao hàng', [{text: 'OK' }], {cancelable: false});
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	resetCodNote(){
		this.refs.modalCodNote.resetCodNote();
	}

	saveCodeNote(cod, note){
		this.setState({ cod: code, note: note });
	}

	showModalCodNote(){
		this.refs.modalCodNote.showModalCodNote();
	}

	componentDidMount(){
		this.freshdataWaitDelivery();
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				
				<FlatList
					data={this.state.data}
					renderItem={({item, index}) => {
						return(
							<FlatListItem check_all_item={this.state.check_all_item} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.id.toString()}
				>
				</FlatList>
				<View style={{height: 100, backgroundColor: '#e6f7ff'}}>
					<View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
						<View style={{flex: 7}}>
							<Item style={{flex: 1, flexDirection: 'row', borderBottomColor: '#efefef'}} thumbnail>
								<View style={{flex: 1}}>
									<CheckBox style={{borderRadius: 100}} color={appConfig.baseColor} checked={this.state.check_all_item} onPress={() => { this.onCheckAllItem() }}/>
								</View>
								<View style={{flex: 5}}>
									<Text style={{ color: appConfig.baseColor, fontSize: 13}}>  Chọn tất cả</Text>
								</View>
							</Item>
						</View>
						<View style={{flex: 5, marginRight: 10}}>
							<Button iconLeft bordered small onPress={() => this.showModalCodNote() }>
								<Icon name="edit" type="FontAwesome" style={{fontSize: 16, color: appConfig.baseColor }} />
								<Text style={{ fontSize: 12, left: -15, color: appConfig.baseColor}}> Cod, ghi chú</Text>
							</Button>
						</View>
						<View style={{flex: 3, paddingRight: 10}}>
							<Button style={{ backgroundColor: appConfig.baseColor, padding: 5, alignItems: 'center', justifyContent: 'center'}} small iconLeft onPress={() => this.createRequestDelivery()}>
								<Label style={{ color: 'white', fontSize: 14}}>Tạo YC</Label>
							</Button>
						</View>
					</View>
					<View style={{flex: 1, padding: 10 }}>
						<Text style={{ fontSize: 13}}>Địa chỉ nhận hàng: {this.state.user_address.reciver_name} / {this.state.user_address.reciver_phone} / {this.state.user_address.detail}</Text>
					</View>
				</View>
				<ModalCodNote ref={'modalCodNote'} parentView={this}></ModalCodNote>
			</View>
		)
	}
}
