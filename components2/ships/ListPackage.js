import React, { Component } from 'react';
import { Button, Label, Icon, Text, Right, Left, Item, Body, Picker, Input } from 'native-base';
import { Dimensions, Platform, TextInput, View, Alert, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import Modal from 'react-native-modalbox';
import DatePicker from 'react-native-datepicker'
import { appConfig } from "../../AppConfig";
import { _retrieveData } from '../../Helpers';
import { getListPackage } from "../../requests/Ship";
import Loader from '../Loader';
import {  MainScreen } from '../../ScreenNames';

var screen = Dimensions.get('window');

class ModalSearch extends Component{
	constructor(props){
		super(props);
		this.state = {
			order_code: '',
			logistic_package_barcode: '',
			start: '',
			end: '',
			status: ''
		};
	}

	_search(){
		var params = {
			order_code: this.state.order_code,
			logistic_package_barcode: this.state.logistic_package_barcode,
			start: this.state.start,
			end: this.state.end,
			status: this.state.status
		}
		this.props.parentView._doSearch(params);
	}

	showModalSearch(){
		this.refs.modalSearch.open();
	}

	closeModalSearch(){
		this.refs.modalSearch.close();
	}

	changeCurStatus(value: string) {
		this.setState({
			status: value
		});
	}

	render(){
		return(
			<Modal
				ref={'modalSearch'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					marginTop: 50,
					height:300,
					borderRadius: 5
				}}
				position='top'
				backdrop={true}
			>
			<View style={{flex: 1, borderRadius: 5, padding: 5}}>
				<Item regular>
					<Input
						style={{fontSize: 13}}
						placeholder="Mã đơn hàng"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({order_code: text})}
						value={this.state.order_code}
					/>
				</Item>
				<Item regular style={{marginTop: 10}}>
					<Input
						style={{fontSize: 13}}
						placeholder="Mã kiện"
						returnKeyType='next'
						autoCorrect={false}
						onChangeText={(text) => this.setState({logistic_package_barcode: text})}
						value={this.state.logistic_package_barcode}
					/>
				</Item>
				<Item picker>
					<Text style={{fontSize: 13}}>Trạng thái: </Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						style={{ width: undefined }}
						selectedValue={this.state.status}
						onValueChange={this.changeCurStatus.bind(this)}
						>
						<Picker.Item label="Tất cả" value="" />
						{Object.values(this.props.package_status).map((value, i) => {
							return <Picker.Item key={i} value={value.key} label={value.title} />
						})}
					</Picker>
				</Item>
				<Item style={{paddingVertical: 10, paddingRight: 10}}>
				<DatePicker
					style={{width: '50%', marginRight: 5 }}
					mode="date"
					date={this.state.start}
					placeholder="Thời gian từ ngày"
					format="YYYY-MM-DD"
					confirmBtnText="Đồng ý"
					cancelBtnText="Hủy"
					showIcon={false}
					onDateChange={(date) => {this.setState({start: date})}}
				  />
				  <DatePicker
  					style={{width: '50%', marginLeft: 5}}
  					mode="date"
					date={this.state.end}
  					placeholder="Tới ngày"
  					format="YYYY-MM-DD"
  					confirmBtnText="Đồng ý"
  					cancelBtnText="Hủy"
  					showIcon={false}
  					onDateChange={(date) => {this.setState({end: date})}}
  				  />
				</Item>

				<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
					<Button onPress={() => this._search() } block style={{
						backgroundColor: appConfig.baseColor,
					 	fontSize: 18,
						padding: 8,
						marginHorizontal: 100,
						height: 40
					}}>
						<Text style={{ color: 'white', fontSize: 14}}>Tìm kiếm</Text>
					</Button>
				</View>
				</View>
			</Modal>
		);
	}
}

class FlatListItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			status_title: '',
			status_color: ''
		 };
	}

	_getPackageStatus(status){
		if(this.props.package_status[status] != undefined){
			this.setState({
				status_title: this.props.package_status[status].title,
				status_color: '#'+this.props.package_status[status].color
			});
		}
	}

	componentDidMount(){
		this._getPackageStatus(this.props.item.status);
	}

	render(){
		return(
			<View style={{ padding: 10, margin: 5, backgroundColor: 'white', borderRadius: 10}}>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 14, color: appConfig.baseColor }}>Mã kiện</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 14, color: appConfig.baseColor }}>{this.props.item.logistic_package_barcode}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Mã đơn</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13, borderWidth: 1, borderColor: 'gray', borderRadius: 5, padding: 5}}>{this.props.item.order.code}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Cân nặng</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.calculate_weight} kg</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Dài / rộng / cao (cm)</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.length_package == null ? '-' : this.props.item.length_package} / {this.props.item.width_package == null ? '-' : this.props.item.width_package} / {this.props.item.height_package == null ? '-' : this.props.item.height_package}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Trạng thái</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{fontSize: 13, textAlign: 'center', justifyContent: 'center', backgroundColor: this.state.status_color, padding: 5, borderRadius: 5, color: 'white'}}>{this.state.status_title}</Text>
					</View>
				</View>
				<View style={{flex: 1, flexDirection: 'row', marginBottom: 5}}>
					<View style={{flex: 1}}>
						<Text style={{ fontSize: 13}}>Thời gian cập nhật</Text>
					</View>
					<View style={{flex: 1.5, alignItems: 'flex-end'}}>
						<Text style={{ fontSize: 13 }}>{this.props.item.time_with_status}</Text>
					</View>
				</View>
			</View>
		)
	}
}

export default class ListPackage extends Component{
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		this.state = {
			isLoading: false,
			per_page: 10,
			page: 1,
			lastpage: false,
			total: 0,
			data: [],
			package_status: [],
			order_code: '',
			status: '',
			logistic_package_barcode: '',
			start: '',
			end: '',

		}
	}
	_onPressSearch(){
		this.refs.modalSearch.showModalSearch();
	}

	freshdataPackages(){
		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				order_code: this.state.order_code,
				status: this.state.status,
				logistic_package_barcode: this.state.logistic_package_barcode,
				start: this.state.start,
				end: this.state.end,
			};
			if(this.state.lastpage) return;
			this.setState({ 
				isLoading: true,
				package_status: user_info.userInfo.package_status
			});
			getListPackage(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ data: [] });
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	_doSearch(params){
		this.setState({
			per_page: 10,
			page: 1,
			lastpage: false,
			total: 0,
			data: [],
			order_code: params.order_code,
			status: params.status,
			logistic_package_barcode: params.logistic_package_barcode,
			start: params.start,
			end: params.end,
		});

		_retrieveData('login_info').then((user_info) => {
			const paramsObj = {
				per_page: this.state.per_page,
				page: this.state.page,
				order_code: params.order_code,
				status: params.status,
				logistic_package_barcode: params.logistic_package_barcode,
				start: params.start,
				end: params.end,
			};

			if(this.state.lastpage) return;
			this.setState({ isLoading: true });
			getListPackage(paramsObj, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				//Dong modal search
				this.refs.modalSearch.closeModalSearch();
				if(result.success == true){
					if (result.data == null || result.data.length == 0 || this.state.lastpage) return;
					this.setState({
						data: this.state.data.concat(result.data),
						total: result.total,
						per_page: this.state.per_page,
						page: this.state.page + 1,
						lastpage: result.data.length < this.state.per_page ? true : false
					});
				}else{
					this.setState({ 
						data: [],
						total: 0
				 	});
				}

				
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra', [{text: 'OK' }], {cancelable: false});
			});
		});
	}

	componentDidMount(){
		this.freshdataPackages();
	}

	render(){
		return (
			<View style={{backgroundColor: '#f1f1f1', flex: 1}}>
				<Loader loading={this.state.isLoading} />
				<Button style={{ justifyContent: 'flex-start', backgroundColor: '#ececec'}} onPress={() => this._onPressSearch() } iconLeft full light>
					<Text style={{ fontSize: 13, color: appConfig.baseColor }}>{this.state.total} (Kiện hàng)</Text>
					<Icon style={{ color: appConfig.baseColor }} name='search' />
					<Text style={{ color: appConfig.baseColor,  fontWeight: 'bold', fontSize: 14 }}>Tìm kiếm</Text>
				</Button>
				<FlatList
					data={this.state.data}
					renderItem={({item, index}) => {
						return(
							<FlatListItem package_status={this.state.package_status} item={item} index={index} parentFlatList={this}></FlatListItem>
						)
					}}

					keyExtractor={(item, index) => item.id.toString()}
					onEndReached={() => {this.freshdataPackages()}}
					onEndReachedThreshold={0.1}
					extraData={this.state.data}
				>

				</FlatList>
				<ModalSearch package_status={this.state.package_status} ref={'modalSearch'} parentView={this}></ModalSearch>
			</View>
		)
	}
}
