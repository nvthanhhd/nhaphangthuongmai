import React, { Component } from 'react';
import { Alert, ActivityIndicator, View } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Button, Picker, Icon } from 'native-base';
import { ProfileScreen, MainScreen } from '../ScreenNames';
import { getUserInfo, updateInfo } from "../requests/User";
import { validatePhoneNumber } from '../Validate';
import { _storeData, _retrieveData, getExpireDate } from '../Helpers';
import { appConfig } from "../AppConfig";

export default class EditProfile extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam('title'),
			headerLeft: () => (<Button iconLeft transparent onPress={() => navigation.goBack()}><Icon style={{color: '#fff'}} name='arrow-back' /></Button>),
			headerRight: () =>
				<Button transparent large onPress={() => navigation.navigate(MainScreen, {active_screen: 'home'})} style={{ justifyContent: 'center'}}>
					<Icon type="FontAwesome" name='home' style={{ fontSize: 26, color: '#FFF', marginRight: 5}} />
				</Button>
		};
	};

	constructor(props){
		super(props);
		const user_info = this.props.navigation.getParam('user_info');
		this.state = {
			isLoading: false,
			name: user_info.name,
			mobile_phone: user_info.mobile_phone,
			txtErrorUpdate: '',
			showPassword: false,
			password: ''
		};
	}

	componentDidMount(){

	}

	onSubmitFormEditProfile(){
		//Validate full name
		if(this.state.name == ""){
			this.setState({ txtErrorUpdate: 'Vui lòng nhập vào họ và tên' });
			return false;
		}

		//Validate PhoneNumber
		if(this.state.mobile_phone == ""){
			this.setState({ txtErrorUpdate: 'Vui lòng nhập vào số điện thoại' });
			return false;
		}

		if(!validatePhoneNumber(this.state.mobile_phone)) {
			this.setState({ txtErrorUpdate: 'Số điện thoại không đúng' });
			return false;
		}else{
			this.setState({ txtErrorUpdate: '' });
		}

		if(this.state.password != "" && this.state.password.length < 6){
			this.setState({ errorRegistration: 'Mật khẩu phải có ít nhất 6 ký tự' });
			return false;
		}

		this.setState({ isLoading: true });
		_retrieveData('login_info').then((user_info) => {
			//Post data to server
			const postData = {
				name: this.state.name,
				mobile_phone: this.state.mobile_phone,
				password: this.state.password,
			};
			updateInfo(postData, user_info.userInfo.access_token).then((result) => {
				this.setState({ isLoading: false });
				if(result.success == true){
					Alert.alert('Thông báo', 'Cập nhật thành công', [{text: 'OK' }], {cancelable: false});
					//back to profile screen
					this.props.navigation.navigate(ProfileScreen);
				}else{
					this.setState({ txtErrorUpdate: result.message });
				}
			}).catch((error) => {
				Alert.alert('Thông báo', 'Có lỗi xảy ra trong quá trình cập nhật', [{text: 'OK', onPress: () => this.setState({ isLoading: false })}], {cancelable: false});
			});
		});
		
	}

	render() {
		if(this.state.isLoading){
			return(
				<View style={{flex: 1, padding: 20}}>
					<ActivityIndicator/>
				</View>
			)
		}

		return (
			<Container>
				<Content>
					<Form>
						<Item floatingLabel>
							<Label style={{fontSize: 13}}>Họ và tên</Label>
							<Input
								style={{fontSize: 13}}
								returnKeyType='next'
								autoCorrect={false}
								onChangeText={(text) => this.setState({name: text})}
								value={this.state.name}
							/>
						</Item>
						<Item floatingLabel>
							<Label style={{fontSize: 13}}>Điện thoại</Label>
							<Input
								style={{fontSize: 13}}
								returnKeyType='next'
								keyboardType="number-pad"
								autoCorrect={false}
								onChangeText={(text) => this.setState({mobile_phone: text})}
								value={this.state.mobile_phone}
							/>
						</Item>
						<Item floatingLabel>
							<Label style={{fontSize: 13}}>Mật khẩu</Label>
							<Input
								style={{fontSize: 13}}
								returnKeyType='go'
								autoCorrect={false}
								secureTextEntry={this.state.showPassword ? false : true}
								onChangeText={(text) => this.setState({password: text})}
								value={this.state.password}
							/>
							<Icon active name={this.state.showPassword ? 'eye-off' : 'eye'} style={{left: -5, opacity: 0.8}} onPress={() => this.setState({ showPassword: !this.state.showPassword })}/>
						</Item>
						<Label style={{ marginTop: 5, color: 'red', padding: 10}}>{this.state.txtErrorUpdate}</Label>
						<Button block style={{ marginTop: 10, backgroundColor: appConfig.baseColor, margin: 10}} onPress={this.onSubmitFormEditProfile.bind(this)}>
							<Label style={{ color: 'white', fontSize: 14}}>Cập nhật thông tin</Label>
						</Button>
					</Form>
				</Content>
			</Container>
			);
			}
}
