/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator, BackHandler, Alert } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Badge, Title, Left, Right, Body, Drawer } from 'native-base';
import HomeView from "./HomeView";
import ListCart from './carts/ListCart';
import Profile from './Profile';
import ListNotify from './users/ListNotify';
import { getDataUserHome } from "../requests/User";
import { _storeData, _retrieveData } from '../Helpers';
import { appConfig } from "../AppConfig";

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			titleScreen: 'Trang chủ',
			screen: undefined,
			screenActive: 'home',
			total_shop: '0',
			total_notify: '0',
		}
	}

	UNSAFE_componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
	}

	handleBackButton() {
		return true;
	}

	componentDidMount() {
		BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
		this.getHomeScreen();

		this.willFocus = this.props.navigation.addListener('willFocus', () => {
			if (this.props.navigation.getParam('active_screen') != undefined) {
				this.changeScreenActive(this.props.navigation.getParam('active_screen'));
			}
		});
	}

	getHomeScreen() {
		_retrieveData('login_info').then((user_info) => {
			getDataUserHome(user_info.userInfo.access_token).then((result) => {
				if (result.error == undefined) {
					this.setState({
						isLoading: false,
						total_shop: result.total_shop,
						total_notify: result.total_notify,
						screen: <HomeView navigation={this.props.navigation} mainView={this} />,
					});
				} else {
					Alert.alert('Thông báo', 'Có lỗi trong quá trình lấy dữ liệu!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
	}

	refreshDataHome(){
		_retrieveData('login_info').then((user_info) => {
			getDataUserHome(user_info.userInfo.access_token).then((result) => {
				if (result.error == undefined) {
					this.setState({
						isLoading: false,
						total_shop: result.total_shop,
						total_notify: result.total_notify,
					});
				} else {
					Alert.alert('Thông báo', 'Có lỗi trong quá trình lấy dữ liệu!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});
	}

	changeScreenActive(screen) {
		var screenCom, titleScreen, screenActive;
		if (screen == 'home') {
			screenCom = <HomeView currency_rate={this.state.currency_rate}
				total_cart_product={this.state.total_cart_product}
				user_credit={this.state.user_credit}
				total_order={this.state.total_order}
				total_complain={this.state.total_complain}
				site_notification={this.state.site_notification}
				cid={this.state.cid}
				navigation={this.props.navigation}
				mainView={this} />;
			titleScreen = 'Trang chủ';
		} else if (screen == 'cart') {
			screenCom = <ListCart mainView={this} navigation={this.props.navigation} />;
			titleScreen = 'Giỏ hàng';
		} else if (screen == 'notify') {
      screenCom = (
        <ListNotify mainView={this} navigation={this.props.navigation} />
      );
			titleScreen = 'Thông báo';
		} else if (screen == 'profile') {
			screenCom = <Profile mainView={this} navigation={this.props.navigation} hotline={this.state.hotline} />;
			titleScreen = 'Cá nhân';
		}

		this.setState({
			screen: screenCom,
			titleScreen: titleScreen,
			screenActive: screen
		});
	}

	render() {
		if (this.state.isLoading) {
			return (
				<View style={{ flex: 1, padding: 20, justifyContent: 'center', alignItems: 'center' }}>
					<ActivityIndicator />
				</View>
			)
		}

		return (
			<Container>
				<Header style={{ backgroundColor: appConfig.baseColor }}>
					<Left style={{ flex: 1 }} />
					<Body style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
						<Title style={{ color: 'white' }}>{this.state.titleScreen}</Title>
					</Body>
					<Right style={{ flex: 1, backgroundColor: 'green' }} />
				</Header>
				{this.state.screen}
				<Footer >
					<FooterTab style={{ backgroundColor: '#ececec' }}>
						<Button vertical onPress={() => this.changeScreenActive('home')}>
							<Icon type="FontAwesome" name='home' style={{ fontSize: 24, color: this.state.screenActive == 'home' ? appConfig.baseColor : 'gray' }} />
							<Text style={{ fontSize: 12, color: this.state.screenActive == 'home' ? appConfig.baseColor : 'gray', textTransform: 'capitalize' }}>Trang chủ</Text>
						</Button>
						<Button badge vertical onPress={() => this.changeScreenActive('cart')}>
							<Badge><Text>{this.state.total_shop}</Text></Badge>
							<Icon type="FontAwesome" name='shopping-cart' style={{ fontSize: 24, color: this.state.screenActive == 'cart' ? appConfig.baseColor : 'gray' }} />
							<Text style={{ fontSize: 12, color: this.state.screenActive == 'cart' ? appConfig.baseColor : 'gray', textTransform: 'capitalize' }}>Giỏ hàng</Text>
						</Button>
						<Button badge vertical onPress={() => this.changeScreenActive('notify')}>
							<Badge ><Text>{this.state.total_notify}</Text></Badge>
							<Icon active type="FontAwesome" name='bell' style={{ fontSize: 24, color: this.state.screenActive == 'notify' ? appConfig.baseColor : 'gray' }} />
							<Text style={{ fontSize: 12, color: this.state.screenActive == 'notify' ? appConfig.baseColor : 'gray', textTransform: 'capitalize' }}>Thông báo</Text>
						</Button>
						<Button vertical onPress={() => this.changeScreenActive('profile')}>
							<Icon type="FontAwesome" name='user' style={{ fontSize: 24, color: this.state.screenActive == 'profile' ? appConfig.baseColor : 'gray' }} />
							<Text style={{ fontSize: 12, color: this.state.screenActive == 'profile' ? appConfig.baseColor : 'gray', textTransform: 'capitalize' }}>Cá nhân</Text>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}
