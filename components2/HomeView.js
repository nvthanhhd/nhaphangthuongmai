/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, StyleSheet, Alert, Animated, Dimensions, Text, TouchableOpacity, Linking, ScrollView } from 'react-native';
import { Icon, Button } from 'native-base';
import NumberFormat from 'react-number-format';
import Modal from 'react-native-modalbox';
import HTML from 'react-native-render-html';
import { appConfig } from "../AppConfig";
import { getDataUserHome } from "../requests/User";
import { listSiteSearch } from '../requests/WebOrder';
import { _storeData, _retrieveData } from '../Helpers';
import { ListOrderScreen, ListTransactionScreen, ListPackageScreen, ListComplainScreen, PagePayScreen, PagePriceScreen, SearchProductScreen } from '../ScreenNames';

var screen = Dimensions.get('window');
class ModalDetailNotification extends Component {
	constructor(props) {
		super(props);
	}

	openModalDetailNotification() {
		this.refs.modalDetailNotification.open();
		var content_notity = JSON.stringify({ content_notify: this.props.content_notify });
		_storeData("content_notify", content_notity);
	}

	closeModalDetailNotification() {
		this.refs.modalDetailNotification.close();
	}

	render() {
		return (
			<Modal
				ref={'modalDetailNotification'}
				style={{
					justifyContent: 'center',
					shadowRadius: 10,
					width: screen.width - 10,
					borderRadius: 5,
					backgroundColor: 'transparent'
				}}
				position='top'
				backdrop={true}
			>
				<View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
					<TouchableOpacity onPress={() => this.closeModalDetailNotification()}>
						<Icon name="close" style={{ fontSize: 30, color: appConfig.baseColor, marginRight: 5 }} />
					</TouchableOpacity>
				</View>
				<ScrollView style={{ borderRadius: 5, padding: 5, backgroundColor: '#fcf8e3', borderColor: '#faebcc' }}>
					<HTML html={this.props.content_notify} />
				</ScrollView>
			</Modal>
		);
	}
}

export default class HomeView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			exchange_rate: '',
			content_notify: '',
			total_order: '',
			total_complaint: '',
			account_balance: '',
			is_new_notify: false,
			transform: new Animated.Value(0),
			hide_search: false,
		};
	}

	componentDidMount() {

		_retrieveData('login_info').then((user_info) => {
			getDataUserHome(user_info.userInfo.access_token).then((result) => {
				if (result.error === undefined) {
					this.setState({
						exchange_rate: result.exchange_rate,
						content_notify: result.content_notify,
						total_order: result.total_order,
						total_complaint: result.total_complaint,
						account_balance: result.account_balance
					});
					_retrieveData('content_notify').then((content) => {
						if (content.content_notify != result.content_notify) {
							this.setState({ is_new_notify: true });
						}
					});

				} else {
					Alert.alert('Thông báo', 'Có lỗi trong quá trình lấy dữ liệu!');
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});

			listSiteSearch(user_info.userInfo.access_token).then((result) => {
				if (result.success === true) {
					if(result.data['taobao.com'] !== undefined){
						this.setState({ hide_search: false });
					}

					this.setState({ list_site: result.data });
				}
			}).catch((error) => {
				console.error(`Error is: ${error}`);
			});
		});

		// 	this.props.mainView.refreshDataHome();
		const anim1 = Animated.timing(
			this.state.transform,
			{
				toValue: 1,
				duration: 100
			}
		);

		const anim2 = Animated.timing(
			this.state.transform,
			{
				toValue: 0,
				duration: 100
			}
		);

		const finalAnim = Animated.sequence([anim1, anim2]);
		Animated.loop(finalAnim).start();

	}

	onOpenAppFbMessenger = async () => {
		return Linking.openURL(appConfig.fbMessengerUrl);
	};

	render() {
		const rotate = this.state.transform.interpolate({
			inputRange: [0, 0.5, 1],
			outputRange: ['15deg', '0deg', '-15deg']
		})

		return (
			<View style={{ flex: 1 }}>
				<View style={{ height: 30, backgroundColor: '#F2A446', justifyContent: 'center', alignItems: 'center' }}>
					<Text style={{ color: 'white' }}>Tỷ giá: <NumberFormat value={this.state.exchange_rate} displayType={'text'} thousandSeparator={true} renderText={value => <Text style={{ color: 'white' }}>{value}</Text>} /> VNĐ/CNY</Text>
				</View>
				<View style={{ flex: 1, backgroundColor: 'white', padding: 10 }}>

					<View style={{ flex: 1.5, borderRadius: 10, flexDirection: 'row', }}>
						{!this.state.hide_search && (
						<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate(SearchProductScreen)}
								style={{
									alignItems: 'center',
									justifyContent: 'center',
									width: 50,
									height: 50,
									backgroundColor: '#F2A446',
									borderRadius: 50,
								}}
							>
								<Icon name="search" style={{ fontSize: 30, color: '#fff' }} />
							</TouchableOpacity>
							<Text style={{ fontWeight: 'bold', color: appConfig.baseColor, marginTop: 5, fontSize: 14 }}>Tìm hàng</Text>
						</View>)
						}
						<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
							<TouchableOpacity onPress={() => this.props.navigation.navigate(PagePriceScreen)}
								style={{
									alignItems: 'center',
									justifyContent: 'center',
									width: 50,
									height: 50,
									backgroundColor: '#1890ff',
									borderRadius: 50,
								}}
							>
								<Icon name="paper" style={{ fontSize: 30, color: '#fff' }} />
							</TouchableOpacity>
							<Text style={{ fontWeight: 'bold', color: appConfig.baseColor, marginTop: 5, fontSize: 14 }}>Biểu phí</Text>
						</View>
						<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate(PagePayScreen)}
								style={{
									alignItems: 'center',
									justifyContent: 'center',
									width: 50,
									height: 50,
									backgroundColor: 'green',
									borderRadius: 50,
								}}
							>
								<Icon name="card" style={{ fontSize: 30, color: '#fff' }} />
							</TouchableOpacity>
							<Text style={{ fontWeight: 'bold', color: appConfig.baseColor, marginTop: 5, fontSize: 14 }}>Thanh toán</Text>
						</View>
						<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate(ListPackageScreen)}
								style={{
									alignItems: 'center',
									justifyContent: 'center',
									width: 50,
									height: 50,
									backgroundColor: appConfig.baseColor,
									borderRadius: 50,
								}}
							>
								<Icon name="clipboard" style={{ fontSize: 30, color: '#fff' }} />
							</TouchableOpacity>
							<Text style={{ fontWeight: 'bold', color: appConfig.baseColor, marginTop: 5, fontSize: 14 }}>Kiện hàng</Text>
						</View>
					</View>
					<View style={{ flex: 4, flexDirection: 'row' }}>
						<View style={{ flex: 1 }}>
							<Button block style={{ flex: 1, backgroundColor: '#e6f7ff', borderRadius: 10, marginRight: 5, marginBottom: 10 }} onPress={() => { this.refs.modalDetailNotification.openModalDetailNotification() }}>
								<View style={{ flex: 1 }}>
									<View style={styles.topItemHome}>
										<View style={styles.topItemHomeInfo}>
											<Text style={{ fontSize: 16, color: this.state.is_new_notify == false ? appConfig.baseColor : '#F2A446', fontWeight: 'bold', marginBottom: 5 }}>{this.state.is_new_notify == false ? 'Thông báo' : 'Thông báo mới'}</Text>
										</View>
										<View style={styles.itemIconContainer}>
											<Animated.View
												style={{

													transform: [{ rotate }, { rotateY: rotate }],
												}}
											>
												<Icon type="FontAwesome" name='bell' style={{ fontSize: 50, color: 'gray', opacity: 0.6, marginRight: 0 }} />
											</Animated.View>

										</View>
									</View>
									<View style={styles.bottomItem}>
										<Text style={styles.bottomItemText}>Xem chi tiết</Text>
									</View>
								</View>
							</Button>
							<Button block style={{ flex: 1, backgroundColor: '#e6f7ff', borderRadius: 10, marginRight: 5, marginBottom: 10 }} onPress={() => this.props.navigation.navigate(ListTransactionScreen)}>
								<View style={{ flex: 1 }}>
									<View style={styles.topItemHome}>
										<View style={styles.topItemHomeInfo}>
											<Text style={{ fontSize: 16, color: '#F2A446', marginBottom: 5, fontWeight: 'bold', width: 200 }}>
												<NumberFormat value={this.state.account_balance} displayType={'text'} thousandSeparator={true} renderText={value => <Text style={{ color: '#F2A446' }}>{value}</Text>} /> đ
											</Text>
											<Text style={{ fontSize: 16, color: appConfig.baseColor, fontWeight: 'bold' }}>Số dư</Text>
										</View>
										<View style={styles.itemIconContainer}>
											<Icon type="FontAwesome" name='dollar' style={{ fontSize: 60, color: 'gray', opacity: 0.6, marginRight: 0 }} />
										</View>
									</View>
									<View style={styles.bottomItem}>
										<Text style={styles.bottomItemText}>Xem chi tiết</Text>
									</View>
								</View>
							</Button>

						</View>
						<View style={{ flex: 1 }}>
							<Button block style={{ flex: 1, backgroundColor: '#e6f7ff', borderRadius: 10, marginBottom: 10, marginLeft: 5 }} onPress={() => this.props.navigation.navigate(ListOrderScreen, { navigation: this.props.navigation })}>
								<View style={{ flex: 1 }}>
									<View style={styles.topItemHome}>
										<View style={styles.topItemHomeInfo}>
											<Text style={{ fontSize: 16, color: appConfig.baseColor, fontWeight: 'bold', marginBottom: 5 }}>{this.state.total_order}</Text>
											<Text style={{ fontSize: 16, color: appConfig.baseColor, fontWeight: 'bold' }}>Đơn hàng</Text>
										</View>
										<View style={styles.itemIconContainer}>
											<Icon name='paper' style={{ fontSize: 60, color: 'gray', opacity: 0.6, marginRight: 0 }} />
										</View>
									</View>
									<View style={styles.bottomItem}>
										<Text style={styles.bottomItemText}>Xem chi tiết</Text>
									</View>
								</View>
							</Button>
							<Button block style={{ flex: 1, backgroundColor: '#e6f7ff', borderRadius: 10, marginBottom: 10, marginLeft: 5 }} onPress={() => this.props.navigation.navigate(ListComplainScreen, { navigation: this.props.navigation })}>
								<View style={{ flex: 1 }}>
									<View style={styles.topItemHome}>
										<View style={styles.topItemHomeInfo}>
											<Text style={{ fontSize: 16, color: appConfig.baseColor, fontWeight: 'bold', marginBottom: 5 }}>{this.state.total_complaint}</Text>
											<Text style={{ fontSize: 16, color: appConfig.baseColor, fontWeight: 'bold' }}>Khiếu nại</Text>
										</View>
										<View style={styles.itemIconContainer}>
											<Icon name='sad' style={{ fontSize: 60, color: 'gray', opacity: 0.6, marginRight: 0 }} />
										</View>
									</View>
									<View style={styles.bottomItem}>
										<Text style={styles.bottomItemText}>Xem chi tiết</Text>
									</View>
								</View>
							</Button>
						</View>
					</View>
				</View>
				<ModalDetailNotification content_notify={this.state.content_notify} ref={'modalDetailNotification'} parentView={this}></ModalDetailNotification>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	topItemHome: { flex: 2, flexDirection: 'row' },
	topItemHomeInfo: { flex: 5, justifyContent: 'center', paddingLeft: 10, right: -10 },
	bottomItem: {
		flex: 1,
		backgroundColor: 'rgba(0,0,0,0.2)',
		justifyContent: 'center',
		alignItems: 'center',
		bottom: -6,
		borderBottomLeftRadius: 10,
		borderBottomRightRadius: 10
	},
	bottomItemText: { color: 'white', fontWeight: 'bold', fontSize: 12 },
	itemIconContainer: { flex: 4, alignItems: 'center', justifyContent: 'center' },
	itemIcon: { fontSize: 60, color: appConfig.baseColor, opacity: 0.6, marginRight: 10 }
});
