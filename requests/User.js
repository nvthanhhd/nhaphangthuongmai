/*Chua cac function request den server lien quan toi Users (Customer)*/
import { appConfig } from "../AppConfig";
import { objToQueryString } from '../Helpers';

async function postDataRegistration(data){
	const apiUrlRegistration = appConfig.apiUrl + 'register';

	try{
		let response = await fetch(apiUrlRegistration, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function postDataLogin(data){
	const apiUrlLogin = appConfig.apiUrl + 'auth/login';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getDataUserHome(access_token){
	const apiUrlLogin = appConfig.apiUrl + 'dashboard';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getUserInfo(access_token){
	const apiUrlLogin = appConfig.apiUrl + 'user_info';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function postLogout(data){
	const apiUrlLogin = appConfig.apiUrl + 'auth/logout';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + data.access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function updateInfo(data, access_token){
	const apiUrlLogin = appConfig.apiUrl + 'update_user_info';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getListNotify(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrlLogin = appConfig.apiUrl + `view_notify?${queryString}`;

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function updateIsReadNotify(data, access_token){
	const apiUrlLogin = appConfig.apiUrl + 'read_notify';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function readAllNotify(access_token){
	const apiUrlLogin = appConfig.apiUrl + 'read_all_notify';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getListAddress(access_token){
	const apiUrlLogin = appConfig.apiUrl + 'address';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function setAddressDefault(data, access_token){
	const apiUrlLogin = appConfig.apiUrl + 'set_default_address';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function deleteAddress(data, access_token){
	const apiUrlLogin = appConfig.apiUrl + 'delete_address';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}


async function addAddress(data, access_token){
	const apiUrlLogin = appConfig.apiUrl + 'add_address';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

export { postDataRegistration, postDataLogin, getDataUserHome, getUserInfo, postLogout, updateInfo, getListNotify, updateIsReadNotify,
		 readAllNotify, getListAddress, setAddressDefault, deleteAddress, addAddress };
