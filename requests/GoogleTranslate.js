/* eslint-disable prettier/prettier */
/* eslint-disable semi */
import { appConfig } from '../AppConfig';

async function translateSearchKeyword(strKeyword, access_token) {
	const data = { 'keywords': strKeyword }
	const apiUrl = appConfig.apiUrl + 'translateWord';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function translateArrayKeyword(arrKeyword, access_token) {
	const data = { 'keywords': arrKeyword }
	const apiUrl = appConfig.apiUrl + 'translateWords';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

export { translateSearchKeyword, translateArrayKeyword };
