/* eslint-disable prettier/prettier */
/* eslint-disable semi */
import { appConfig } from '../AppConfig';

async function listSiteSearch(access_token) {
	const apiUrl = appConfig.apiUrl + 'listSite';

	try {
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

export { listSiteSearch };
