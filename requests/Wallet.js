/*Chua cac function request den server lien quan toi Vi dien tu (Wallet)*/
import { appConfig } from "../AppConfig";
import { objToQueryString } from '../Helpers';

async function getListTransactionHistory(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrlLogin = appConfig.apiUrl + `transaction?${queryString}`;

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

export { getListTransactionHistory };
