/*Chua cac function request den server lien quan toi Order*/
import { appConfig } from "../AppConfig";
import { objToQueryString } from '../Helpers';

async function getListOrder(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrl = appConfig.apiUrl + `orders?${queryString}`;
	try{
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getDetailOrder(order_id, access_token){
	const apiUrl = appConfig.apiUrl + 'order/' + order_id;
	try{
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getListComplain(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrl = appConfig.apiUrl + `complaints?${queryString}`;
	try{
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getDetailComplain(id, access_token){
	const apiUrl = appConfig.apiUrl + 'complaint/' + id;
	try{
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getListComment(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrl = appConfig.apiUrl + `comments?${queryString}`;
	try{
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function newComment(data, access_token){
	const apiUrl = appConfig.apiUrl + 'comment';

	try{
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function newOrderNote(data, access_token){
	const apiUrl = appConfig.apiUrl + 'order/note';

	try{
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function saveNoteItem(data, access_token){
	const apiUrl = appConfig.apiUrl + 'order_item/note';

	try{
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function saveNoteItemPrivate(data, access_token){
	const apiUrl = appConfig.apiUrl + 'order_item/note_private';

	try{
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function createComplain(data, access_token){
	const apiUrl = appConfig.apiUrl + 'complaint';

	try{
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

export { getListOrder, getDetailOrder, getListComplain, getDetailComplain, getListComment, newComment, newOrderNote, 
	saveNoteItem, saveNoteItemPrivate, createComplain };
