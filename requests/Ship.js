/*Chua cac function request den server lien quan toi Kien hang*/
import { appConfig } from "../AppConfig";
import { objToQueryString } from '../Helpers';

async function getListPackage(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrlLogin = appConfig.apiUrl + `packages?${queryString}`;

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getListRequestDelivery(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrlLogin = appConfig.apiUrl + `request_delivery?${queryString}`;

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getListWaitDelivery(access_token){
	const apiUrlLogin = appConfig.apiUrl + 'waiting_delivery';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getDeliveryInfo(queryString, access_token){
	const apiUrlLogin = appConfig.apiUrl + `delivery_info?${queryString}`;

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

async function createDelivery(data, access_token){
	const apiUrlLogin = appConfig.apiUrl + 'delivery_create';

	try{
		let response = await fetch(apiUrlLogin, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token
			},
			body: JSON.stringify(data)
		});
		let responseJson = await response.json();
		return responseJson;
	} catch(error) {
		console.error(`Error is:  ${error}`);
	}
}

export { getListPackage, getListRequestDelivery, getListWaitDelivery, getDeliveryInfo, createDelivery };
