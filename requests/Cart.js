/* eslint-disable prettier/prettier */
/*Chua cac function request den server lien quan toi cart*/
import { appConfig } from '../AppConfig';
import { objToQueryString } from '../Helpers';

async function getListSaveFavorite(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrl = appConfig.apiUrl + `user/favorite?${queryString}`;

	try {
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function removeFavorite(data, access_token){
	const apiUrl = appConfig.apiUrl + 'favorite_delete';

	try {
		let response = await fetch(apiUrl, {
			method: 'DELETE',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function addToCart(data, access_token){
	const apiUrl = appConfig.baseUrl + 'customer-addon';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getListCart(params, access_token){
	const queryString = objToQueryString(params);
	const apiUrl = appConfig.apiUrl + `cart_index?${queryString}`;

	try {
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function removeShop(data, access_token){
	const apiUrl = appConfig.apiUrl + 'remove_shop';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function removeShopItem(data, access_token){
	const apiUrl = appConfig.apiUrl + 'remove_shop_item';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function saveNoteShop(data, access_token){
	const apiUrl = appConfig.apiUrl + 'shop_note';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function saveNoteItem(data, access_token){
	const apiUrl = appConfig.apiUrl + 'item_note';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function saveFavorite(data, access_token){
	const apiUrl = appConfig.apiUrl + 'item_favorite';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function changeQuantity(data, access_token){
	const apiUrl = appConfig.apiUrl + 'change_quantity';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function checkServiceShop(data, access_token){
	const apiUrl = appConfig.apiUrl + 'choose_service';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function changeShippingTranspTypeName(data, access_token){
	const apiUrl = appConfig.apiUrl + 'change_shipping_transport_type';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function changeServiceInsure(data, access_token){
	const apiUrl = appConfig.apiUrl + 'service_insure';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function saveShippingAmount(data, access_token){
	const apiUrl = appConfig.apiUrl + 'insure_value';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function saveFreightBill(data, access_token){
	const apiUrl = appConfig.apiUrl + 'freight_bill';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function getListCartDepositView(str_data, access_token){
	const apiUrl = appConfig.apiUrl + 'cart_deposit_view?data=' + str_data;

	try {
		let response = await fetch(apiUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

async function cartDeposit(data, access_token){
	const apiUrl = appConfig.apiUrl + 'cart_deposit_process';

	try {
		let response = await fetch(apiUrl, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + access_token,
			},
			body: JSON.stringify(data),
		});
		let responseJson = await response.json();
		return responseJson;
	} catch (error) {
		console.error(`Error is:  ${error}`);
	}
}

export { getListSaveFavorite, removeFavorite, addToCart, getListCart, removeShop, removeShopItem, saveNoteShop, saveNoteItem, saveFavorite
		, changeQuantity, checkServiceShop, changeShippingTranspTypeName, changeServiceInsure, saveShippingAmount, saveFreightBill
		, getListCartDepositView, cartDeposit };
