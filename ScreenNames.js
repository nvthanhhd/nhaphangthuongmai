/*
Luu cac ten man hinh
Author: Kee
*/
const LoginScreen = 'LoginScreen';
const RegistrationScreen = 'RegistrationScreen';
const MainScreen = 'MainScreen';
const ProfileScreen = 'ProfileScreen';
const EditProfileScreen = 'EditProfileScreen';
const ListOrderScreen = 'ListOrderScreen';
const ListTransactionScreen = 'ListTransactionScreen';
const ListPackageScreen = 'ListPackageScreen';
const ListFavoriteScreen = 'ListFavoriteScreen';
const ListAddressScreen = 'ListAddressScreen';
const ListRequestDeliveryScreen = 'ListRequestDeliveryScreen';
const ListComplainScreen = 'ListComplainScreen';
const PagePayScreen = 'PagePayScreen';
const PagePriceScreen = 'PagePriceScreen';
const CreateRequestDeliveryScreen = 'CreateRequestDeliveryScreen';
const DetailComplainScreen = 'DetailComplainScreen';
const DetailOrderScreen = 'DetailOrderScreen';
const CreateComplainScreen = 'CreateComplainScreen';
const SearchProductScreen = 'SearchProductScreen';
const DetailWebOrderScreen = 'DetailWebOrderScreen';
const ListCartScreen = 'ListCartScreen';
const AddressDepositScreen = 'AddressDepositScreen';

export {
	LoginScreen,
	RegistrationScreen,
	MainScreen,
	ProfileScreen,
	EditProfileScreen,
	ListOrderScreen,
	ListTransactionScreen,
	ListPackageScreen,
	ListFavoriteScreen,
	ListAddressScreen,
	ListRequestDeliveryScreen,
	ListComplainScreen,
	PagePayScreen,
	PagePriceScreen,
	CreateRequestDeliveryScreen,
	DetailComplainScreen,
	DetailOrderScreen,
	CreateComplainScreen,
	SearchProductScreen,
	DetailWebOrderScreen,
	ListCartScreen,
	AddressDepositScreen
};
