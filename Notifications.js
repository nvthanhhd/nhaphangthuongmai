/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import messaging from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app';
//import notifee from '@notifee/react-native';

export default class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fcmToken: '',
    };
  }

  async componentDidMount() {
    this.getDeviceToken();
    this.requestUserPermission();
    this.createNotificationListener();
  }

  async createNotificationListener() {
    // messaging().onMessage(async remoteMessage => {
    //   console.log('Message handled in the foreground!', remoteMessage);
    //   // Create a channel
    //   const channelId = await notifee.createChannel({
    //     id: 'default',
    //     name: 'Default Channel',
    //   });

    //   // Display a notification
    //   await notifee.displayNotification({
    //     title:
    //       remoteMessage.notification !== undefined
    //         ? remoteMessage.notification.title
    //         : remoteMessage.data.notification.title,
    //     body:
    //       remoteMessage.notification !== undefined
    //         ? remoteMessage.notification.body
    //         : remoteMessage.data.notification.body,
    //     android: {
    //       channelId,
    //     },
    //   });
    // });
    // Register background handler
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
        }
      });
  }

  async requestUserPermission() {
    const authorizationStatus = await messaging().requestPermission({
      sound: true,
    });
    // const register = await messaging().registerDeviceForRemoteMessages()

    if (authorizationStatus) {
      console.log('Permission status:', authorizationStatus);
    }
  }

  async getDeviceToken() {
    let deviceToken = await AsyncStorage.getItem('fcmToken');
    if (!deviceToken) {
      deviceToken = await firebase.messaging().getToken();
      if (deviceToken) {
        // user has a device token
        console.log('fcmToken1:', deviceToken);
        this.setState({fcmToken: deviceToken});
        await AsyncStorage.setItem('fcmToken', deviceToken);
      }
    }

    console.log('fcmToken2: ', deviceToken);
    this.setState({fcmToken: deviceToken});
  }

  render() {
    return <View />;
  }
}
// const Notifications = () => {
//   // eslint-disable-next-line no-unused-vars
//   const [fcmToken, setFcmToken] = useState('');

//   const requestUserPermission = async () => {
//     const authorizationStatus = await messaging().requestPermission({
//       sound: true,
//     });
//     // const register = await messaging().registerDeviceForRemoteMessages()

//     if (authorizationStatus) {
//       console.log('Permission status:', authorizationStatus);
//     }
//   };

//   const getDeviceToken = async () => {
//     let deviceToken = await AsyncStorage.getItem('fcmToken');
//     if (!deviceToken) {
//       deviceToken = await firebase.messaging().getToken();
//       if (deviceToken) {
//         // user has a device token
//         console.log('fcmToken1:', deviceToken);
//         setFcmToken(deviceToken);
//         await AsyncStorage.setItem('fcmToken', deviceToken);
//       }
//     }

//     console.log('fcmToken2: ', deviceToken);
//     setFcmToken(deviceToken);
//   };

//   useEffect(() => {
//     getDeviceToken();
//     requestUserPermission();
//   }, []);

//   useEffect(() => {
//     messaging().onMessage(async remoteMessage => {
//       console.log('Message handled in the foreground!', remoteMessage);
//       if (Platform.OS === 'ios') {
//         PushNotificationIOS.presentLocalNotification({
//           alertTitle: remoteMessage.data.notification.title,
//           alertBody: remoteMessage.data.notification.body,
//         });
//       } else {
//       }
//     });
//     // Register background handler
//     messaging().setBackgroundMessageHandler(async remoteMessage => {
//       console.log('Message handled in the background!', remoteMessage);
//     });
//     messaging().onNotificationOpenedApp(remoteMessage => {
//       console.log(
//         'Notification caused app to open from background state:',
//         remoteMessage.notification,
//       );
//     });

//     // Check whether an initial notification is available
//     messaging()
//       .getInitialNotification()
//       .then(remoteMessage => {
//         if (remoteMessage) {
//           console.log(
//             'Notification caused app to open from quit state:',
//             remoteMessage.notification,
//           );
//         }
//       });
//   }, []);

//   return <View />;
// };

// export default Notifications;
