function validateEmail(email) {
	let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	return reg.test(email) == 1;
}

function validatePhoneNumber(phone_number){
	let reg = /[0-9]$/;
	return reg.test(phone_number) == 1;
}

export {
	validateEmail,
	validatePhoneNumber
};
