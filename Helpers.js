
import { Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

async function _storeData(key, value){
	try {
		await AsyncStorage.setItem(key, value);
	} catch (error) {
		console.error(`Error is: ${error}`);
	}
};

 async function _retrieveData(key){
	try {
		const value = await AsyncStorage.getItem(key);
		if (value !== null) {
			return JSON.parse(value);
		}
	} catch (error) {
		console.error(`Error is: ${error}`);
	}
};

async function _clear(){
	try {
		await AsyncStorage.clear();
	} catch (error) {
		console.error(`Error is: ${error}`);
	}
};

/**
 *
 * @param expireInMinutes
 * @returns {Date}
 */
function getExpireDate(expireInMinutes) {
	const now = new Date();
	let expireTime = new Date(now);
	expireTime.setMinutes(now.getMinutes() + expireInMinutes);
	return expireTime;
};

function objToQueryString(obj) {
	const keyValuePairs = [];
	for (const key in obj) {

		if(typeof obj[key] === 'object'){
			var params_child  = [];
			for (const key_child in obj[key]) {
				keyValuePairs.push(encodeURIComponent(key+'[]') + '=' + encodeURIComponent(obj[key][key_child]));
			}
		}else{
			keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
		}
	}
	return keyValuePairs.join('&');
}

function isValidURL(string) {
	var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
	if (res == null)
		return false;
	else
		return true;
}

async function onOpenBrowser(item_link){
	Linking.canOpenURL(item_link).then(supported => {
		if(supported){
			return Linking.openURL(item_link);
		} else {
			console.log("Don't know how to open URI: " + item_link);
		}
	});
}

export { _storeData, _retrieveData, _clear, getExpireDate, objToQueryString, isValidURL, onOpenBrowser } ;
