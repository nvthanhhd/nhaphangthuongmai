import {Platform} from 'react-native';

const appConfig = {
  baseUrl: 'http://admin.nhaphangthuongmai.com/',
  baseUrl2: 'http://admin.nhaphangthuongmai.com',
  apiUrl:
    Platform.OS === 'ios'
      ? 'http://admin.nhaphangthuongmai.com/api/'
      : 'http://admin.nhaphangthuongmai.com/api/',
  //apiUrl: Platform.OS === 'ios' ? 'http://localhost/adminhaitau/api/client/' : 'http://10.0.2.2/adminhaitau/api/client/',
  baseColor: '#4e73df', //#01B038
  //fbMessengerUrl: Platform.select({ ios: 'fb-messenger://user-thread/haitau.vn', default: "http://m.me/haitau.vn" })
};

export {appConfig};
